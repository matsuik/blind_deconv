# core
python scripts. NNモデルの定義, training, evaluationなど. また, test内はそれらを書くときにいろいろ試したipynb

# dwsignsim
matlab scripts. training data, test dataの生成モデル, MRtrix3とのinterfaceなど

# model_scripts
実際にNNを学習させる時に動かすスクリプト

# results
ipynbで結果をまとめたもの

# shell_utils