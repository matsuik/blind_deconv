# -*- coding: utf-8 -*-
"""
Created on Mon Jan 11 17:34:48 2016

@author: matsuik

"""

from __future__ import division
import os
import sys
import cPickle
import glob

import h5py
import numpy as np
import scipy.io as sio
from scipy.spatial import ConvexHull

import pandas as pd
from sympy.physics.quantum.cg import CG
from sympy.physics.quantum.spin import Rotation
from sympy import pi

import theano
import theano.tensor as T

import blocks

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
print 'using', theano.config.device
print 'theano.config.floatX', theano.config.floatX


proj_path = '/home/matsui-k/projects/blind_deconv'


def printf(*args):
    print args
    sys.stdout.flush()


# theano utils
def floatX(array_):
    """
    Args
    ----
    array_: numpy.ndarray or float
    
    Returns
    -------
    np.ndarray
    """

    return np.asarray(array_, dtype=theano.config.floatX)


def zero_shared(shape, name=None):
    """
    Args
    ----
    shape : TensorVariable
        from TensorVariable.shape.eval()
    name : str
    """
    return theano.shared(floatX(np.zeros(shape)), name=name)


def make_named_shared_dict(shared_list):
    """
    
    Parameters
    ----------
    shared_list : list of theano.SharedTensorVariable

    Returns
    -------
    dict : str -> theano.SharedTensorVariable

    """
    named_shared_dict = dict()
    for var in shared_list:
        if var.name is not None:
            named_shared_dict[var.name] = var

    return named_shared_dict


# basic
def make_index(L):
    """

    Parameters
    ----------
    L: int

    Returns
    -------
    list of tuple of two int

    >>make_index(4)
    [(0, 1), (1, 6), (6, 15)]

    """
    index_list = []
    start = 0
    for step in [2 * l + 1 for l in range(0, L + 1, 2)]:
        index_list.append((start, start + step))
        start += step
    return index_list


def make_dim_l_list(index_list):
    return [2*(i[1] - i[0]) for i in index_list]


def normalize_ex2_feature_vec(feature_vec_train):
    """
    Parameters
    ----------
    feature_vec : ndarray
        (N, n_tensors, dim) or (N, dim)

    Returns
    -------
    normed : ndarray
        (N, n_tensors, dim) or (N, dim)
    norm_coef : ndarray or float
        (n_tensors, )
    """

    dim_index = len(feature_vec_train.shape) - 1

    eps = 1e-8
    norm_coef = np.sqrt(np.mean(np.linalg.norm(feature_vec_train, axis=dim_index) ** 2, axis=0))

    if dim_index == 2:
        normed_train = feature_vec_train / (norm_coef[:, np.newaxis] + eps)
    if dim_index == 1:
        normed_train = feature_vec_train / (norm_coef + eps)
    return normed_train, norm_coef


def normalize_vec(vec, norm_coef):
    """
    Parameters
    ----------
    feature_vec : ndarray
        (N, n_tensors, dim) or (N, dim)
    norm_coef : ndarray or float
        (n_tensors, ) or float

    Returns
    -------
    normed : ndarray
        (N, n_tensors, dim) or (N, dim)
    """
    if len(vec.shape) == 3:
        normed = vec / (norm_coef[:, np.newaxis] + 1e-8)
    if len(vec.shape) == 2:
        normed = vec / (norm_coef + 1e-8)
    return normed


def vec2comp(x_float):
    """

    Parameters
    ----------
    x_float: numpy.ndarray
        (N, dim, n_tensors)

    Returns
    -------
        (N, dim/2, n_tensors), dtype=complex

    """
    return x_float[:, ::2] + 1j * x_float[:, 1::2]


def comp2vec(x_comp):
    """

    Parameters
    ----------
    x: numpy.ndarray
        (N, dim/2, n_tensors) dtype=complex

    Returns
    -------
    x: numpy.ndarray
        (N, dim, n_tensors) dtype=float

    """
    x_float = floatX(np.zeros((x_comp.shape[0], 2 * x_comp.shape[1], x_comp.shape[2])))
    x_float[:, ::2] = np.real(x_comp)
    x_float[:, 1::2] = np.imag(x_comp)
    return x_float


def rotx(angle):
    theta = angle / 180 * np.pi
    c = np.cos(theta)
    s = np.sin(theta)
    return np.array([[1, 0, 0], [0, c, -s], [0, s, c]])


def roty(angle):
    theta = angle / 180 * np.pi
    c = np.cos(theta)
    s = np.sin(theta)
    return np.array([[c, 0, s], [0, 1, 0], [-s, 0, c]])


def rotz(angle):
    theta = angle / 180 * np.pi
    c = np.cos(theta)
    s = np.sin(theta)
    return np.array([[c, -s, 0], [s, c, 0], [0, 0, 1]])


def separate_tensors(tensor, index_list):
    """

    Parameters
    ----------
    tensor : (complex_dim, N)
    index_list

    Returns
    -------
    list of numpy.ndarray
        (separated_dim, N)

    """
    return [tensor[index[0]:index[1]] for index in index_list]


def comp2vector_tensors(tens_list):
    """

    Parameters
    ----------
    tens_list : list of numpy.ndarrayneg_lambda value
        each has shape (dim_l_comp, n_samples)

    Returns
    -------
    list of numpy.ndarray
        each has shape (n_samples, dim_l_float)

    """
    v_list = []
    for tens in tens_list:
        # (n_samples, dim_l_comp, n_tensors=1) -> (n_samples, dim_l_float)
        tmp = np.squeeze(comp2vec(tens.T[:, :, np.newaxis]))
        v_list.append(tmp)
        print tmp.shape
    return v_list


# load, preprocces
def load_hardi_sh(max_L, fp, matlabv73=True):
    """
    matlab structからloadして、lごとにsplitして、複素数を実数に直す

    Parameters
    ----------
    max_L : int
    fp : full path to hardi_sh[lmax].mat

    Returns
    -------
    input_list : list of ndarray
        0, 2, 4, 6, ...
        (N, dim=2*(2*l+1))


    """

    full_path = os.path.expanduser(os.path.join(fp,'hardi_sh'+str(max_L)+'.mat'))
    printf('loding data from '+full_path)

    if matlabv73:
        with h5py.File(full_path) as f:
            input = np.array((f['signal_sh'])).T
    else:
        ms_input = sio.loadmat(full_path)
        input = ms_input['signal_sh']
    print 'input.shape', input.shape

    input_index = make_index(max_L)
    print 'indices', input_index
    input_list = separate_tensors(input, input_index)
    input_list = comp2vector_tensors(input_list)

    printf('data is loaded.')

    print 'input_list shapes', [a.shape for a in input_list]
    return input_list


def load_hardi_qspace(dirpath, for_response_prediction=False):
    """
    
    Parameters
    ----------
    filepath : str
        for train "project_path/simulation/b3000_gaussian_novi_snr50/train"
        for test "projecct_path/simulation/b3000_gaussian_novi_snr50/test/angle20ratio1/data"
    for_response_prediction : bool
        rotback_bdirと対応付けるために, reshapeの仕方変える
    
    Returns
    -------
    numpy.ndarray
        has shape (n_samples, n_grad_schemes)

    """
    if 'train' in dirpath:
        with h5py.File(os.path.join(dirpath, 'data_base.mat')) as f:
            signal = np.array(f['signal'])   # has shape (n_gen_params, n_fiber_directions, n_dirs)
        signal = signal.reshape(signal.shape[0] * signal.shape[1], signal.shape[2])
        return signal[:, 1:]

    elif 'rotated' in dirpath:
        if for_response_prediction:
            ms = sio.loadmat(os.path.join(dirpath, 'data_base.mat'))
            signal = ms['signal']  # has shape (n_dirs, n_fiber_dir=100, n_gen_params=80)
            signal = np.transpose(signal, (0, 2, 1))  # (n_dirs, n_gen_params, n_fiber_dirs)
            signal = signal.reshape((signal.shape[0], signal.shape[1] * signal.shape[2]))
            signal = signal.T
            del ms
            return signal[:, 1:]
        else:
            ms = sio.loadmat(os.path.join(dirpath, 'data_base.mat'))
            signal = ms['signal']  # has shape (n_dirs, n_fiber_dir=100, n_gen_params=80)
            signal = signal.reshape((signal.shape[0], signal.shape[1] * signal.shape[2]))
            signal = signal.T
            del ms
            return signal[:, 1:]
    else:
        ms = sio.loadmat(os.path.join(dirpath, 'data_base.mat'))
        signal = ms['signal']  # has shape (n_dirs, 1, 800)
        signal = signal.reshape((signal.shape[0], signal.shape[2]))
        signal = signal.T
        del ms
        return signal[:, 1:]


def load_fodf_reconst(max_L, fp, matlabv73=True):
    """

    Parameters
    ----------
    max_L : int
        8 of 12
    dir : str
        full path to fodf_reconst[lmax].mat

    Returns
    -------
    fodf : numpy.ndarray
        shape (N, n_dirs)
    """
    if matlabv73:
        with h5py.File(os.path.expanduser(os.path.join(proj_path, fp, 'fodf_reconst'+str(max_L)+'.mat'))) as f:
            return np.array(f['fodf'])
    else:
        ms = sio.loadmat(os.path.expanduser(os.path.join(proj_path, fp, 'fodf_reconst'+str(max_L)+'.mat')))
        return np.array(ms['fodf'].T)


def load_fodf_sh(max_L, dirpath):
    """
    NOTE : trainとtestは分けなくてよかった
    
    Parameters
    ----------
    max_L : int
    filepath : str
        for train "/home/matsu-k/simulation/response_b3000_gauss_90HCP_1272_snr50/train"
        for test "/home/matsui-k/simulation/response_b3000_gauss_90HCP_1272_snr50/test/angle20ratio1/data"
    Returns
    -------
    list of numpy.ndarray
        an element has (n_samples, dim_l=2*(2*l+1))

    """
    filepath = os.path.join(dirpath, 'fodf_sh' + str(max_L) + '.mat')
    with h5py.File(filepath) as f:
        fodf_sh = np.array(f['fodf_sh'])  # has shape (n_gen_params, n_fiber_directions, n_dirs)

    fodf_sh = fodf_sh.reshape((fodf_sh.shape[0] * fodf_sh.shape[1], fodf_sh.shape[2]))
    index_list = make_index(max_L)
    input_tensors_comp = separate_tensors(fodf_sh.T, index_list)
    input_tensors = comp2vector_tensors(input_tensors_comp)
    return input_tensors


def load_Mcontra_signal(max_L, dirpath):
    """
    
    Parameters
    ----------
    max_L : int
    dirpath : str
        'projectpath/simulation/response_b3000_gauss_90HCP_1272_snr50/train'

    Returns
    -------

    """
    ms = sio.loadmat(os.path.join(dirpath, 'Mcontra_signal{}.mat'.format(max_L)))
    M = ms['Mcontra_signal']
    M_t = M[:make_index(max_L)[-1][1]].T

    pad = np.zeros((M_t.shape[0], M_t.shape[1] * 2))
    pad[:, ::2] = np.real(M_t)
    pad[:, 1::2] = -np.imag(M_t)

    return floatX(pad)


def load_Mreconstruct(max_L, n_dirs):
    """
    Parameters
    ----------
    max_L : int

    Returns
    -------
    pad : numpy.ndarray
        (n_dir, sum(2*(2*L_array+1)))
        [[R, -I, R, -I, ...],
         [R, -I, R, -I, ...],,,

    """
    ms = sio.loadmat(os.path.join(proj_path, 'data', 'Mcontra_fodf'+str(n_dirs)+'.mat'))
    M = ms['Mcontra_fodf']
    M_t = M[:make_index(max_L)[-1][1]].T

    pad = np.zeros((M_t.shape[0], M_t.shape[1] * 2))
    pad[:, ::2] = np.real(M_t)
    pad[:, 1::2] = -np.imag(M_t)

    return floatX(pad)


def load_gt_dir(folder_name):
    """

    Parameters
    ----------
    folder_name : str

    Returns
    -------
    n1 : numpy.ndarray
        (3, )
    n2 : numpy.ndarray
        (3, )

    """
    try:
        ms = sio.loadmat(os.path.join(folder_name, 'data_base.mat'))
        n1 = floatX(ms['n1'][:, 0])
        n2 = floatX(ms['n2'][:, 0])
    except NotImplementedError:
        with h5py.File(os.path.join(folder_name, 'data_base.mat')) as ms:
            n1 = floatX(ms['n1'][0])
            n2 = floatX(ms['n2'][0])
    return n1, n2


def pre_blind_pc(input_tensors, gt_vec, normed=True, train_ratio=0.8, permu=True, N=None):
    """
    inputは正規化してる, gtは0以下のところを0にしちゃってる

    Parameters
    ----------
    input_tensors : list of numpy.ndarray
        (N_all, dim) or (N_all, dim, n_tensors)
    gt_vec : numpy.ndarray
        (N_all, n_dir)
    normed : bool
    N : #samples to use
    train_ratio

    Returns
    -------
    list of ndarray:
        l-th element has shape (N, dim, n_tensors)
    ndarray:
        (N, n_dir)
    list of ndarray:
        l-th element has shape (N, dim, n_tensors)
    ndarray:
        (N, n_dir)
    input_coef : list of ndarray
        (n_tensors, )

    """
    if N is None:
        N = input_tensors[0].shape[0]
    n_train = int(N * train_ratio)
    n_test = int(N * (1. - train_ratio))
    if permu:
        permu_index = np.random.permutation(range(input_tensors[0].shape[0]))
    else:
        permu_index = range(input_tensors[0].shape[0])

    def normalize(tens_list):
        """

        Parameters
        ----------
        tens_list : list of numpy.ndarray
            a array has shape (N_all, n_tensors, dim) for normalize_ex2_feature_vec
        Returns
        -------
        list of numpy.ndarray
            a array has shape (N_train, dim, n_tensors)
        list of numpy.ndarray
            a array has shape (N_test, dim, n_tensors)
        list of numpy.ndarray
            a array has shape (n_tensors, )

        """
        train_normed_list = []
        test_normed_list = []
        coef_list = []
        if normed:
            for tens in tens_list:
                train_normed, coef = normalize_ex2_feature_vec(tens[permu_index][:n_train])
                printf(train_normed.shape)
                train_normed_list.append(train_normed.transpose(0, 2, 1))
                coef_list.append(coef)
                test_normed = normalize_vec(tens[permu_index][n_train:n_train + n_test], coef)
                test_normed_list.append(test_normed.transpose(0, 2, 1))
                printf(test_normed.shape)
        else:
            for tens in tens_list:
                train_normed = tens[permu_index][:n_train]
                coef = np.ones(train_normed.shape[1])
                printf(train_normed.shape)
                train_normed_list.append(train_normed.transpose(0, 2, 1))
                coef_list.append(coef)
                test_normed = tens[permu_index][n_train:n_train + n_test]
                test_normed_list.append(test_normed.transpose(0, 2, 1))
                printf(test_normed.shape)

        return train_normed_list, test_normed_list, coef_list

    if input_tensors[0].ndim == 2:  # (N_all, dim) to (N_all, 1, dim)
        input_tensors = [tensor[:, np.newaxis, :] for tensor in input_tensors]
        printf(input_tensors[1].shape)
    elif input_tensors[0].ndim == 3:  # (N_all, dim, n_tensors) to (N_all, n_tensors, dim)
        input_tensors = [tensor.transpose(0, 2, 1) for tensor in input_tensors]
        printf(input_tensors[1].shape)

    train_input, test_input, input_coef = normalize(input_tensors)

    gt_vec[gt_vec < 0.] = 0.
    gt_vec = gt_vec / np.sum(gt_vec, axis=1)[:, np.newaxis]

    train_output = gt_vec[permu_index][:n_train]
    test_output = gt_vec[permu_index][n_train:n_train + n_test]

    return train_input, train_output, test_input, test_output, input_coef


# postproces, evaluation
def predict_original_feature(f_output, feature_vec_list, norm_coef_list, n_dirs, non_negative=True,
                             softmax=False, minusmin=False, batch_size=1024):
    """
    正規化して, 一気に計算するとメモリに乗らないので分けてoutputして、concatenate
    最後に ret_mat > 0にしとく

    Args
    ----
    f_output : theano compiled fuction
        from a few np.ndarray-s shape=(N, dim, n_tensors) dtype=np.float32
        to one length list of np.ndarray shape=(N, n_dir) dtype=np.float32
    feature_vec_list : list of np.ndarray
        not normalized yet
        (N, dim, n_tensors)
    norm_coef_list : list of numpy.ndarray
        (n_tensors,)
    n_dirs : int
        HARDI信号の離散化方向の数
    batch_size : int

    Returns
    -------
    np.ndarray : (N, n_dir)
    """

    if feature_vec_list[0].ndim == 2:
        feature_vec_list = [v[:, :, np.newaxis] for v in feature_vec_list]
    processed_list = [tens/coef for (tens, coef) in zip(feature_vec_list, norm_coef_list)]
    ret_mat = floatX(np.zeros((processed_list[0].shape[0], n_dirs)))
    n_batch = processed_list[0].shape[0] // batch_size
    for i_batch in range(n_batch):
        index = i_batch * batch_size
        # f_output returns (N, n_dir)
        ret_mat[index: index + batch_size] = f_output(
            *[floatX(processed[index: index + batch_size]) for processed in processed_list])[0]
    if not n_batch * batch_size == processed_list[0].shape[0]:
        ret_mat[n_batch * batch_size:] = \
            f_output(*[floatX(processed[n_batch * batch_size:]) for processed in processed_list])[0]

    print non_negative
    if softmax:
        ret_mat = np.exp(ret_mat) / np.sum(np.exp(ret_mat), axis=1)[:, np.newaxis]
    elif non_negative:
        print 'padding negative by 0'
        ret_mat[ret_mat < 0] = 0
    elif minusmin:
        ret_mat = ret_mat - np.min(ret_mat, axis=1)[:, np.newaxis]
    return ret_mat


def rotate_back_bdirs(bdirs, euler_angles, n_config):
    """
    Parameters
    ----------
    bdirs : numpy.ndarray
        (3, n_dirs)
    euler_angles : numpy.ndarray
        (n_rotations, 3(rotx, roty, rotz)) angles used for forward rotation
    n_config : int
        number of the scalar parameters of the simulation

    Returns
    -------
        numpy.ndarray
        (n_rotations*n_config, 3, n_dirs)

    """
    rotback_bdirs = np.zeros((euler_angles.shape[0], bdirs.shape[0], bdirs.shape[1]))
    for i_rot, eul in enumerate(euler_angles):
        rotback_bdirs[i_rot] = np.dot(rotz(-eul[2]), np.dot(roty(-eul[1]), np.dot(rotx(-eul[0]), bdirs)))
    return np.tile(rotback_bdirs, (n_config, 1, 1))


def localmax(pred, n_dirs, ratio=0.2):
    """

    Parameters
    ----------
    pred : numpy.ndarray
        (N, n_dir)
    n_dirs : float
    ratio : float
        peak is valid when its scale is larger than ratio * 1st peak scale
    Returns
    -------
    peak_value_list_list : list of list
    peak_dir_list_list : list of list
    valueが大きい順に並んでる
    全部


    """
    ms = sio.loadmat('/home/matsui-k/projects/blind_deconv/dwsignsim/bdirs_whole_'+str(n_dirs)+'.mat')
    bdirs = ms['bdirs']
    assert pred.shape[1] == bdirs.shape[1]

    # convexhullでedgeで繋がってるvertexをneighborとする, nnの数は自分も含めて6か7になる
    ch = ConvexHull(bdirs.T)
    knn_indices = []
    for i_dir in range(bdirs.shape[1]):
        nn_indices_list = []
        for simplex in ch.simplices:
            if i_dir in simplex:
                nn_indices_list.extend(list(simplex))
        knn_indices.append(np.unique(nn_indices_list))

    peak_value_list_list = []
    peak_dir_list_list = []
    for s in pred:
        # nnの中でlocal maxなら候補にする
        local_max_indices_for_bdirs = []
        for i_dir in range(s.shape[0]):
            if knn_indices[i_dir].shape[0] == 0:
                continue
            if i_dir == knn_indices[i_dir][np.argmax(s[knn_indices[i_dir]])]:
                local_max_indices_for_bdirs.append(i_dir)
        indices_sorted_for_local_max = np.argsort(s[local_max_indices_for_bdirs])[::-2]  # ソートしてから後ろから1ッコ飛ばしで

        # global maxのratio倍のやつだけ採用する
        max_index = indices_sorted_for_local_max[0]
        valid_peak_indices = []
        for i_peak in indices_sorted_for_local_max:
            if s[local_max_indices_for_bdirs[i_peak]] > s[local_max_indices_for_bdirs[max_index]] * ratio:
                valid_peak_indices.append(local_max_indices_for_bdirs[i_peak])

        # 2字形式で近似して、eigenvectorを取ることでmaxを求める
        peak_value_list = []
        peak_dir_list = []
        for i_peak in valid_peak_indices:
            nn_index = knn_indices[i_peak]
            nn_bdirs = bdirs[:, nn_index]
            # x.shape (dim=6, #nn=6 or 7)
            x = np.array([nn_bdirs[0] ** 2, nn_bdirs[1] ** 2, nn_bdirs[2] ** 2, nn_bdirs[0] * nn_bdirs[1],
                          nn_bdirs[1] * nn_bdirs[2], nn_bdirs[2] * nn_bdirs[0]])
            # y.shape (#nn=6 or 7, )
            y = s[nn_index]
            w = np.dot(np.linalg.pinv(x.T), y)  # w.shape (dim=6, )
            H = np.array([[w[0], w[3] / 2, w[5] / 2], [w[3] / 2, w[1], w[4] / 2], [w[5] / 2, w[4] / 2, w[2]]])
            e_val, e_vec = np.linalg.eigh(H)
            peak_value_list.append(e_val[-1])
            peak_dir_list.append(e_vec[:, -1])

        peak_dir_list_list.append(peak_dir_list)
        peak_value_list_list.append(peak_value_list)

    return peak_value_list_list, peak_dir_list_list


def localmax_rotback(pred, rotback_bdirs_ar, ratio=0.2):
    """

    Parameters
    ----------
    pred : numpy.ndarray
        (N, n_dir)
    n_dirs : float
    ratio : float
        peak is valid when its scale is larger than ratio * 1st peak scale
    Returns
    -------
    peak_value_list_list : list of list
    peak_dir_list_list : list of list # valueが大きい順に並んでる


    """
    assert pred.shape[1] == rotback_bdirs_ar.shape[2]

    # convexhullでedgeで繋がってるvertexをneighborとする, nnの数は自分も含めて6か7になる
    ch = ConvexHull(rotback_bdirs_ar[0].T)
    knn_indices = []
    for i_dir in range(rotback_bdirs_ar[0].shape[1]):
        nn_indices_list = []
        for simplex in ch.simplices:
            if i_dir in simplex:
                nn_indices_list.extend(list(simplex))
        knn_indices.append(np.unique(nn_indices_list))

    peak_value_list_list = []
    peak_dir_list_list = []
    for s, bdirs in zip(pred, rotback_bdirs_ar):
        # nnの中でlocal maxなら候補にする
        local_max_indices_for_bdirs = []
        for i_dir in range(s.shape[0]):
            if knn_indices[i_dir].shape[0] == 0:
                continue
            if i_dir == knn_indices[i_dir][np.argmax(s[knn_indices[i_dir]])]:
                local_max_indices_for_bdirs.append(i_dir)
        indices_sorted_for_local_max = np.argsort(s[local_max_indices_for_bdirs])[::-2]  # ソートしてから後ろから1ッコ飛ばしで

        # global maxのratio倍のやつだけ採用する
        max_index = indices_sorted_for_local_max[0]
        valid_peak_indices = []
        for i_peak in indices_sorted_for_local_max:
            if s[local_max_indices_for_bdirs[i_peak]] > s[local_max_indices_for_bdirs[max_index]] * ratio:
                valid_peak_indices.append(local_max_indices_for_bdirs[i_peak])

        # 2字形式で近似して、eigenvectorを取ることでmaxを求める
        peak_value_list = []
        peak_dir_list = []
        for i_peak in valid_peak_indices:
            nn_index = knn_indices[i_peak]
            nn_bdirs = bdirs[:, nn_index]
            # x.shape (dim=6, #nn=6 or 7)
            x = np.array([nn_bdirs[0] ** 2, nn_bdirs[1] ** 2, nn_bdirs[2] ** 2, nn_bdirs[0] * nn_bdirs[1],
                          nn_bdirs[1] * nn_bdirs[2], nn_bdirs[2] * nn_bdirs[0]])
            # y.shape (#nn=6 or 7, )
            y = s[nn_index]
            w = np.dot(np.linalg.pinv(x.T), y)  # w.shape (dim=6, )
            H = np.array([[w[0], w[3] / 2, w[5] / 2], [w[3] / 2, w[1], w[4] / 2], [w[5] / 2, w[4] / 2, w[2]]])
            e_val, e_vec = np.linalg.eigh(H)
            peak_value_list.append(e_val[-1])
            peak_dir_list.append(e_vec[:, -1])

        peak_dir_list_list.append(peak_dir_list)
        peak_value_list_list.append(peak_value_list)

    return peak_value_list_list, peak_dir_list_list


def sucess_rate_sprious_peak(peak_dir_list_list):
    """

    Parameters
    ----------
    peak_dir_list_list

    Returns
    -------
    success_rate : float
        peakが2本以上でてる割合
    n_spurious_peaks : float
        2本以上peakがあるときの、それらの数の平均
    success_mask : numpy.ndarray
        successしてるとこでTrueになってる

    """
    len_ar = np.array([len(peak_value_list) for peak_value_list in peak_dir_list_list])
    success_mask = len_ar >= 2
    success_rate = np.mean(success_mask)
    n_spurious_peaks = np.mean(len_ar[success_mask] - 2)
    return success_rate, n_spurious_peaks, success_mask


def eval_peak_dirs(peak_value_list_list, peak_dir_list_list, n1, n2):
    peak_list = [] # peakが2個以上あるやつだけでの計算につかう
    peak_list_list_for_v = [] # peakが2個以上あるやつだけの, scaleありでvisualizationに使える
    peak_list_all = [] # peakが1個だけでも, 両方に割り当てて計算するもの
    fault_list = []

    # peak_valueが高いやつから優先的に評価, second peakにあまりのnを割り当てる
    for i, peak_dir_list in enumerate(peak_dir_list_list):
        if len(peak_dir_list) < 2:
            fault_list.append(i)
            try:
                peak1 = peak_dir_list[0]
                peak_list_all.append([peak1, peak1])
            except IndexError as e:
                print e
                continue
        else:
            i_max = np.argmax([abs(np.dot(peak_dir_list[0], n)) for n in [n1, n2]])
            if i_max == 0:
                peak1 = peak_dir_list[0]
                v1 = peak_value_list_list[i][0]
                peak2 = peak_dir_list[1]
                v2 = peak_value_list_list[i][1]
            else:
                peak1 = peak_dir_list[1]
                v1 = peak_value_list_list[i][1]
                peak2 = peak_dir_list[0]
                v2 = peak_value_list_list[i][0]
            peak_list.append([peak1, peak2])
            peak_list_list_for_v.append([v1*peak1, v2*peak2])

    peak_list_all.extend(peak_list)

    peak_ar = np.array(peak_list)
    peak_ar_all = np.array(peak_list_all)

    try:
        bias1, std1 = dyadic_evaluation(peak_ar[:, 0], n1)
        bias2, std2 = dyadic_evaluation(peak_ar[:, 1], n2)
    except IndexError as e:
        print e
        print "WARNING: THERE ARE ONLY SINGLE PEAKS"
        bias1, std1, bias2, std2 = [90 for _ in range(4)]

    try:
        bias1_all, std1_all = dyadic_evaluation(peak_ar_all[:, 0], n1)
        bias2_all, std2_all = dyadic_evaluation(peak_ar_all[:, 1], n2)
    except IndexError as e:
        print e
        print "WARNING: THERE IS NO PEAK"
        bias1_all, std1_all, bias2_all, std2_all = [90 for _ in range(4)]

    return peak_list_list_for_v, [bias1, bias2], [std1, std2], [bias1_all, bias2_all], [std1_all, std2_all]


def eval_peak_dirs_with_tolerance(peak_dir_list_list, n1, n2, angle_tolerance):
    peak_list = [] # peakが2個以上あるやつだけでの計算につかう

    for i, peak_dir_list in enumerate(peak_dir_list_list):
        peak1and2 = []
        if len(peak_dir_list) < 2:
            continue
        else:
            for peak_dir in peak_dir_list:
                degree = calc_degree(peak_dir, n1)
                if degree < angle_tolerance:
                    peak1and2.append(peak_dir)
                    break

            if peak1and2 == []:
                continue

            # n1に対応するやつがある状態で実行される
            for peak_dir in peak_dir_list:
                degree = calc_degree(peak_dir, n2)
                if degree < angle_tolerance:
                    peak1and2.append(peak_dir)
                    break

            if len(peak1and2) == 2:
                peak_list.append(peak1and2)

    success_rate = len(peak_list) / len(peak_dir_list_list)

    peak_ar = np.array(peak_list)

    try:
        bias1, std1 = dyadic_evaluation(peak_ar[:, 0], n1)
        bias2, std2 = dyadic_evaluation(peak_ar[:, 1], n2)
    except IndexError as e:
        print e
        print "WARNING: NO DECENT PEAKS"
        bias1, std1, bias2, std2 = [90 for _ in range(4)]

    return success_rate, [bias1, bias2], [std1, std2]


def dyadic_evaluation(peak_ar, gt_vec):
    """

    Parameters
    ----------
    peak_ar : numpy.ndarray
        (N, 3)

    Returns
    -------

    """
    value, vec = np.linalg.eig(np.dot(peak_ar.T, peak_ar)/peak_ar.shape[0])
    mean_vec = vec[:, np.argmax(value)]
    bias = np.arccos(abs(np.dot(mean_vec, gt_vec)))/ np.pi * 180
    std = np.percentile(np.arccos(abs(np.dot(peak_ar, mean_vec)))/ np.pi * 180, [0, 68], interpolation='linear')[1]
    return bias, std


def calc_degree(vector_array, target_vector):
    """

    Parameters
    ----------
    vector_array : (N, 3) or (3, )
    target_vector : (3, )

    Returns
    -------
    (N, ) or float

    """
    if vector_array.ndim == 1:
        v_normed = vector_array / (np.linalg.norm(vector_array) + 1e-8)
    else:
        v_normed = vector_array / (np.linalg.norm(vector_array, axis=1) + 1e-8)[:, np.newaxis]

    t_normed = target_vector / (np.linalg.norm(target_vector) + 1e-8)
    return np.arccos(abs(np.dot(v_normed, t_normed))) / np.pi * 180


def df_over_angles(simulation_dirname, trained_model_dirname,  rotated, method=None,ratio=1):
    """
    Examples
    -----------
    blind_utils.summarize_over_angles2df('response_b3000_gauss_90HCP_1272_snr50', 'lmaxXregY', 1, 0, '/home/matsui-k/projects/blind_deconv/simulation/response_b3000_gauss_90HCP_1272_snr50/trained_models/lmaxXregY/not_rotated.png', method='csd')
    blind_utils.summarize_over_angles2df('response_b3000_gauss_90HCP_1272_snr50', 'lmaxXregY', 1, 1, '/home/matsui-k/projects/blind_deconv/simulation/response_b3000_gauss_90HCP_1272_snr50/trained_models/lmaxXregY/rotated.png', method='csd')
    Parameters
    ----------
    simulation_dirname : simulation basename
    trained_model_dirname : model basename or lmax8thresh0
    ratio
    rotated
    method : str
        None or rcn or csd

    Returns
    -------
loss = (2 - z) ** 2
    """

    simulation_dirname = os.path.basename(os.path.normpath(simulation_dirname))
    trained_model_dirname = os.path.basename(os.path.normpath(trained_model_dirname))

    if method is None:
        if 'nlayer' in trained_model_dirname:
            method = 'rcn'
        elif 'lmax' in trained_model_dirname:
            method = 'csd'
        else:
            print 'WARNING : INVALID MODEL NAME {}'.format(trained_model_dirname)
            return


    if rotated:
        angleratio_dirname_list = [os.path.basename(p) for p in glob.glob(
        os.path.join(proj_path, 'simulation', simulation_dirname, 'test/*ratio{ratio}rotated').format(
        ratio=ratio))]
    else:
        angleratio_dirname_list = [os.path.basename(p) for p in glob.glob(
            os.path.join(proj_path, 'simulation', simulation_dirname, 'test/*ratio{ratio}').format(
            ratio=ratio))]

    angleratio_dirname_list = sorted(angleratio_dirname_list)

    def summary2df(sammary, name):
        return pd.DataFrame({s.split(' ')[0]: s.split(' ')[1][:-1] for s in sammary}, index=[name])

    df_list = []
    for angleratio_dirname in angleratio_dirname_list:
        with open(os.path.join(proj_path, 'simulation', simulation_dirname,
                               'test', angleratio_dirname, '{}_results'.format(method),
                               trained_model_dirname, 'summary.txt'),
                  'r') as f:
            df_list.append(summary2df(f.readlines(), angleratio_dirname[5:7]))

    return pd.concat(df_list).astype(float)



def summarize_over_angles2df(simulation_dirname, trained_model_dirname, ratio, rotated, save_filepath=None, method='rcn', to_return=False):
    """
    Examples
    -----------
    blind_utils.summarize_over_angles2df('response_b3000_gauss_90HCP_1272_snr50', 'lmaxXregY', 1, 0, '/home/matsui-k/projects/blind_deconv/simulation/response_b3000_gauss_90HCP_1272_snr50/trained_models/lmaxXregY/not_rotated.png', method='csd') 
    blind_utils.summarize_over_angles2df('response_b3000_gauss_90HCP_1272_snr50', 'lmaxXregY', 1, 1, '/home/matsui-k/projects/blind_deconv/simulation/response_b3000_gauss_90HCP_1272_snr50/trained_models/lmaxXregY/rotated.png', method='csd')
    Parameters
    ----------
    simulation_dirname : simulation basename
    trained_model_dirname : model basename or lmax8thresh0
    ratio
    rotated
    method : str
        rcn or csd

    Returns
    -------
loss = (2 - z) ** 2
    """

    simulation_dirname = os.path.basename(os.path.normpath(simulation_dirname))
    trained_model_dirname = os.path.basename(os.path.normpath(trained_model_dirname))

    if save_filepath is None:
        save_filepath = os.path.join('/home/matsui-k/projects/blind_deconv/simulation',
                                     simulation_dirname, 'trained_models', trained_model_dirname, 'summary_{}.png'.format(
                'rotated' if rotated else 'not_rotated'
            ))

    if rotated:
        angleratio_dirname_list = [os.path.basename(p) for p in glob.glob(
            os.path.join(proj_path, 'simulation', simulation_dirname, 'test/*ratio{ratio}rotated').format(
                ratio=ratio))]
    else:
        angleratio_dirname_list = [os.path.basename(p) for p in glob.glob(
            os.path.join(proj_path, 'simulation', simulation_dirname, 'test/*ratio{ratio}').format(
                ratio=ratio))]

    angleratio_dirname_list = sorted(angleratio_dirname_list)

    def summary2df(sammary, name):
        return pd.DataFrame({s.split(' ')[0]: s.split(' ')[1][:-1] for s in sammary}, index=[name])

    df_list = []
    for angleratio_dirname in angleratio_dirname_list:
        with open(os.path.join(proj_path, 'simulation', simulation_dirname,
                               'test', angleratio_dirname, '{}_results'.format(method),
                               trained_model_dirname, 'summary.txt'),
                  'r') as f:
            df_list.append(summary2df(f.readlines(), angleratio_dirname))

    fig = plt.figure(figsize=(16, 4))

    df = pd.concat(df_list).astype(float)

    ax = fig.add_subplot(151)
    df.loc[:, ['success_rate']].plot(ax=ax)
    plt.xticks(rotation='vertical')

    ax = fig.add_subplot(152)
    df.loc[:, ['n_spurious_peaks']].plot(ax=ax)
    plt.xticks(rotation='vertical')

    ax = fig.add_subplot(153)
    ax.set_ylim(0, df.loc[:, ['bias1', 'bias2']].max().max() + 0.5)
    df.loc[:, ['bias1', 'bias2']].plot(ax=ax)
    plt.xticks(rotation='vertical')

    ax = fig.add_subplot(154)
    ax.set_ylim(0, df.loc[:, ['std1', 'std2']].max().max())
    df.loc[:, ['std1', 'std2']].plot(ax=ax)
    plt.xticks(rotation='vertical')

    ax = fig.add_subplot(155)
    df.loc[:, ['success_rate_tol']].plot(ax=ax)
    plt.xticks(rotation='vertical')

    if not os.path.exists(os.path.dirname(save_filepath)):
        os.makedirs(os.path.dirname(save_filepath))
    plt.savefig(save_filepath)
    print('saved fig in ', save_filepath)

    if to_return:
        return df


# spherical tensors
def clebsch_Gordan_coef(j, m, j1, m1, j2, m2):
    """
    see https://en.wikipedia.org/wiki/Table_of_Clebsch%E2%80%93Gordan_coefficients
    """
    assert -j <= m <= j
    assert -j1 <= m1 <= j1
    assert -j2 <= m2 <= j2
    assert abs(j1 - j2) <= j <= j1 + j2

    # CG returns 0 against the invalid args.
    return float(CG(j1, m1, j2, m2, j, m).doit())


def cg_table(j, j1, j2):
    """

    Parameters
    ----------
    j
    j1
    j2

    Returns
    -------
    table : numpy.ndarray
        shape=(2*J+1, 2*j1+1, 2*j2+1)

    """
    table = np.zeros((2 * j + 1, 2 * j1 + 1, 2 * j2 + 1))
    for m in range(-j, j + 1):
        for m1 in range(-j1, j1 + 1):
            for m2 in range(-j2, j2 + 1):
                table[m, m1, m2] = clebsch_Gordan_coef(j, m, j1, m1, j2, m2)
    return floatX(table)


def wignerD(j, alpha=pi / 2., beta=pi / 2., gamma=pi / 2.):
    """

    Parameters
    ----------
    j
    alpha : around z
    beta : around y
    gamma : around x

    Returns
    -------
    wigner D matrix

    """
    A = np.zeros((2 * j + 1, 2 * j + 1), dtype=complex)
    for m in range(-j, j + 1):
        for mp in range(-j, j + 1):
            A[m + j, mp + j] = Rotation.D(j, m, mp, alpha, beta, gamma).doit()
    return A


def rotate_tensor(x_float, D):
    """
    Parameters
    ----------
    x_float : numpy.ndarray
        (N, dim, n_tensors)
    D : wignerD matrix

    Returns
    -------

    """
    x_comp = vec2comp(x_float)
    return floatX(comp2vec(np.tensordot(np.conj(D), x_comp, axes=(1, 1)).transpose(1, 0, 2)))


def tensor_coupling(x1_t3, x2_t3, j, j1, j2):
    """

    Parameters
    ----------
    x1_t3 : numpy.ndarray
        (N, dim1, n_tensors)
    x2_t3 : numpy.ndarray
        (N, dim2, n_tensors)
    j : int
        the order of the output tensors
    j1 : int
        the order of x1_t3
    j2 : int
        the order of x2_t3

    Returns
    -------
    out : numpy.ndarray
        (N, dim_out, n_tensors)
    """
    def c_mul(y1, y2):
        """x : (N, 2, n_tensors)"""
        return np.asarray([y1[:, 0]*y2[:, 0] - y1[:, 1]*y2[:, 1],
                             y1[:, 0]*y2[:, 1] + y1[:, 1]*y2[:, 0]]).transpose(1, 0, 2)

    cgt = cg_table(j, j1, j2)
    tensor = []
    for m in range(-j, j+1):
        tmp = 0
        for m1, m2 in [(m1, m2) for m1 in range(-j1, j1+1) for m2 in range(-j2, j2+1) if m == m1+m2]:
            tmp += cgt[m, m1, m2]*c_mul(x1_t3[:, 2*(m1+j1) : 2*(m1+j1)+2], x2_t3[:, 2*(m2+j2) : 2*(m2+j2)+2])
        tensor.append(tmp)
    out = np.concatenate(tensor, axis=1)
    return out


def simple_multi_order_mix(tensor_list, j_input_list, j_output):
    """

    Parameters
    ----------
    tensor_list : list of numpy.ndarray
         (N, dim, 1)
    j_input_list : list of int
    j_output : int

    Returns
    -------
    numpy.ndarray
        (N, dim, n_tensors)

    """
    assert len(tensor_list) == len(j_input_list)
    coupling_list = [(l1, l2) for l1 in j_input_list for l2 in j_input_list
                     if abs(l1-l2) <= j_output <= l1+l2 and l1 <= l2]
    print coupling_list

    output_list = []
    for coupling in coupling_list:
        j1 = coupling[0]
        j2 = coupling[1]
        index_j1 = j_input_list.index(j1)
        index_j2 = j_input_list.index(j2)

        h_tc = tensor_coupling(tensor_list[index_j1], tensor_list[index_j2],  j_output, j1, j2)
        output_list.append(h_tc)

    return np.concatenate(output_list, axis=2)


def multi_order_mix(np_tensor_list, max_j_input, max_j_output):
    """

    Parameters
    ----------
    np_tensor_list : list of numpy.ndarray
        a ndarray has shape (N, dim_l)
    max_j_input : int
    max_j_output : int

    Returns
    -------
    list of numpy.ndarray
        a ndarray has shape (N, dim_l, n_tensors_l)
    list of int

    """
    printf('mixing tensors...')
    j_input_list = range(0, max_j_input + 1, 2)
    j_output_list = range(0, max_j_output + 1, 2)
    out_list = []
    for j in j_output_list:
        printf('mixing ' + str(j_input_list) + '_' + str(j))
        out = simple_multi_order_mix([tensor[:, :, np.newaxis] for tensor in np_tensor_list], j_input_list, j_output=j)
        if j_output_list.index(j) < len(np_tensor_list):
            out = np.concatenate([out, np_tensor_list[j_output_list.index(j)][:, :, np.newaxis]], axis=2)
        out_list.append(out)
    printf('tensors are mixed.')
    return out_list, [tensor.shape[2] for tensor in out_list]


def multi_order_mix_theano_obsolete(np_tensor_list, max_j_input, max_j_output):
    """

    Parameters
    ----------
    np_tensor_list : list of numpy.ndarray
        a ndarray has shape (N, dim_l)
    max_j_input : int
    max_j_output : int

    Returns
    -------
    list of numpy.ndarray
        a ndarray has shape (N, dim_l, n_tensors_l)
    list of int

    """
    printf('mixing tensors...')
    sys.setrecursionlimit(500000)  # for pickle
    j_input_list = range(0, max_j_input + 1, 2)
    j_output_list = range(0, max_j_output + 1, 2)
    tensor_list = [T.tensor3() for _ in range(len(np_tensor_list))]
    out_list = []
    for j in j_output_list:
        printf('mixing ' + str(j_input_list) + '_' + str(j))
        cache_path = os.path.join(os.path.dirname(__file__), 'theano_function_cache',
                                  'multi_order_mix_' + str(j_input_list) + '_' + str(j) + '.pkl')
        if os.path.exists(cache_path):
            printf('loding a cached function...')
            with open(cache_path, 'rb') as f:
                func = cPickle.load(f)
        else:
            printf('compiling ' + 'multi_order_mix_' + str(j_input_list) + '_' + str(j))
            y = blocks.simple_multi_order_mix(tensor_list, j_input_list, j_output=j)
            func = theano.function(inputs=tensor_list, outputs=[y])
            with open(cache_path, 'wb') as f:
                cPickle.dump(func, f, protocol=cPickle.HIGHEST_PROTOCOL)
        out = func(*[tensor[:, :, np.newaxis] for tensor in np_tensor_list])[0]
        out = np.concatenate([out, np_tensor_list[j_output_list.index(j)][:, :, np.newaxis]], axis=2)
        out_list.append(out)
    printf('tensors are mixed.')
    return out_list, [tensor.shape[2] for tensor in out_list]

