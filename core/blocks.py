# -*- coding: utf-8 -*-
"""
Created on Tue Feb 23 20:59:19 2016

@author: matsuik

models.pyにsuperpose_vectorsやnorm_activationなどの低レベルの関数を提供

"""

import numpy as np
import theano
import theano.tensor as T
from theano.ifelse import ifelse
import blind_utils


# theano utils
def floatX(array_):
    """
    Args
    ----
    array_: numpy.ndarray or float

    Returns
    -------
    np.ndarray
    """

    return np.asarray(array_, dtype=theano.config.floatX)


def make_weight(n_input_tensors, n_output_tensors, model=None, name=None, scale=0.01):
    """
    Parameters
    ----------
    n_input_tensors
    n_output_tensors
    scale

    Returns
    -------
    w_mat : theano shared variable
        (n_output_tensors, n_input_tensors)
    """
    u, s, v = np.linalg.svd(np.random.normal(size=(n_output_tensors, n_input_tensors)), full_matrices=False)
    if n_output_tensors >= n_input_tensors:
        mat = u
    else:
        mat = v
    w_mat = theano.shared(
                blind_utils.floatX(scale * mat),
                name=name,
                borrow=False)
    if model is not None:
        model.param_list.append(w_mat)
        model.weight_list.append(w_mat)

    return w_mat


def make_bias(n_output_tensors, model=None, name=None, scale=0.):
    """
    Parameters
    ----------
    n_output_tensors
    scale

    Returns
    -------
    bias_vec : theano shared variable
        (n_output_tensors, )
    """
    bias_vec = theano.shared(
                    blind_utils.floatX(scale*np.ones(shape=(n_output_tensors,))),
                    name=name,
                    borrow=False)
    if model:
        model.param_list.append(bias_vec)

    return bias_vec


def get_act_func(keyword):
    """
    Args
    ----
    keyword : str
        "relu", "retanh", "sigm", "l-retanh"
        
    Returns
    -------
    elementswise scalar f: TensorVariable -> TensorVariable
    """

    if keyword == "linear":
        a = lambda x: x
    elif keyword == "sigmoid":
        a = T.nnet.sigmoid
    elif keyword == "tanh":
        a = T.tanh
    elif keyword == "relu":
        a = lambda x: T.switch(T.gt(x, 0), x, 0)
    elif keyword == "l-relu":
        a = lambda x: T.switch(T.gt(x, 0), x, 0.02*x)
    elif keyword == "elu":
        a = lambda x: T.switch(T.gt(x, 0), x, T.exp(x)-1.)
    elif keyword == "selu":
        a = lambda x: T.switch(T.gt(x, 0), floatX(1.0507)*x, floatX(1.0507*1.6733)*(T.exp(x)-1.))

    elif keyword == 'softmax':
        a = T.nnet.softmax

    elif keyword == "retanh":
        a = lambda x: T.switch(T.gt(x, 0), T.tanh(x), 0)
    elif keyword == "exp-linear":
        a = lambda x: T.switch(T.gt(x, 0), x + 1., T.exp(x))
    elif keyword == "l-retanh":
        a = lambda x: T.switch(T.gt(x, 0), T.tanh(x), 0.01*x)

    elif keyword == "super-tanh":
        a = lambda x: (T.tanh(x-1.)+1.)/2.
    elif keyword == '0.5*sigmoid+0.5':
        a = lambda x: 0.5*T.nnet.sigmoid(x) + 0.5
    else:
        raise Exception('NO_MATCHING_ACT_FUNC')
        
    return a


def get_error_func(metoric_name):
    if metoric_name == 'l2':
        def error_func(pred, gt):
            return T.mean((pred - gt)**2)
    elif metoric_name == 'l1':
        def error_func(pred, gt):
            return T.mean(abs(pred - gt))
    elif metoric_name == 'crossentropy':
        def error_func(pred, gt):
            pred_nn = pred - T.min(pred, axis=1).dimshuffle(0, 'x')
            pred_normed = pred / (T.sum(pred_nn, axis=1) + 10e-8).dimshuffle(0, 'x')
            gt_normed = gt / (T.sum(gt, axis=1) + 10e-8).dimshuffle(0, 'x')
            return T.mean(T.nnet.categorical_crossentropy(pred_normed, gt_normed))
    elif metoric_name == 'predsoftmaxcrossentropy':
        def error_func(pred, gt):
            pred_s = T.nnet.softmax(pred)
            gt_normed = gt / (T.sum(gt, axis=1) + 10e-8).dimshuffle(0, 'x')
            return T.mean(T.nnet.categorical_crossentropy(pred_s, gt_normed))
    elif metoric_name == 'bothsoftmaxcrossentropy':
        def error_func(pred, gt):
            pred_s = T.nnet.softmax(pred)
            gt_s = T.nnet.softmax(gt)
            return T.mean(T.nnet.categorical_crossentropy(pred_s, gt_s))
    elif metoric_name == 'KL':
        def error_func(pred, gt):
            return T.sum(-pred*T.log(gt/pred))
    else:
        raise Exception('NO_MATCHING_ERROR_FUNC')
    return error_func


def get_reg_func(metoric_name):
    if metoric_name == 'l2':
        def func(pred):
            return T.mean(pred**2)
    elif metoric_name == 'l1':
        def func(pred):
            return T.mean(abs(pred))
    elif metoric_name == 'nnl2':
        def func(pred, thresh=0.1):
            #return T.mean(pred[(pred < T.cast(thresh, 'float32')*T.max(pred).dimshuffle(0, 'x')).nonzero()]**2)
            return T.mean(pred[(pred < T.cast(0., 'float32')).nonzero()] ** 2)
    elif metoric_name == 'nnl1':
        def func(pred):
            return  T.mean(abs(pred[(pred < 0).nonzero()]))
    elif metoric_name == 'entropy':
        # entropyはおおきくしたい(ひとつのpeakより, それが分かれてて２つになってるほうがいい)
        # 目的関数を最小化してるんやから, entropyのマイナスを目的関数に加えればいい
        # これは entropyのマイナスを返してる
        def func(pred):
            pred = pred + 10**(-8)
            pred = pred / T.sum(pred)
            return T.sum(pred*T.log(pred))
    else:
        raise Exception('NO_MATCHING_REG_FUNC')
    return func


def superpose_vectors(x_t3, w_mat):
    """
    superpose D-dimansion vectors with scalars
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_input_tensors)
    w_mat : theano.tensor.var.TensorVariable
        (n_ouput_tensors, n_input_tensors)
    
    Returns
    -------
    theano.tensor.var.TensorVariable
        (batch_size, D, n_ouput_tensors)
    """
    
    # sum_k x_ijk w_pk -> y_ijp
    return T.tensordot(x_t3, w_mat, axes=(2, 1))


def activate_norm(x_t3, bias_vec, func_key="relu", eps=1e-8):
    """
    elementwise_scalar_func(||x||+b) * x / (||x|| + eps)
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key)  
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    activation = norm + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3 / (norm.dimshuffle(0, "x", 1) + eps)
    return ouput_t3, acted_norm


def dropout(x_t3, p, is_train, srng):
    """


    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    p : float
    is_train : theano.TensorVariable 
        boolean
    srng : RandomStreams

    Returns
    -------
    symbolic tensor (batch_size, D, n_tensors)
    """

    mask = srng.binomial(n=1, p=p, size=(x_t3.shape[0], x_t3.shape[2]))
    return ifelse(is_train, x_t3 * T.cast(mask/p, theano.config.floatX).dimshuffle(0, "x", 1), x_t3)


def tensor_coupling(x1_t3, x2_t3, j, j1, j2):
    """

    Parameters
    ----------
    x1_t3 : TensorVariable
        (N, dim1, n_tensors)
    x2_t3 : TensorVariable
        (N, dim2, n_tensors)
    j : int
        the order of the output tensors
    j1 : int
        the order of x1_t3
    j2 : int
        the order of x2_t3

    Returns
    -------
    out : TensorVariable
        (N, dim_out, n_tensors)
    """
    def c_mul(y1, y2):
        """x : (N, 2, n_tensors)"""
        return T.stacklists([y1[:, 0]*y2[:, 0] - y1[:, 1]*y2[:, 1],
                             y1[:, 0]*y2[:, 1] + y1[:, 1]*y2[:, 0]]).transpose(1, 0, 2)

    cg_table = blind_utils.cg_table(j, j1, j2)
    tensor = []
    for m in range(-j, j+1):
        tmp = 0
        for m1, m2 in [(m1, m2) for m1 in range(-j1, j1+1) for m2 in range(-j2, j2+1) if m == m1+m2]:
            tmp += cg_table[m, m1, m2]*c_mul(x1_t3[:, 2*(m1+j1) : 2*(m1+j1)+2], x2_t3[:, 2*(m2+j2) : 2*(m2+j2)+2])
        tensor.append(tmp)
    out = T.concatenate(tensor, axis=1)
    return out


def multi_order_mix(tensor_list, j_input_list, j_output, n_input_tensors_list, n_output_tensors,
                    is_train, func_key, weight_list=None, bias_list=None):
    """

    Parameters
    ----------
    tensor_list : list of TensorVariable
        a Tensorbariable has shape (N, dim, n_tensors)
    j_input_list : list of int
    j_output : int
    n_input_tensors_list : list of int
    n_output_tensors : int
    is_train : bool
    func_key : str
    weight_list : list of SharedVariable
    bias_list : list of SharedVariable

    Returns
    -------

    """
    assert len(tensor_list) == len(j_input_list) == len(n_input_tensors_list)
    coupling_list = [(l1, l2) for l1 in j_input_list for l2 in j_input_list
                     if abs(l1-l2) <= j_output <= l1+l2 and l1 <= l2]
    print coupling_list

    output_list = []
    if is_train:
        weight_list = []
        bias_list = []
        for coupling in coupling_list:
            j1 = coupling[0]
            j2 = coupling[1]
            index_j1 = j_input_list.index(j1)
            index_j2 = j_input_list.index(j2)

            W1 = make_weight(n_input_tensors_list[index_j1], n_output_tensors,
                             name='W_{}_{}'.format(coupling[0], coupling))
            W2 = make_weight(n_input_tensors_list[index_j2], n_output_tensors,
                             name='W_{}_{}'.format(coupling[1], coupling))
            weight_list.append(W1)
            weight_list.append(W2)
            bias = make_bias(n_output_tensors, name='bias_{}'.format(coupling))
            bias_list.append(bias)

            h1 = superpose_vectors(tensor_list[index_j1], W1)
            h2 = superpose_vectors(tensor_list[index_j2], W2)

            h_tc = tensor_coupling(h1, h2,  j_output, j1, j2)

            h_na = activate_norm(h_tc, bias, func_key)[0]

            output_list.append(h_na)

        return T.concatenate(output_list, axis=2), weight_list, bias_list, len(coupling_list)

    else:
        for i, coupling in enumerate(coupling_list):
            j1 = coupling[0]
            j2 = coupling[1]
            index_j1 = j_input_list.index(j1)
            index_j2 = j_input_list.index(j2)

            W1 = weight_list[2*i]
            W2 = weight_list[2*i + 1]
            bias = bias_list[i]

            h1 = superpose_vectors(tensor_list[index_j1], W1)
            h2 = superpose_vectors(tensor_list[index_j2], W2)

            h_tc = tensor_coupling(h1, h2,  j_output, j1, j2)

            h_na = activate_norm(h_tc, bias, func_key)[0]

            output_list.append(h_na)

        return T.concatenate(output_list, axis=2)


def simple_multi_order_mix(tensor_list, j_input_list, j_output, model=None):
    """

    Parameters
    ----------
    tensor_list : list of TensorVariable
        a Tensorvariable has shape (n_samples, dim_j, 1) for a j
    j_input_list : list of int
    j_output : int
    model : response_models.Model

    Returns
    -------
    TensorVariable
        (N, dim, n_tensors)

    """
    assert len(tensor_list) == len(j_input_list)
    coupling_list = [(l1, l2) for l1 in j_input_list for l2 in j_input_list
                     if abs(l1-l2) <= j_output <= l1+l2 and l1 <= l2]
    print coupling_list

    output_list = []
    for coupling in coupling_list:
        j1 = coupling[0]
        j2 = coupling[1]
        index_j1 = j_input_list.index(j1)
        index_j2 = j_input_list.index(j2)

        h_tc = tensor_coupling(tensor_list[index_j1], tensor_list[index_j2],  j_output, j1, j2)
        output_list.append(h_tc)

    if model is not None:
        model.n_input_tensors_list.append(len(coupling_list))

    return T.concatenate(output_list, axis=2)


### batch nomalization

    
def activate_norm_bn(x_t3, model, gamma_vec, bias_vec, beta, t, n_hidden, func_key, eps=1e-8):
    """
    n = ||x||
    activation = (n - E[n]) / sqrt(V[n]+eps) 
    activate(gamma*n + b) * x / (n + eps)
    activationをDで割らない
    
    mean, varは指数的に最近のをみて推定
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    gamma_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    beta : symbolic scalar
        平均, 分散の推定につかう
    t : symbolic scalar
        timestep, 平均、分散の推定のバイアスを取るために使う(Adamぽく)
    ac_mean : shared variable
        (n_tensors,) 平均の推定値
    ac_var : shared variable
        (n_tensors, ) 分散の推定値  
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computational graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    ac_mean = make_bias(n_hidden, scale=0)
    ac_var = make_bias(n_hidden, scale=1)

    act_func = get_act_func(func_key)  
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    
    batch_mean = T.mean(norm, axis=0)
    batch_var = T.var(norm, axis=0) # (n_tensors,)
    mean = beta*batch_mean + (1.-beta)*ac_mean
    var = beta*batch_var + (1.-beta)*ac_var
    
    activation = (norm - ac_mean/(1. - beta**t)) / T.sqrt(ac_var/(1. - beta**t)+eps) * gamma_vec + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3 / (norm.dimshuffle(0, "x", 1) + eps)
                
    model_updates = dict()
    model_updates[ac_mean] = mean
    model_updates[ac_var] = var
    model.model_updates.update(model_updates)
    # tは外でupdateする
    
    return ouput_t3


def activate_norm_bn_test(x_t3, gamma_vec, bias_vec, ac_mean, ac_var, func_key, eps=1e-8):
    """
    elementwise_scalar_func(||x||+b) * x / (||x|| + eps)
    
    Args
    ----
    x_t3 : theano.tensor.var.TensorVariable
        (batch_size, D, n_tensors)
    gamma_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    bias_vec : theano.tensor.var.TensorVariable
        (n_tensors, )
    ac_mean : shared variable
        (n_tensors,) 平均の推定値
    ac_var : shared variable
        (n_tensors, ) 分散の推定値  
    func_key : str
        see theano_utils.get_act_func
    eps : float
        small number prevents zerodivision
        
    Returns
    -------
    output_t3 : a node of computational graph
        (batch_size, D, n_tensors)
    acted_norm : a node of computaional graph
        (batch_size, n_tensors)
        これじゃなくて activationを見るべき
    
    """
    
    act_func = get_act_func(func_key)  
    
    norm = x_t3.norm(L=2, axis=1)  # (batch_size, n_tensors)
    
    activation = (norm - ac_mean) / T.sqrt(ac_var+eps) * gamma_vec + bias_vec  # (batch_size, n_tensors)
    acted_norm = act_func(activation)  # (batch_size, n_tensors)
    ouput_t3 = acted_norm.dimshuffle(0, "x", 1) * x_t3 / (norm.dimshuffle(0, "x", 1) + eps)
    return ouput_t3, acted_norm
    
    

