# -*- coding:utf-8 -*-
"""
blind_deconv/simulation/b3000_uniform/test/angle60ratio1/[csd, rcn]_results/[model]
の中で実行してる
"""
import os
import argparse
import scipy.io as sio

import sys

import visualization
import blind_utils

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


def evaluate_pred(pred, angleX_dirpath, to_visialize=False):
    bdirs, ch = visualization.load_bdirs_ch(pred.shape[1])
    if 'rotated' not in angleX_dirpath:
        peak_value_list_list, peak_dir_list_list = blind_utils.localmax(pred, n_dirs=pred.shape[1])
    else:
        ms = sio.loadmat(os.path.join(angleX_dirpath, 'data/data_base.mat'))
        euler_angles = ms['euler_angles']
        del ms
        bdirs = blind_utils.rotate_back_bdirs(bdirs, euler_angles, pred.shape[0]//euler_angles.shape[0])
        peak_value_list_list, peak_dir_list_list = blind_utils.localmax_rotback(pred, bdirs)

    success_rate, n_spurious, success_mask = blind_utils.sucess_rate_sprious_peak(peak_dir_list_list)

    for peak_value_list in peak_value_list_list:
        if len(peak_value_list) == 0:
            del peak_value_list
    for peak_dir_list in peak_dir_list_list:
        if len(peak_dir_list) == 0:
            del peak_dir_list
    if peak_dir_list_list == []:
        raise Exception('ERROR: PRED ARE ALL ZERO')

    n1, n2 = blind_utils.load_gt_dir(os.path.join(angleX_dirpath, 'data'))
    chosen_peak_list, biases, stds, biases_all, stds_all = blind_utils.eval_peak_dirs(peak_value_list_list, peak_dir_list_list, n1, n2)
    angle_tolerance = 10
    success_rate_tol, biases_tol, stds_tol = blind_utils.eval_peak_dirs_with_tolerance(peak_dir_list_list, n1, n2, angle_tolerance)

    value_list = [success_rate, n_spurious, biases[0], biases[1], stds[0], stds[1],
                  biases_all[0], biases_all[1], stds_all[0], stds_all[1],
                  success_rate_tol, biases_tol[0], biases_tol[1], stds_tol[0], stds_tol[1],
                  angle_tolerance]
    if to_visialize:
        return value_list, success_mask, chosen_peak_list, peak_value_list_list, peak_dir_list_list, n1, n2, bdirs, ch
    else:
        return value_list


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('test_dir', action='store', type=str)  # fullpath to angleX0ratioY
    parser.add_argument('result_dir', action='store', type=str)  # fullpath to angleXratioY/trained_model dir
    args = parser.parse_args()
    assert os.path.exists(args.test_dir)
    assert os.path.exists(args.result_dir)

    print 'evaluating ' + os.path.join(args.result_dir, 'pred.mat')
    print args.result_dir
    sys.stdout.flush()

    ms = sio.loadmat(os.path.join(args.result_dir, 'pred.mat'))
    pred = ms['pred']

    value_list, success_mask, chosen_peak_list, peak_value_list_list, peak_dir_list_list, n1, n2, bdirs, ch = \
        evaluate_pred(pred, args.test_dir, to_visialize=True)

    key_list = ['success_rate', 'n_spurious_peaks', 'bias1', 'bias2', 'std1', 'std2',
                'bias1_all', 'bias2_all', 'std1_all', 'std2_all',
                'success_rate_tol', 'biases_tol1', 'biases_tol2', 'stds_tol1', 'stds_tol2',
                'angle_tolerance']
    with open(os.path.join(args.result_dir, 'summary.txt'), 'w') as f:
        f.writelines(['{} {}\n'.format(key, value) for (key, value) in zip(key_list, value_list)])

    visualization.tile_visualization(pred, success_mask, chosen_peak_list, peak_value_list_list, peak_dir_list_list,
                                     n1, n2, bdirs, ch)
    plt.savefig(os.path.join(args.result_dir, 'nine_fods.png'))

    os.remove(os.path.join(args.result_dir, 'pred.mat'))



