# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams
from datetime import datetime

from blind_utils import floatX
import blocks, blind_utils


def compile(train_feature_vec_list, train_gt_vec, test_feature_vec_list, test_gt_vec, model, optimizer):
    """
    データをsharedにして、modelとoptimizerを使ってcomputational graphを作って、
    コンパイルする。

    Parameters
    -----------
    train_feture_vec_list : list of numpy.ndarray
        list of (N, dim, n_tensors)
    train_gt_vec : numpy.ndarray
        (N, n_dir)
    test_feature_vec_list, test_gt_vec
    model : models.Rcn1layerとか
    optimizer : optimizers.SGDとか

    """

    def share_data(data):
        return theano.shared(floatX(data), borrow=True)

    s_train_feature_vec_list = [share_data(data) for data in train_feature_vec_list]
    s_train_gt_vec = share_data(train_gt_vec)
    s_test_feature_vec_list = [share_data(data) for data in test_feature_vec_list]
    s_test_gt_vec = share_data(test_gt_vec)

    model.make_graph_train()

    updates = optimizer.make_updates(loss=model.obj, param_list=model.param_list)
    updates.update(model.model_updates)

    i_batch = T.iscalar("i_batch")
    index_list = T.ivector("index_list")
    batch_size = T.iscalar("batch_size")

    # for input : 対応するLのテンソルをもってきて、index_listでpermutateして、batchを切り出す
    # and for target : index_listでpermutateして、batchを切り出す
    train_givens = [(tensor,
                     s_train_feature_vec_list[i][index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])
                    for i, tensor in enumerate(model.tensors_list)] + \
                   [(model.t_mat, s_train_gt_vec[index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])]
    test_givens = [(tensor,
                    s_test_feature_vec_list[i][index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])
                   for i, tensor in enumerate(model.tensors_list)] + \
                  [(model.t_mat, s_test_gt_vec[index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])]

    train_givens.append((model.is_train, T.cast(1, 'int32')))  # for dropout : is_trainでdropout内のtrainとtestを切り替えてる

    f_train = theano.function(
        inputs=[i_batch, index_list, batch_size],
        updates=updates,
        givens=train_givens,
        on_unused_input='warn'
    )

    f_training_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=train_givens,
        on_unused_input='warn'
    )

    # modelは一緒なのでgivesだけ変えてる
    test_givens.append((model.is_train, T.cast(0, 'int32')))
    f_test_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=test_givens,
        on_unused_input='warn'
    )

    # modelは一緒なのでtrainのoutつかってる
    # test_out.shape (N, n_dir)
    f_output = theano.function(
        inputs=model.tensors_list,
        outputs=[model.train_out],
        givens=[(model.is_train, T.cast(0, 'int32'))],
        on_unused_input='warn'
    )

    result = [f_train, f_training_error, f_test_error, f_output, model.param_list]
    return result


class Model(object):
    def __init__(self, j_input_list=[], j_output_list=[], n_input_tensors_list=[], n_hidden=0, func_key_list=None,
                 error_func_key='l2', reg_lambda=0., reg_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 after_tensor_coupling=True, default_bias=0., dropout_p=1.0, weight_decay_lambda=0.,
                 weight_decay_func_key='l2', n_layer=0):
        self.j_input_list = j_input_list
        self.j_output_list = j_output_list

        self.error_func_key = error_func_key
        self.reg_func_key = reg_func_key
        self.pc_func_key = pc_funk_key
        self.reg_lambda = floatX(reg_lambda)
        self.n_input_tensors_list = n_input_tensors_list
        self.n_hidden = n_hidden

        if func_key_list is None:
            func_key_list = [None]
        self.func_key_list = func_key_list
        self.default_bias = default_bias

        self.tensors_list = [T.tensor3(name='input_tensor' + str(j)) for j in j_input_list]
        self.t_mat = T.matrix("target")  # (batchsize, n_dir)
        self.param_list = []
        self.M = blind_utils.load_Mreconstruct(self.j_output_list[-1], n_dirs)
        # (n_dir, dim_concat), L=0もはいってる, 全部のLのがconcatenateされてる
        self.after_tensor_coupling = after_tensor_coupling

        self.out_list = []  # for each L's output : to be concatenated

        self.model_updates = dict()  # for batch normalization
        self.is_train = T.iscalar()

        self.weight_decay_lambda = weight_decay_lambda
        self.weight_decay_func_key = weight_decay_func_key
        self.weight_list = []
        self.n_layer = n_layer

    def __str__(self):
        model_description = '_'.join([self.__class__.__name__, 'nlayer'+str(self.n_layer),
                                      'jin'+str(self.j_input_list[-1]), 'jout'+str(self.j_output_list[-1]),
                                      'act'+str(self.func_key_list[0]), 'bias' + str(self.default_bias),
                                      'wdl' + str(self.weight_decay_lambda),
                                      'error' + self.error_func_key,
                                      ''.join(datetime.now().isoformat().split(':'))])
        return model_description

    def make_graph_head(self):
        pass

    def concatenate(self):
        out_concat = T.concatenate(self.out_list, axis=1)[:, :, 0]  # (N, dim_concat, 1) to (N, dim_concat)
        pred_reconst = T.dot(out_concat, self.M.T)  # (N, n_dirs)
        pred = blocks.get_act_func(self.pc_func_key)(pred_reconst)

        error_func = blocks.get_error_func(self.error_func_key)
        error = error_func(pred, self.t_mat)
        reg_func = blocks.get_reg_func(self.reg_func_key)
        weight_decay_func = blocks.get_reg_func(self.weight_decay_func_key)
        obj = error + self.reg_lambda * reg_func(pred) \
              + self.weight_decay_lambda * T.sum([weight_decay_func(w) for w in self.weight_list])

        self.obj = obj
        self.train_error = error
        self.train_out = pred

    def make_graph_train(self):
        self.make_graph_head()
        self.concatenate()


class ScalarProducts(Model):
    def __init__(self, j_input_list, j_output_list, after_tensor_coupling=False,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0.,  ):
        l = locals()
        del l['self']
        super(ScalarProducts, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for i, tensor in enumerate(self.tensors_list):
            w = blind_utils.zero_shared(1)
            self.param_list.append(w)

            h = w * tensor
            out_list.append(h)
        self.out_list = out_list


class LinearCombination(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0., ):
        assert len(j_input_list) == len(n_input_tensors_list)
        l = locals()
        del l['self']
        super(LinearCombination, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            weight = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                        1, self, name='W{}'.format(j_output))

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], weight)
            out_list.append(h1sv)
        self.out_list = out_list


class NonlinearOneLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, func_key_list,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 1
        l = locals()
        del l['self']
        super(NonlinearOneLayer, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            weight = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                        1, self, name='W{}'.format(j_output))
            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], weight)

            bias = blocks.make_bias(1, self, name='bias{}'.format(j_output))
            h1na = blocks.activate_norm(h1sv, bias, func_key=self.func_key_list[0])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h1na * w_scalar)
        self.out_list = out_list


class MultiLayer(Model):
    def __init__(self, n_layer, j_input_list, j_output_list, n_input_tensors_list, n_hidden, func_key_list,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0., ):
        assert len(j_input_list) == len(n_input_tensors_list)
        l = locals()
        del l['self']
        super(MultiLayer, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output))
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            for i_layer in range(2, self.n_layer):
                w = blocks.make_weight(self.n_hidden,
                                       self.n_hidden, self, name='W{}{}'.format(i_layer, j_output))
                h = blocks.superpose_vectors(h, w)

                b = blocks.make_bias(self.n_hidden, self, name='bias{}{}'.format(i_layer, j_output))
                h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h * w_scalar)
        self.out_list = out_list


class DropoutThreeLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, n_hidden, func_key_list, dropout_p,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0.):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 3
        l = locals()
        del l['self']
        super(DropoutThreeLayer, self).__init__(**l)

        self.dropout_p = dropout_p
        self.srng = RandomStreams(seed=1999)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]
            h = blocks.dropout(h, self.dropout_p, self.is_train, self.srng)

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias2{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[1])[0]
            h = blocks.dropout(h, self.dropout_p, self.is_train, self.srng)

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self, scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[2])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)
            out_list.append(h * w_scalar)

        self.out_list = out_list


class BnThreeLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, n_hidden, func_key_list,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0.):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 3
        l = locals()
        del l['self']
        super(BnThreeLayer, self).__init__(**l)

        self.t = theano.shared(blind_utils.floatX(1))
        self.model_updates = dict()
        self.model_updates[self.t] = self.t + 1

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, scale=self.default_bias)
            gamma = blocks.make_bias(self.n_hidden, self, scale=1)
            h = blocks.activate_norm_bn(h, self, gamma_vec=gamma, bias_vec=b, beta=0.9,
                                        t=self.t,
                                        n_hidden=self.n_hidden,
                                        func_key=self.func_key_list[0], eps=1e-8)

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias2{}'.format(j_output), scale=self.default_bias)
            gamma = blocks.make_bias(self.n_hidden, self, scale=1)

            h = blocks.activate_norm_bn(h, self, gamma_vec=gamma, bias_vec=b, beta=0.9,
                                        t=self.t,
                                        n_hidden=self.n_hidden,
                                        func_key=self.func_key_list[0], eps=1e-8)

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self, scale=self.default_bias)
            gamma = blocks.make_bias(1, self, scale=1)
            h = blocks.activate_norm_bn(h, self, gamma_vec=gamma, bias_vec=b, beta=0.9,
                                        t=self.t,
                                        n_hidden=1,
                                        func_key=self.func_key_list[0], eps=1e-8)

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)
            out_list.append(h * w_scalar)

        self.out_list = out_list

class Res(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, n_hidden, func_key_list,
                 reg_lambda=0., reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):
        assert len(j_input_list) == len(n_input_tensors_list)
        assert len(func_key_list) == 4
        l = locals()
        del l['self']
        super(Res, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            # res
            b = blocks.make_bias(self.n_hidden, self, name='bias1res{}'.format(j_output))
            hres = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2res{}'.format(j_output))
            hres = blocks.superpose_vectors(hres, w)
            b = blocks.make_bias(self.n_hidden, self, name='bias2res{}'.format(j_output))
            hres = blocks.activate_norm(hres, b, func_key=self.func_key_list[1])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W3res{}'.format(j_output))
            hres = blocks.superpose_vectors(hres, w)
            # end res

            h = h + hres

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[3])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h * w_scalar)
        self.out_list = out_list


# tensor coupling前の重みも学習してる
class Rcn2layer_pc(Model):
    def __init__(self, j_input_list, j_output_list, n_input_tensors_list, n_hidden, func_key_list):
        assert len(j_input_list) == len(n_input_tensors_list)
        l = locals()
        del l['self']
        super(Rcn2layer_pc, self).__init__(**l)

    def make_graph_head(self):
        out2_list = []
        for j_output in self.j_output_list:
            param_dict = {}
            out1, self.tc_weight_list, self.tc_bias_list, n_couples = \
                blocks.multi_order_mix(self.tensors_list, self.j_input_list, j_output,
                                       self.n_input_tensors_list, self.n_hidden, is_train=True,
                                       func_key=self.func_key_list[0])
            param_dict['tc_weight_list'] = self.tc_weight_list
            param_dict['tc_bias_list'] = self.tc_bias_list
            self.param_list.extend(self.tc_weight_list)
            self.param_list.extend(self.tc_bias_list)

            if j_output in self.j_input_list:
                self.W1 = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                             self.n_hidden, name='W1')
                self.bias1 = blocks.make_bias(self.n_hidden, name='bias1')
                param_dict['W1'] = self.W1
                param_dict['bias1'] = self.bias1
                self.param_list.append(self.W1)
                self.param_list.append(self.bias1)

                h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)],
                                                self.W1)
                h1na = blocks.activate_norm(h1sv, self.bias1, func_key=self.func_key_list[0])[0]
                out1 = T.concatenate([out1, h1na], axis=2)

                n_couples += 1

            self.W2 = blocks.make_weight(n_couples * self.n_hidden, 1, name='W2')
            self.bias2 = blocks.make_bias(1, name='bias2')
            param_dict['W2'] = self.W2
            param_dict['bias2'] = self.bias2
            self.param_list.append(self.W2)
            self.param_list.append(self.bias2)

            self.param_dict_list.append(param_dict)

            h2 = blocks.superpose_vectors(out1, self.W2)
            # out2.shape = (N, dim, n_tensors=1)
            out2, acted_norm2 = blocks.activate_norm(h2, self.bias2, self.func_key_list[1])
            out2_list.append(out2)
        self.out_list = out2_list
