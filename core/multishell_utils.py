# -*- coding: utf-8 -*-
from __future__ import division
import os
import numpy as np
import scipy.io as sio
import h5py
import blind_utils

opj = os.path.join

def load_fodf_reconst(dir_path):
    """
    
    Parameters
    ----------
    dir_path

    Returns
    -------
    fodf_reconst : numpy.ndarray
        shape (n_samples, n_discretize_dirs)

    """
    file_path = opj(dir_path, 'data_base.mat')
    with h5py.File(file_path) as f:
        fodf_reconst = np.array((f['output_raw']))
    return fodf_reconst


def load_hardi_coupled_sh(dir_path):
    """
    
    Parameters
    ----------
    file_path
    
    Returns
    -------
    concat_feature_list : list
        of numpy.ndarray shape (n_samples, dim_l, n_tensors)
    n_tensors_list : list
        of int
    
    """
    def squeeze_comp2vec(feature_):
        """
        
        Parameters
        ----------
        feature_ : numpy.ndarray
            shape (2, dim_l/2, 1, 1, n_samples)

        Returns
        -------
        numpy.ndarray
            shape (dim_l, n_samples)

        """
        sq = feature_[:, :, 0, 0, :]
        retmat = np.zeros((sq.shape[1] * 2, sq.shape[-1]))
        retmat[0::2] = sq[0]
        retmat[1::2] = sq[1]
        return retmat

    file_path = opj(dir_path, 'fields.mat')
    mat = sio.loadmat(file_path)

    index_list = []
    for l in (0, 2, 4, 6):
        index_list.append([i for i in range(49) if mat['fields'][0, i][0, 0][2] == l])

    feature_list = [squeeze_comp2vec(mat_) for mat_ in [mat['fields'][0, i_feature][0, 0][3] for i_feature in range(49)]]

    feature_list_list = []
    for l in range(len(index_list)):
        feature_list_list.append([feature_list[i] for i in index_list[l]])

    concat_feature_list = [
        np.concatenate([feature[:, :, np.newaxis] for feature in a_list], axis=2).transpose((1, 0, 2))
        for a_list in feature_list_list]
    return concat_feature_list, [a.shape[2] for a in concat_feature_list]


def preprocess(input_tensors, fodf, n_all=None):
    """
    
    Parameters
    ----------
    input_tensors : list
        of ndarray shape (n_samples, dim_l, n_tensors)
        from load_hardi_coupled_sh
    fodf : ndarray
        (n_samples, n_discretize_dirs)
        from load_fodf_reconst
    n_all
    Returns
    -------
    list of ndarray:
        l-th element has shape (N, dim, n_tensors)
    ndarray:
        (N, n_dir)
    list of ndarray:
        l-th element has shape (N, dim, n_tensors)
    ndarray:
        (N, n_dir)
    input_coef : list of ndarray
        (n_tensors, )
    """
    train_ratio = 0.8

    if n_all is None:
        n_all = input_tensors[0].shape[0]
        print 'n_all', n_all
    n_train = int(n_all * train_ratio)
    n_test = n_all - n_train
    permu_index = np.random.permutation(range(input_tensors[0].shape[0]))

    def separate_train_test(array_):
        train_ = array_[permu_index][:n_train]
        test_ = array_[permu_index][n_train:n_train + n_test]
        return train_, test_

    def normalize(tens_list):
        """

        Parameters
        ----------
        tens_list : list of numpy.ndarray
            a array has shape (N_all, n_tensors, dim) for normalize_ex2_feature_vec
        Returns
        -------
        list of numpy.ndarray
            a array has shape (N_train, dim, n_tensors)
        list of numpy.ndarray
            a array has shape (N_test, dim, n_tensors)
        list of numpy.ndarray
            a array has shape (n_tensors, )

        """
        train_normed_list = []
        test_normed_list = []
        coef_list = []
        for tens in tens_list:
            train_normed, coef = blind_utils.normalize_ex2_feature_vec(tens[permu_index][:n_train])
            train_normed_list.append(train_normed.transpose(0, 2, 1))
            coef_list.append(coef)
            test_normed = blind_utils.normalize_vec(tens[permu_index][n_train:n_train + n_test], coef)
            test_normed_list.append(test_normed.transpose(0, 2, 1))

        return train_normed_list, test_normed_list, coef_list

    input_tensors = [tensor.transpose(0, 2, 1) for tensor in input_tensors]
    input_train_list, input_test_list, input_coef_list = normalize(input_tensors)
    fodf[fodf < 0.] = 0.
    fodf = fodf / np.sum(fodf, axis=1)[:, np.newaxis]
    train_fodf, test_fodf = separate_train_test(fodf)

    return input_train_list, train_fodf, input_test_list, test_fodf, input_coef_list
