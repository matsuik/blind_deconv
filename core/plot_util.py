import os

import blind_utils


from pprint import pprint

import glob

import matplotlib.pyplot as plt


import numpy as np

def select_model(simulation_dir, list_include, list_exclude=None):
    if list_exclude is None:
        list_exclude = []

    simulation_dir_name = os.path.basename(os.path.normpath(simulation_dir))
    list_model = glob.glob('../simulation/{}/trained_models/*'.format(simulation_dir_name))
    list_selected_model = []
    for m in list_model:
        for model_key in list_include:
            if model_key in m:
                list_selected_model.append(m)
                break
    for except_key in list_exclude:
        list_selected_model = sorted([model for model in list_selected_model if except_key not in model])

    if all(['lmax' in model for model in list_selected_model]):
        list_selected_model = sorted(list_selected_model, key=lambda name: float(name.split('reg')[1]))

    return list_selected_model

def load_df(simulation_dir_name, model, measure_name):
    try:
        df = blind_utils.df_over_angles(simulation_dir_name, model, rotated=True)
    except IOError as e:
        print e
        return

    if df is None:
        return

    if measure_name not in df.keys():
        print 'ERROR: INVALID MEASURE_NAME : {}'.format(measure_name)
        print 'valid keys'
        pprint(df.keys())
        return

    if measure_name == 'bias1_all':
        array_plot = (df.loc[:, ['bias1_all']].values.flatten() + df.loc[:, ['bias2_all']].values.flatten()) / 2
    elif measure_name == 'std1':
        array_plot = (df.loc[:, ['std1_all']].values.flatten() + df.loc[:, ['std2_all']].values.flatten()) / 2
    else:
        array_plot = df.loc[:, [measure_name]].values.flatten()

    return array_plot

def parse_model_name(model_name):
    if 'Scalar' in model_name:
        label = 'Scalar'
    elif 'Linear' in model_name:
        label = 'Linear'
    elif 'nlayer2' in model_name:
        label = 'Layer2'
    elif 'nlayer4' in model_name:
        label = 'Layer4'
    else:
        label = os.path.basename(os.path.normpath(model_name))
    return label

def plot_over_angle(simulation_dir_name, measure_name, list_include, list_exclude=None):
    list_selected_model = select_model(simulation_dir_name, list_include, list_exclude)
    for model in list_selected_model:

        array_plot = load_df(simulation_dir_name, model, measure_name)
        if array_plot is None:
            continue


        label = parse_model_name(model)
        plt.plot(np.arange(20, 91, 10), array_plot, label=label)

        plt.xticks(np.arange(20, 91, 10), np.arange(20, 91, 10))
        plt.tick_params(labelsize=16)

        plt.xlabel("Angles (degree)", fontsize=18)

        plt.grid(linestyle='dashed')

    plt.legend(fontsize=16, bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)
    measure_name_dict = {u'bias1_all':u'Bias (degree)', u'n_spurious_peaks':u'# of false positive peaks',
                         u'success_rate_tol':u'Success rate'}
    plt.ylabel(measure_name_dict[measure_name], fontsize=18)
    if measure_name == u'n_spurious_peaks':
        plt.ylim([-0.2, 3])
    elif measure_name == u'bias1_all':
        plt.ylim([-1, 20])
    elif measure_name == u'success_rate_tol':
        plt.ylim([-0.05, 1])


def plot_random_seeds_over_angle(simulation_dir_name_list, measure_name, list_include, list_exclude=None, note_list=None):
    for simulation_dir_name in simulation_dir_name_list:
        for i_model, model_name in enumerate(list_include):
            list_selected_model = select_model(simulation_dir_name, [model_name], list_exclude)
            array_plot_list = []
            for model in list_selected_model:
                array_plot = load_df(simulation_dir_name, model, measure_name)
                if array_plot is None:
                    continue
                array_plot_list.append(array_plot)

            label = parse_model_name(model)
            if note_list is not None:
                label += note_list[i_model]
            if len(simulation_dir_name_list) > 1:
                label = simulation_dir_name + label

            mean = np.mean(array_plot_list, axis=0)
            std = np.std(array_plot_list, axis=0)

            plt.plot(np.arange(20, 91, 10), mean, label=label)
            plt.fill_between(np.arange(20, 91, 10), mean + std, mean - std, alpha=0.3)

            plt.xticks(np.arange(20, 91, 10), np.arange(20, 91, 10))
            plt.tick_params(labelsize=16)

            plt.xlabel("Angles (degree)", fontsize=18)

            plt.grid(linestyle='dashed')

    plt.legend(fontsize=16, bbox_to_anchor=(1.05, 1), loc='upper left', borderaxespad=0)
    measure_name_dict = {u'bias1_all':u'Bias (degree)', u'n_spurious_peaks':u'# of false positive peaks',
                         u'success_rate_tol':u'Success rate'}
    plt.ylabel(measure_name_dict[measure_name], fontsize=18)

    if measure_name == u'n_spurious_peaks':
        plt.ylim([-0.05, 1])
    elif measure_name == u'bias1_all':
        plt.ylim([-1, 10])
    elif measure_name == u'success_rate_tol':
        plt.ylim([-0.05, 1])



def plot_over_simulation(measure_name, angle_list, list_include, list_exclude=None):
    list_simulation_dir = ['b3000_gauss_15_1272_snr20', 'b3000_gaussian_snr20_dw30', 'b3000_gaussian_snr20_dw90']
    list_selected_model = select_model(list_simulation_dir[0], list_include, list_exclude)

    fig = plt.figure(figsize=(4,12))
    fig.suptitle(measure_name)

    ax_list = []
    for i, angle in enumerate(angle_list):
        ax = fig.add_subplot(len(angle_list) * 100 + 10 + i + 1)
        ax.set_title('angle={}'.format(angle))
        plt.xticks([0, 1, 2], ['15', '30', '90'])
        ax_list.append(ax)

    for model in list_selected_model:
        array_per_simulation = []
        for simulation_dir in list_simulation_dir:
            array_per_simulation.append(get_value(simulation_dir, model, angle_list, measure_name))
        tmp = np.asarray(array_per_simulation)
        tmp = tmp.T
        for i, angle in enumerate(angle_list):
            ax_list[i].plot([0., 1., 2.], tmp[i], label=os.path.basename(os.path.normpath(model)))

    for ax in ax_list:
        ax.legend(bbox_to_anchor=(1.05, 1.0), loc='upper left', borderaxespad=0)


def get_value(simulation_dir, model_name, angle_list, measure_name):
    angle_list = [str(angle) for angle in angle_list]
    df = blind_utils.df_over_angles(simulation_dir, model_name
                                                    , rotated=True)
    try:
        df = blind_utils.df_over_angles(simulation_dir, model_name, rotated=True)
    except IOError as e:
        print e

    if df is None:
        return

    if measure_name not in df.keys():
        print 'ERROR: INVALID MEASURE_NAME : {}'.format(measure_name)
        print 'valid keys'
        pprint(df.keys())
        return
    if measure_name == 'bias1_all':
        array_plot = (df.loc[angle_list, ['bias1_all']].values.flatten() + df.loc[angle_list, ['bias2_all']].values.flatten()) / 2
    elif measure_name == 'std1_all':
        array_plot = (df.loc[angle_list, ['std1_all']].values.flatten() + df.loc[angle_list, ['std2_all']].values.flatten()) / 2
    else:
        array_plot = df.loc[angle_list, [measure_name]].values.flatten()
    return array_plot


list_major_measure = [u'bias1_all', u'n_spurious_peaks', u'success_rate_tol']


def show_major_measures(simulation_dir, list_include, list_exclude):
    plt.figure(figsize=(6,20))
    for i, measure in enumerate(list_major_measure):
        plt.subplot(410+i+1)
        plot_over_angle(simulation_dir, measure, list_include, list_exclude)
    plt.subplots_adjust(hspace=0.3)


def show_major_measures_random_seeds(simulation_dir_list, list_include, list_exclude, note_list=None):
    plt.figure(figsize=(6,18))
    for i, measure in enumerate(list_major_measure):
        plt.subplot(410+i+1)
        plot_random_seeds_over_angle(simulation_dir_list, measure, list_include, list_exclude, note_list=note_list)
    plt.subplots_adjust(hspace=0.3)