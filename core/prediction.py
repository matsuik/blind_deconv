# -*- coding: utf-8 -*-
from __future__ import print_function
import os
os.environ['THEANO_FLAGS'] = 'device=gpu'
import argparse
import gzip
import cPickle
import scipy.io as sio
import hdf5storage

import blind_utils

opj = os.path.join

parser = argparse.ArgumentParser()
parser.add_argument('test_dir', action='store', type=str)  # fullpath to angleX0ratioY
parser.add_argument('model_dir', action='store', type=str)  # fullpath
parser.add_argument('--no_non_negative', dest='non_negative', action='store_false', default=True)
parser.add_argument('--softmax', action='store_true', default=False)
parser.add_argument('--minusmin', action='store_true', default=False)
parser.add_argument('--v73', action='store_true', default=False)
args = parser.parse_args()
assert os.path.exists(args.test_dir)
assert os.path.exists(args.model_dir)

with gzip.open(os.path.join(args.model_dir, 'train_args.pkl.gz'), 'rb') as f:
    train_args = cPickle.load(f)
with gzip.open(os.path.join(args.model_dir, 'f_output.pkl.gz'), 'rb') as f:
    f_output = cPickle.load(f)
with gzip.open(os.path.join(args.model_dir, 'after_tensor_coupling.pkl.gz'), 'rb') as f:
    after_tensor_coupling = cPickle.load(f)
with gzip.open(os.path.join(args.model_dir, 'norm_coef.pkl.gz'), 'rb') as f:
    norm_coef = cPickle.load(f)

if 'multishell' in args.test_dir:
    import multishell_utils
    input_tensors, n_input_tensors_list = multishell_utils.load_hardi_coupled_sh(opj(args.test_dir, 'data'))
else:
    input_tensors = blind_utils.load_hardi_sh(train_args.input_L, os.path.join(args.test_dir, 'data'), matlabv73=args.v73)
    if after_tensor_coupling:
        input_tensors, n_input_tensors_list = blind_utils.multi_order_mix(input_tensors,
                                                                          max_j_input=train_args.input_L,
                                                                          max_j_output=train_args.output_L)

pred = blind_utils.predict_original_feature(f_output, input_tensors, norm_coef, n_dirs=train_args.n_dirs,
                                            non_negative=args.non_negative,
                                            softmax=args.softmax, minusmin=args.minusmin)

save_dir = os.path.join(args.test_dir, 'rcn_results', os.path.basename(os.path.normpath(args.model_dir)))
if not os.path.exists(save_dir):
    os.makedirs(save_dir)

if not args.v73:
    try:
        sio.savemat(os.path.join(save_dir, 'pred.mat'), {'pred': pred})
    except Exception as e:
        print(e)
        hdf5storage.write({u'pred': pred}, '', os.path.join(save_dir,'pred.mat'), matlab_compatible=True)
else:
    hdf5storage.write({u'pred': pred}, '', os.path.join(save_dir, 'pred.mat'), matlab_compatible=True)

