# -*- coding: utf-8 -*-
"""
gpuが必要 memoryは10Gくらいでいける
b3000_uniformとかをsimulation_dirに与えると, まだpred.matがないangle60ratio1とかで
predをつくって、evaluationもしてくれる

ex)
source shell_utils/run_rcn_kng.sh 20 'python core/rcn_pred_evaluation.py response_b3000_gauss_90HCP_1272_snr50 /home/matsui-k/projects/blind_deconv/simulation/response_b3000_gauss_90HCP_1272_snr50/trained_models/ScalarProducts_8_8_8__bias0.0_wdl0.0_adam_0.001_1.0_/ --reg_lambda=0. --alpha=0.001'
"""
import os
import sys
import argparse
import time
import subprocess
from subprocess import Popen

project = os.path.expanduser('~/projects/blind_deconv/')
head = os.path.join(project, 'simulation')
import blind_utils


def pred_evaluation(simulation_dir_path, model_dir_name, force=False, only_evaluation=False, only_rotated=False,
                    response=False, reg_lambda=0., alpha=0.001):
    """
    simulation_dirとmodel_dirで使うデータと評価するmodelを指定して、
    simulation_dir/test/angleXrarioYIS_ROTATED/trained_model/rcn_resultsに保存する

    Parameters
    ----------
    simulation_dir_path : full path
    model_dir_name : we need only basename
    force : bool
    only_evaluation : bool
    only_rotated : bool

    Returns
    -------

    """
    print 'pred_evaluation called'
    if 'response' in simulation_dir_path:
        response = True

    if response:
        model_description = os.path.basename(os.path.normpath(model_dir_name)) + 'regl{}'.format(reg_lambda) + 'alphax{}'.format(alpha)

    test_dir_path = os.path.join(simulation_dir_path, 'test')
    angle_ratio_dir_path_list = [os.path.join(test_dir_path, d) for d in os.listdir(test_dir_path) if 'DS_Store' not in d]  # angleXratioYとか
    if only_rotated:
        angle_ratio_dir_path_list = [d for d in angle_ratio_dir_path_list if 'rotated' in d]
    for angle_ratio_dir in angle_ratio_dir_path_list:
        print angle_ratio_dir
        if force or (not os.path.exists(os.path.join(angle_ratio_dir, 'rcn_results', os.path.basename(os.path.normpath(model_dir_name)), 'pred.mat'))):
            if not only_evaluation:
                if response:
                    save_dirpath = os.path.join(angle_ratio_dir, 'rcn_results', model_description)
                    cmd = 'python {prediction} {angleXratioY_fp} {model_dir_fp} {save_dirpath} {reg_lambda} {alpha}'.format(
                        prediction=os.path.join(project, 'core/response_prediction.py'),
                        angleXratioY_fp=angle_ratio_dir,
                        model_dir_fp=model_dir_name,
                        save_dirpath=save_dirpath,
                        reg_lambda=reg_lambda,
                        alpha=alpha
                    )
                    print 'prediction in ', save_dirpath
                else:
                    cmd = 'python {prediction} {angleXratioY_fp} {model_dir_fp}'.format(
                        prediction=os.path.join(project, 'core/prediction.py'),
                        angleXratioY_fp=angle_ratio_dir,
                        model_dir_fp=model_dir_name
                        )
                    print 'prediction in '+os.path.join(angle_ratio_dir, 'rcn_results', os.path.basename(os.path.normpath(model_dir_name)))
                sys.stdout.flush()
                p = subprocess.Popen(cmd, shell=True)
                p.wait()
                sys.stdout.flush()

            if response:
                cmd = 'python {script} {ar_dir} {result_dir}'.format(
                    script=os.path.join(project, 'core/evaluation.py'),
                    ar_dir=angle_ratio_dir,
                    result_dir=save_dirpath
                )
                print 'evaluation in', save_dirpath
            else:
                cmd = 'python {script} {ar_dir} {result_dir}'.format(
                    script=os.path.join(project, 'core/evaluation.py'),
                    ar_dir=angle_ratio_dir,
                    result_dir=os.path.join(angle_ratio_dir, 'rcn_results', os.path.basename(os.path.normpath(model_dir_name)))
                )
                print 'evaluation in '+os.path.join(angle_ratio_dir, 'rcn_results',
                                                    os.path.basename(os.path.normpath(model_dir_name)))
            sys.stdout.flush()
            p = subprocess.Popen(cmd, shell=True)
            p.wait()
            sys.stdout.flush()

    # save results over angles for rotated and not_rotated
    if response:
        if not os.path.exists(os.path.join(model_dir_name, 'regl{}'.format(reg_lambda)+ 'alphax{}'.format(alpha))):
            os.makedirs(os.path.join(model_dir_name, 'regl{}'.format(reg_lambda) + 'alphax{}'.format(alpha)))
        blind_utils.summarize_over_angles2df(
            simulation_dir_path, model_description, ratio=1, rotated=True,
            save_filepath=os.path.join(model_dir_name, 'regl{}'.format(reg_lambda) + 'alphax{}'.format(alpha), 'sammay_rotated.png'))
        blind_utils.summarize_over_angles2df(
            simulation_dir_path, model_description,
            ratio=1, rotated=False,
            save_filepath=os.path.join(model_dir_name, 'regl{}'.format(reg_lambda) + 'alphax{}'.format(alpha), 'sammay_not_rotated.png'))


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('simulation_dir_name', type=str)
    parser.add_argument('model_dir_name')
    parser.add_argument('--force', action='store_true', default=True)
    parser.add_argument('--only_evaluation', action='store_true', default=False)
    parser.add_argument('--only_rotated', action='store_true', default=False)
    parser.add_argument('--reg_lambda', default=0.)
    parser.add_argument('--alpha', default=0.001)
    args = parser.parse_args()
    simulation_dir_name = os.path.basename(os.path.normpath(args.simulation_dir_name))
    model_dir_name = os.path.basename(os.path.normpath(args.model_dir_name))
    simulation_dir_path = os.path.join(head, simulation_dir_name)
    pred_evaluation(simulation_dir_path, model_dir_name, args.force, args.only_evaluation, args.only_rotated,
                    reg_lambda=args.reg_lambda, alpha=args.alpha)

