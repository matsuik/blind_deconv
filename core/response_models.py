# -*- coding: utf-8 -*-
from __future__ import division

import numpy as np
import theano
import theano.tensor as T
from theano.tensor.shared_randomstreams import RandomStreams

from blind_utils import floatX
import blocks, blind_utils


def compile(train_feature_vec_list, train_gt_vec, test_feature_vec_list, test_gt_vec, model, opt_w, opt_x):
    """
    データをsharedにして、modelとoptimizerを使ってcomputational graphを作って、
    コンパイルする。

    Parameters
    -----------
    train_feture_vec_list : list of numpy.ndarray
        list of (N, dim, n_tensors)
    train_gt_vec : numpy.ndarray
        (N, n_dir)
    test_feature_vec_list, test_gt_vec
    model : models.Rcn1layerとか
    opt_w : optimizers.SGDとか
    opt_x
    

    """

    def share_data(data):
        return theano.shared(floatX(data), borrow=True)

    s_train_feature_vec_list = [share_data(data) for data in train_feature_vec_list]
    s_train_gt_vec = share_data(train_gt_vec)
    s_test_feature_vec_list = [share_data(data) for data in test_feature_vec_list]
    s_test_gt_vec = share_data(test_gt_vec)

    model.make_graph_train()

    weight_updates = opt_w.make_updates(loss=model.obj_w, param_list=model.param_list)
    weight_updates.update(model.model_updates)

    x_updates = opt_x.make_updates(loss=model.obj_x, param_list=model.fodf_sh_list)

    i_batch = T.iscalar("i_batch")
    index_list = T.ivector("index_list")
    batch_size = T.iscalar("batch_size")

    # for input : 対応するLのテンソルをもってきて、index_listでpermutateして、batchを切り出す
    # and for target : index_listでpermutateして、batchを切り出す
    train_givens = [(tensor,
                     s_train_feature_vec_list[i][index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])
                    for i, tensor in enumerate(model.fodf_sh_list)] + \
                   [(model.t_mat, s_train_gt_vec[index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])]
    test_givens = [(tensor,
                    s_test_feature_vec_list[i][index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])
                   for i, tensor in enumerate(model.fodf_sh_list)] + \
                  [(model.t_mat, s_test_gt_vec[index_list[i_batch * batch_size: i_batch * batch_size + batch_size]])]

    train_givens.append((model.is_train, T.cast(1, 'int32')))   # for dropout : is_trainでdropout内のtrainとtestを切り替えてる

    f_w_train = theano.function(
        inputs=[i_batch, index_list, batch_size],
        updates=weight_updates,
        givens=train_givens,
        on_unused_input='warn'
    )

    f_training_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=train_givens,
        on_unused_input='warn'
    )

    # modelは一緒なのでgivesだけ変えてる
    test_givens.append((model.is_train, T.cast(0, 'int32')))
    f_test_error = theano.function(
        inputs=[i_batch, index_list, batch_size],
        outputs=[model.train_error],
        givens=test_givens,
        on_unused_input='warn'
    )

    f_x_train = theano.function(
        inputs=[],
        outputs=[model.obj_x],
        updates=x_updates,
        givens=[(model.is_train, T.cast(1, 'int32'))]
    )

    result = [f_w_train, f_training_error, f_test_error, model.param_list]
    return result, f_x_train


class Model(object):
    def __init__(self, m_reconstruct, j_input_list=[], j_output_list=[], n_hidden=0, func_key_list=[],
                 error_func_key='l2', reg_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 after_tensor_coupling=True, default_bias=0., dropout_p=1.0, weight_decay_lambda=0., weight_decay_func_key='l2'):
        self.j_input_list = j_input_list
        self.j_output_list = j_output_list

        self.error_func_key = error_func_key
        self.reg_func_key = reg_func_key
        self.pc_func_key = pc_funk_key
        self.reg_lambda = theano.shared(floatX(0.), name='reg_lambda')
        self.n_input_tensors_list = []
        self.n_hidden = n_hidden
        self.func_key_list = func_key_list
        self.default_bias = default_bias

        batch_size = 800
        dim_l_list = blind_utils.make_dim_l_list(blind_utils.make_index(j_input_list[-1]))
        self.fodf_sh_list = [theano.shared(floatX(np.zeros((batch_size, dim_l_list[i_j], 1))),
                                           name='input_tensor'+str(j)) for i_j, j in enumerate(j_input_list)]
        self.t_mat = theano.shared(np.asarray([[]], dtype=theano.config.floatX), name="target")  # (batchsize, n_dir)
        self.param_list = []
        self.M = m_reconstruct
        self.after_tensor_coupling = after_tensor_coupling

        self.out_list = []  # for each L's output : to be concatenated

        self.model_updates = dict()  # for batch normalization
        self.is_train = T.iscalar()

        self.weight_decay_lambda = weight_decay_lambda
        self.weight_decay_func_key = weight_decay_func_key
        self.weight_list = []

    def __str__(self):
        model_description = '_'.join([self.__class__.__name__, str(self.j_input_list[-1]), str(self.j_output_list[-1]),
                                      '_'.join(self.func_key_list), 'bias' + str(self.default_bias),
                                      'wdl'+str(self.weight_decay_lambda)])
        return model_description

    def multi_order_mix_wo_weight(self):
        tens_list = []
        for j_output in self.j_output_list:
            tens_list.append(blocks.simple_multi_order_mix(self.fodf_sh_list, self.j_input_list, j_output, model=self))

        self.tensors_list = tens_list

    def make_graph_head(self):
        pass

    def concatenate(self):
        out_concat = T.concatenate(self.out_list, axis=1)[:, :, 0]  # (N, dim_concat, 1) to (N, dim_concat)
        pred_reconst = T.dot(out_concat, self.M.T)  # (N, n_dirs)
        pred = blocks.get_act_func(self.pc_func_key)(pred_reconst)

        error_func = blocks.get_error_func(self.error_func_key)
        error = error_func(pred, self.t_mat)
        reg_func = blocks.get_reg_func(self.reg_func_key)
        weight_decay_func = blocks.get_reg_func(self.weight_decay_func_key)
        obj_w = error + self.weight_decay_lambda * T.sum([weight_decay_func(w) for w in self.weight_list])
        obj_x = error + self.reg_lambda * reg_func(pred)

        self.obj_w = obj_w
        self.obj_x = obj_x
        self.train_error = error
        self.train_out = pred

    def make_graph_train(self):
        self.multi_order_mix_wo_weight()
        self.make_graph_head()
        self.concatenate()


class ScalarProducts(Model):
    def __init__(self, j_input_list, j_output_list, m_reconstruct, after_tensor_coupling=False,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):
        l = locals()
        del l['self']
        super(ScalarProducts, self).__init__(**l)

    def multi_order_mix_wo_weight(self):
        # mixしない
        self.tensors_list = self.fodf_sh_list

    def make_graph_head(self):
        out_list = []
        for tensor in self.tensors_list:
            w = blind_utils.zero_shared(shape=(1, ))
            self.param_list.append(w)

            h = w * tensor
            out_list.append(h)
        self.out_list = out_list


class LinearCombination(Model):
    def __init__(self, j_input_list, j_output_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):
        l = locals()
        del l['self']
        super(LinearCombination, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            weight = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                        1, self, name='W{}'.format(j_output))

            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], weight)
            out_list.append(h1sv)
        self.out_list = out_list


class NonlinearOneLayer(Model):
    def __init__(self, j_input_list, j_output_list, func_key_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):
        assert len(func_key_list) == 1
        l = locals()
        del l['self']
        super(NonlinearOneLayer, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            weight = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                        1, self, name='W{}'.format(j_output))
            h1sv = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], weight)

            bias = blocks.make_bias(1, self, name='bias{}'.format(j_output))
            h1na = blocks.activate_norm(h1sv, bias, func_key=self.func_key_list[0])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h1na * w_scalar)
        self.out_list = out_list


class TwoLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_hidden, func_key_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):

        assert len(func_key_list) == 2
        l = locals()
        del l['self']
        super(TwoLayer, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output))
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[1])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h * w_scalar)
        self.out_list = out_list


class ThreeLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_hidden, func_key_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0., weight_decay_lambda=0., weight_decay_func_key='l2'):

        assert len(func_key_list) == 3
        l = locals()
        del l['self']
        super(ThreeLayer, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias2{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[1])[0]

            w = blocks.make_weight(self.n_hidden, 1, self, name='W3{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self, name='bias3{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[2])[0]

            w_scalar = blind_utils.zero_shared(1, name='w_scalar{}'.format(j_output))
            self.param_list.append(w_scalar)
            self.weight_list.append(w_scalar)
            out_list.append(h * w_scalar)

        self.out_list = out_list


class DropoutThreeLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_hidden, func_key_list, dropout_p, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0.):

        assert len(func_key_list) == 3
        l = locals()
        del l['self']
        super(DropoutThreeLayer, self).__init__(**l)

        self.dropout_p = dropout_p
        self.srng = RandomStreams(seed=1999)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]
            h = blocks.dropout(h, self.dropout_p, self.is_train, self.srng)

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias2{}'.format(j_output), scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[1])[0]
            h = blocks.dropout(h, self.dropout_p, self.is_train, self.srng)

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self, scale=self.default_bias)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[2])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)
            out_list.append(h * w_scalar)

        self.out_list = out_list


class BnThreeLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_hidden, func_key_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272,
                 default_bias=0.):

        assert len(func_key_list) == 3
        l = locals()
        del l['self']
        super(BnThreeLayer, self).__init__(**l)

        self.t = theano.shared(blind_utils.floatX(1))
        self.model_updates = dict()
        self.model_updates[self.t] = self.t + 1

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, scale=self.default_bias)
            gamma = blocks.make_bias(self.n_hidden, self, scale=1)
            h = blocks.activate_norm_bn(h, self, gamma_vec=gamma, bias_vec=b, beta=0.9,
                                        t=self.t,
                                        n_hidden=self.n_hidden,
                                        func_key=self.func_key_list[0], eps=1e-8)

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias2{}'.format(j_output), scale=self.default_bias)
            gamma = blocks.make_bias(self.n_hidden, self, scale=1)

            h = blocks.activate_norm_bn(h, self, gamma_vec=gamma, bias_vec=b, beta=0.9,
                                        t=self.t,
                                        n_hidden=self.n_hidden,
                                        func_key=self.func_key_list[0], eps=1e-8)

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self, scale=self.default_bias)
            gamma = blocks.make_bias(1, self, scale=1)
            h = blocks.activate_norm_bn(h, self, gamma_vec=gamma, bias_vec=b, beta=0.9,
                                        t=self.t,
                                        n_hidden=1,
                                        func_key=self.func_key_list[0], eps=1e-8)

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)
            out_list.append(h * w_scalar)

        self.out_list = out_list


class FourLayer(Model):
    def __init__(self, j_input_list, j_output_list, n_hidden, func_key_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):

        assert len(func_key_list) == 4
        l = locals()
        del l['self']
        super(FourLayer, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            b = blocks.make_bias(self.n_hidden, self, name='bias1{}'.format(j_output))
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias2{}'.format(j_output))
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[1])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W3{}'.format(j_output))
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(self.n_hidden, self, name='bias3{}'.format(j_output))
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[2])[0]

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[3])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h * w_scalar)
        self.out_list = out_list


class Res(Model):
    def __init__(self, j_input_list, j_output_list, n_hidden, func_key_list, m_reconstruct,
                 reg_func_key='l2', error_func_key='l2', pc_funk_key='l-relu', n_dirs=1272, ):

        assert len(func_key_list) == 4
        l = locals()
        del l['self']
        super(Res, self).__init__(**l)

    def make_graph_head(self):
        out_list = []
        for j_output in self.j_output_list:
            w = blocks.make_weight(self.n_input_tensors_list[self.j_input_list.index(j_output)],
                                   self.n_hidden, self, name='W1{}'.format(j_output))
            h = blocks.superpose_vectors(self.tensors_list[self.j_input_list.index(j_output)], w)

            # res
            b = blocks.make_bias(self.n_hidden, self, name='bias1res{}'.format(j_output))
            hres = blocks.activate_norm(h, b, func_key=self.func_key_list[0])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W2res{}'.format(j_output))
            hres = blocks.superpose_vectors(hres, w)
            b = blocks.make_bias(self.n_hidden, self, name='bias2res{}'.format(j_output))
            hres = blocks.activate_norm(hres, b, func_key=self.func_key_list[1])[0]

            w = blocks.make_weight(self.n_hidden,
                                   self.n_hidden, self, name='W3res{}'.format(j_output))
            hres = blocks.superpose_vectors(hres, w)
            # end res

            h = h + hres

            w = blocks.make_weight(self.n_hidden, 1, self)
            h = blocks.superpose_vectors(h, w)

            b = blocks.make_bias(1, self)
            h = blocks.activate_norm(h, b, func_key=self.func_key_list[3])[0]

            w_scalar = blind_utils.zero_shared(1)
            self.param_list.append(w_scalar)

            out_list.append(h * w_scalar)
        self.out_list = out_list

