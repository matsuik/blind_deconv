# -*- coding: utf-8 -*-
import os
import sys
import time
import argparse
import resource
import gzip, cPickle
import scipy.io as sio
import numpy as np

os.environ['THEANO_FLAGS'] = 'device=gpu'
import blind_utils
floatX = blind_utils.floatX

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt


parser = argparse.ArgumentParser()
parser.add_argument('test_dir', type=str) # fullpath to angleX0ratioY
parser.add_argument('model_dir', type=str) # fullpath
parser.add_argument('save_dir', type=str)
parser.add_argument('reg_lambda', type=float)
parser.add_argument('alpha', type=float)
parser.add_argument('--n_iteration', type=int, default=30000)

args = parser.parse_args()
assert os.path.exists(args.test_dir)
assert os.path.exists(args.model_dir)
print 'args has been set'
sys.stdout.flush()


sys.setrecursionlimit(500000)  # for compile and pickle
soft, hard = resource.getrlimit(resource.RLIMIT_STACK)
resource.setrlimit(resource.RLIMIT_STACK, [hard, hard])
with gzip.open(os.path.join(args.model_dir, 'train_args.pkl.gz'), 'rb') as f:
    train_args = cPickle.load(f)
with gzip.open(os.path.join(args.model_dir, 'f_x_train.pkl.gz'), 'rb') as f:
    f_x_train = cPickle.load(f)
with gzip.open(os.path.join(args.model_dir, 'norm_coef.pkl.gz'), 'rb') as f:
    norm_coef = cPickle.load(f)

print 'f_x_train is loded'
sys.stdout.flush()

hardi_q = blind_utils.load_hardi_qspace(os.path.join(args.test_dir, 'data'), for_response_prediction=True)

print 'data is loaded'
sys.stdout.flush()

named_shared_dict = blind_utils.make_named_shared_dict(f_x_train.get_shared())
if args.reg_lambda is not None:
    named_shared_dict['reg_lambda'].set_value(floatX(args.reg_lambda))
if args.alpha is not None:
    named_shared_dict['alpha_x'].set_value(floatX(args.alpha))
print 'lamda and alpha have been set'

j_input_list = range(0, train_args.input_L+1, 2)
dim_l_list = blind_utils.make_dim_l_list(blind_utils.make_index(train_args.input_L))
n_sh_coef = sum(dim_l_list)
n_input_js = len(j_input_list)

n_samples = hardi_q.shape[0]
batch_size = 800
n_batch = n_samples // batch_size  # あまりを含まないbatchの数

result_fodf_sh_list = [floatX(np.zeros((n_samples, dim_l, 1))) for dim_l in dim_l_list]

plt.figure()
n_iteration = args.n_iteration
train_start = time.time()
for i_batch in range(n_batch):
    train_error_list = []
    index = i_batch * batch_size
    named_shared_dict['target'].set_value(floatX(hardi_q[index: index + batch_size]))
    for i_j in range(n_input_js):
        named_shared_dict['input_tensor{}'.format(j_input_list[i_j])].set_value(
            floatX(np.zeros((batch_size, dim_l_list[i_j], 1))))

    for i_iteration in range(n_iteration):
        train_error = f_x_train()[0]
        train_error_list.append(train_error)
    plt.plot(np.array(train_error_list))

    for i_j in range(n_input_js):
        normed_sh = named_shared_dict['input_tensor{}'.format(j_input_list[i_j])].get_value()
        result_fodf_sh_list[i_j][index: index + batch_size] = normed_sh * norm_coef[i_j]

print 'training_time', time.time() - train_start, '\n'
sys.stdout.flush()

fodf_sh_concat = np.concatenate(result_fodf_sh_list, axis=1)[:, :, 0]
m_reconst = blind_utils.load_Mreconstruct(train_args.input_L, n_dirs=1272)
pred = np.dot(fodf_sh_concat, m_reconst.T)

save_dir = args.save_dir
if not os.path.exists(save_dir):
    os.makedirs(save_dir)
sio.savemat(os.path.join(save_dir, 'pred.mat'), {'pred':pred})
plt.savefig(os.path.join(save_dir, 'x_train_error.png'))