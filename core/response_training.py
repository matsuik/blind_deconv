# -*- coding:utf-8 -*-
"""
ex) source shell_scripts/run_rcn.sh 60 'python model_scripts/one.py b3000_uniform 8 8'
memoryはone.py, linearでもなら32gbくらい,
"""
from __future__ import print_function
import os
import sys
import time
import argparse
import resource

import gzip
import cPickle

import numpy as np

import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from blind_utils import printf
import optimizers, response_models, blind_utils, rcn_pred_evaluation, evaluation


def handle_command():

    parser = argparse.ArgumentParser()
    parser.add_argument('simulation_dir', action='store', type=str)  # b3000_uniformとか
    parser.add_argument('--input_L', action='store', type=int, default=8)
    parser.add_argument('--output_L', action='store', type=int, default=8)
    # model parameters
    parser.add_argument('--n_hidden', action='store', type=int, default=100)
    parser.add_argument('--pc_func_key', action='store', type=str, default='l-relu')
    parser.add_argument('--act_func_key', type=str, default='relu')
    parser.add_argument('--error_func_key', type=str, default='l2')

    parser.add_argument('--reg_func_key', action='store', type=str, default='l2') # for output in q_space

    parser.add_argument('--weight_decay_func_key', type=str, default='l2')
    parser.add_argument('--weight_decay_lambda', type=float, default=0.)

    parser.add_argument('--default_bias', type=float, default=0.)

    # train parameters
    parser.add_argument('--n_epochs', action='store', type=int, default=21)
    parser.add_argument('--interval', action='store', type=int, default=1)
    parser.add_argument('--batch_size', type=int, default=1000)

    parser.add_argument('--opt_algo', type=str, default='adam')
    parser.add_argument('--alpha', action='store', type=float, default=0.001)
    parser.add_argument('--lr_decay', action='store', type=float, default=1.)
    parser.add_argument('--dropout_p', action='store', type=float, default=1.)

    #  data parameters
    parser.add_argument('--train_ratio', type=float, default=0.8)
    parser.add_argument('--n_dirs', type=int, default=1272)  # of discretization of the output q_space : not_arbitrary
    parser.add_argument('--no_normalization', dest='normed', action='store_false')
    parser.add_argument('--not_permutate_train', action='store_false', default=True)

    parser.add_argument('--note', action='store', type=str, default='')

    parser.add_argument('--trained_model_dirpath', type=str, default='') # for curriculum learning

    # for debug
    parser.add_argument('--debug_data', action='store_true', default=False)  # use less data

    args = parser.parse_args()

    assert args.n_dirs in [256, 1272]

    return args


def train(train_input, train_output, test_input, test_output, norm_coef, model, args):

    # trained_modelを保存するdirnameの決定
    model_description = model.__str__()+args.note
    model_description = model_description.split('_')
    model_description.insert(1, str(args.input_L))
    model_description = '_'.join(model_description)
    model_description += '_{}_{}'.format(args.opt_algo, args.alpha)
    if not args.normed:
        model_description += '_notnormed'
    if not args.lr_decay == 0.:
        model_description += '_{}'.format(args.lr_decay)
    model_description += '_' + os.path.basename(args.trained_model_dirpath)
    print(model_description)
    
    # 保存先の設定
    save_dirpath = os.path.join('/home/matsui-k/projects/blind_deconv/simulation',
                            args.simulation_dir, 'trained_models', model_description)

    # compile

    sys.setrecursionlimit(500000)  # for compile and pickle
    soft, hard = resource.getrlimit(resource.RLIMIT_STACK)
    resource.setrlimit(resource.RLIMIT_STACK, [hard, hard])
    if args.opt_algo == 'adam':
        opt_x = optimizers.Adam(alpha=args.alpha, alpha_name='alpha_x')
        opt_w = optimizers.Adam(alpha=args.alpha, alpha_name='alpha_w')
    elif args.opt_algo == 'sgd':
        otp_x = optimizers.Sgd(lr=args.alpha)
        opt_w = optimizers.Sgd(lr=args.alpha)
    else:
        raise Exception('NO_MATCHING_OPTIMIZER')

    t1 = time.clock()
    results, f_x_train = response_models.compile(train_input, train_output, test_input, test_output,
                                            model=model, opt_x=opt_x, opt_w=opt_w)
    f_train, f_training_error, f_test_error, param_list = results
    compile_time = time.clock() - t1
    print('compile_time', compile_time)


# train loop parameter
    n_epochs = args.n_epochs
    batch_size = args.batch_size
    interval = args.interval

    n_train_samples = train_input[0].shape[0]
    n_batch = n_train_samples // batch_size
    index_list = np.asarray(np.random.permutation(range(n_train_samples)), dtype=np.int32)

    n_test_samples = test_input[0].shape[0]
    test_index_list = np.asarray(range(n_test_samples), dtype=np.int32)

    training_error_array = np.zeros((n_epochs // interval + 2,))
    test_error_array = np.zeros((n_epochs // interval + 2,))

# train loop
    # error without training
    i_batch = 0
    training_error = f_training_error(i_batch, index_list, batch_size)[0]
    training_error_array[0] = training_error
    test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
    min_test_error = test_error
    test_error_array[0] = test_error
    printf('initial', training_error, test_error)

    start_time = time.clock()
    for i_epoch in range(n_epochs):
        index_list = np.random.permutation(index_list)
        test_index_list = np.random.permutation(test_index_list)

        for i_batch in range(n_batch):
            f_train(i_batch, index_list, batch_size)

        # for dev_decay
        test_error = f_test_error(0, test_index_list, batch_size)[0]
        if test_error > min_test_error:
            print(i_epoch, 'decay lr to', opt_w.decay_alpha(args.lr_decay))
        else:
            min_test_error = test_error

        # interval数の epochに1回 いろいろ評価する
        if i_epoch % interval == 0:
            training_error = f_training_error(i_batch, index_list, batch_size)[0]
            training_error_array[i_epoch // interval + 1] = training_error

            test_error = f_test_error(0, test_index_list, batch_size)[0]
            test_error_array[i_epoch // interval + 1] = test_error

            if np.isnan(training_error) or np.isnan(test_error):
                printf('nan!')
                break

            printf(i_epoch, training_error, test_error, (time.clock() - start_time) // 60)

    train_time = time.clock() - start_time

# save many things
    if not os.path.exists(save_dirpath):
        os.makedirs(save_dirpath)

    with gzip.open(os.path.join(save_dirpath, 'f_x_train.pkl.gz'), 'wb') as f:
        cPickle.dump(f_x_train, f, protocol=cPickle.HIGHEST_PROTOCOL)

    with gzip.open(os.path.join(save_dirpath, 'norm_coef.pkl.gz'), 'wb') as f:
        cPickle.dump(norm_coef, f, protocol=cPickle.HIGHEST_PROTOCOL)

    with gzip.open(os.path.join(save_dirpath, 'train_args.pkl.gz'), 'wb') as f:
        cPickle.dump(args, f, protocol=cPickle.HIGHEST_PROTOCOL)

    save_list = ['args', 'n_epochs', 'n_train_samples', 'batch_size', 'training_error_array', 'test_error_array',
                 'compile_time', 'train_time']
    str_list = [n + '\t{' + n + '}\n' for n in save_list]
    write_list = [s.format(**locals()) for s in str_list]
    with open(os.path.join(save_dirpath, 'summary.txt'), 'w') as f:
        f.writelines(write_list)

    plt.figure()
    plt.plot(training_error_array, label='training')
    plt.plot(test_error_array, label='test')
    plt.legend()
    plt.savefig(os.path.join(save_dirpath, 'errors.png'))

# prediction and evaluate over angles
    rcn_pred_evaluation.pred_evaluation(
        os.path.join('/home/matsui-k/projects/blind_deconv/simulation', args.simulation_dir),
        save_dirpath, force=True, response=True)

