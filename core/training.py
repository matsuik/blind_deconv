# -*- coding:utf-8 -*-
"""
ex) source shell_scripts/run_rcn.sh 60 'python model_scripts/one.py b3000_uniform 8 8'
memoryはone.py, linearでもなら32gbくらい,
"""
from __future__ import print_function
import os
import sys
import time
import argparse
import argcomplete
import csv


import gzip
import cPickle

import numpy as np

import pandas as pd

import matplotlib
from pandas.core.algorithms import mode

matplotlib.use('Agg')
import matplotlib.pyplot as plt

from blind_utils import printf
import optimizers, models_positive_const, blind_utils, rcn_pred_evaluation, evaluation


def handle_command():
    parser = argparse.ArgumentParser()
    parser.add_argument('simulation_dir', action='store', type=str)  # b3000_uniformとか
    parser.add_argument('n_layer', action='store', type=int)
    parser.add_argument('input_L', action='store', type=int)
    parser.add_argument('output_L', action='store', type=int)

    # model parameters
    parser.add_argument('--n_hidden', action='store', type=int, default=100)
    parser.add_argument('--pc_func_key', action='store', type=str, default='l-relu')
    parser.add_argument('--act_func_key', type=str, default='relu')
    parser.add_argument('--error_func_key', type=str, default='l2')

    parser.add_argument('--reg_func_key', action='store', type=str, default='nnl2')  # for output in q_space
    parser.add_argument('--reg_lambda', action='store', type=float, default=0.)

    parser.add_argument('--weight_decay_func_key', type=str, default='l2')
    parser.add_argument('--weight_decay_lambda', type=float, default=0.)

    parser.add_argument('--default_bias', type=float, default=0.)

    # train parameters
    parser.add_argument('--n_epochs', action='store', type=int, default=101)
    parser.add_argument('--interval', action='store', type=int, default=5)
    parser.add_argument('--n_all', action='store', type=int, default=None)
    parser.add_argument('--batch_size', type=int, default=1000)

    parser.add_argument('--opt_algo', type=str, default='adam')
    parser.add_argument('--alpha', action='store', type=float, default=0.001)
    parser.add_argument('--lr_decay', action='store', type=float, default=1.)
    parser.add_argument('--decay_epoch_list', nargs='+', type=int, default=[])
    parser.add_argument('--dropout_p', action='store', type=float, default=1.)

    #  data parameters
    parser.add_argument('--train_ratio', type=float, default=0.8)
    parser.add_argument('--n_dirs', type=int, default=1272)  # of discretization of the output q_space : not_arbitrary
    parser.add_argument('--no_normalization', dest='normed', action='store_false')
    parser.add_argument('--not_permutate_train', action='store_false', default=True)

    parser.add_argument('--note', action='store', type=str, default='')

    parser.add_argument('--trained_model_dirpath', type=str, default='') # for curriculum learning

    # for debug
    parser.add_argument('--debug_data', action='store_true', default=False)  # use less data
    parser.add_argument('--debug_train', action='store_true', default=False)
    parser.add_argument('--only_train', action='store_true', default=False)

    argcomplete.autocomplete(parser)
    args = parser.parse_args()

    print(args.decay_epoch_list)

    assert args.n_dirs in [256, 1272]

    return args


def train(train_input, train_output, test_input, test_output, norm_coef, model, args):
    if args.only_train:
        print("only_train")

    # trained_modelを保存するdirnameの決定
    model_description = model.__str__()+args.note
    model_description = model_description.split('_')
    model_description.insert(1, str(args.input_L))
    model_description = '_'.join(model_description)
    model_description += '_{}_{}'.format(args.opt_algo, args.alpha)
    if not args.normed:
        model_description += '_notnormed'
    if not args.lr_decay == 0.:
        model_description += '_lrdecay{}'.format(args.lr_decay)
    if not args.decay_epoch_list == []:
        model_description += '_decay_epochs{}'.format('-'.join([str(i) for i in args.decay_epoch_list]))
    model_description += '_' + os.path.basename(args.trained_model_dirpath)
    print(model_description)
    
    # 保存先の設定
    save_dirpath = os.path.join('/home/matsui-k/projects/blind_deconv/simulation',
                            args.simulation_dir, 'trained_models', model_description)
    if not os.path.exists(save_dirpath):
        os.makedirs(save_dirpath)

    # compile
    if args.opt_algo == 'adam':
        opt = optimizers.Adam(alpha=args.alpha)
    elif args.opt_algo == 'sgd':
        opt = optimizers.MomentumSgd(alpha=args.alpha)
    else:
        raise Exception('NO_MATCHING_OPTIMIZER')

    t1 = time.clock()
    results = models_positive_const.compile(train_input, train_output, test_input, test_output,
                                            model=model, optimizer=opt)
    f_train, f_training_error, f_test_error, f_output, param_list = results
    compile_time = time.clock() - t1
    print('compile_time', compile_time)

    # trained_modelをロードして、そのweightを今から学習するモデルに代入する curriculum learningとかに使う
    if args.trained_model_dirpath:
        with gzip.open(os.path.join(args.trained_model_dirpath, 'f_output.pkl.gz'), 'rb') as f:
            f_trained_output = cPickle.load(f)
        for param in f_train.get_shared():
            if param.name is not None:
                for trained_param in f_trained_output.get_shared():
                    if param.name == trained_param.name:
                        param.set_value(trained_param.get_value())
                        print('reload '+param.name)

    if not args.only_train:
        # test_evaluationを保存するcsvの設定
        test_eval_filepath = os.path.join(save_dirpath, 'test_evaluation.txt')
        f_summary_csv = file(test_eval_filepath, 'w')
        csv_writer = csv.writer(f_summary_csv, lineterminator='\n')  # 書き込みファイルの設定
        key_list = ['success_rate', 'n_spurious_peaks', 'bias1', 'bias2', 'std1', 'std2', 'bias1_all',
                    'bias2_all',
                    'std1_all', 'std2_all',
                    'success_rate_tol', 'biases_tol', 'biases_tol', 'stds_tol', 'stds_tol',
                    'angle_tolerance']
        csv_writer.writerow(key_list)  # 1行(リスト)の書き込み

        # lode test tensors
        if 'multishell' in args.simulation_dir:
            import multishell_utils
            angleX_dirpath = os.path.join('/home/matsui-k/projects/blind_deconv/simulation',
                                          args.simulation_dir, 'test', 'angle60ratio1')
            test_input_tensors, n_input_tensors_list = multishell_utils.load_hardi_coupled_sh(os.path.join(angleX_dirpath, 'data'))

        else:  # single_shellとかで
            angleX_dirpath = os.path.join('/home/matsui-k/projects/blind_deconv/simulation',
                                    args.simulation_dir, 'test', 'angle60ratio1rotated')
            test_input_tensors = blind_utils.load_hardi_sh(args.input_L,
                                                           os.path.join(angleX_dirpath, 'data'), matlabv73=False)
            if model.after_tensor_coupling:
                test_input_tensors, n_input_tensors_list = blind_utils.multi_order_mix(test_input_tensors,
                                                                                       max_j_input=args.input_L,
                                                                                       max_j_output=args.output_L)

# train loop parameter
    n_epochs = args.n_epochs
    batch_size = args.batch_size
    interval = args.interval
    if args.debug_train:
        n_epochs = 2
        interval = 1
        batch_size = 10
    assert n_epochs >= interval, "interval is too long"

    n_train_samples = train_input[0].shape[0]
    assert n_train_samples >= batch_size, "batch_size is too big"
    n_batch = n_train_samples // batch_size
    print('n_bathc', n_batch)
    index_list = np.asarray(np.random.permutation(range(n_train_samples)), dtype=np.int32)

    n_test_samples = test_input[0].shape[0]
    test_index_list = np.asarray(range(n_test_samples), dtype=np.int32)

    training_error_array = np.zeros(((n_epochs-1) // interval + 2,))
    test_error_array = np.zeros(((n_epochs-1) // interval + 2,))

# train loop
    # error without training
    i_batch = 0
    training_error = f_training_error(i_batch, index_list, batch_size)[0]
    training_error_array[0] = training_error
    test_error = f_test_error(i_batch, test_index_list, batch_size)[0]
    min_test_error = test_error
    test_error_array[0] = test_error
    printf('initial', training_error, test_error)

    if args.decay_epoch_list != []:
        i_decay_epoch = 0

    start_time = time.clock()
    for i_epoch in range(n_epochs):
        index_list = np.random.permutation(index_list)
        test_index_list = np.random.permutation(test_index_list)

        for i_batch in range(n_batch):
            f_train(i_batch, index_list, batch_size)

        if args.decay_epoch_list != []:
            if i_epoch == args.decay_epoch_list[i_decay_epoch]:
                print(i_epoch, 'decay lr to', opt.decay_alpha(args.lr_decay))
                if i_decay_epoch < len(args.decay_epoch_list) -1:
                    i_decay_epoch += 1

        # for dev_decay
        # test_error = f_test_error(0, test_index_list, batch_size)[0]
        # if test_error > min_test_error and args.lr_decay != 1.:
        #     print(i_epoch, 'decay lr to', opt.decay_alpha(args.lr_decay))
        # else:
        #     min_test_error = test_error

        # interval数の epochに1回 いろいろ評価する
        if i_epoch % interval == 0:
            training_error = f_training_error(i_batch, index_list, batch_size)[0]
            training_error_array[i_epoch // interval + 1] = training_error

            test_error = f_test_error(0, test_index_list, batch_size)[0]
            test_error_array[i_epoch // interval + 1] = test_error

            if not args.only_train:
                pred = blind_utils.predict_original_feature(f_output, test_input_tensors,
                                                            norm_coef,
                                                            n_dirs=args.n_dirs, non_negative=True)
                value_list = evaluation.evaluate_pred(pred, angleX_dirpath)
                csv_writer.writerow(value_list)

            if np.isnan(training_error) or np.isnan(test_error):
                printf('nan!')
                break

            printf(i_epoch, training_error, test_error, (time.clock() - start_time) // 60)

    train_time = time.clock() - start_time

    if not args.only_train:
        f_summary_csv.close()

# save many things
    sys.setrecursionlimit(500000)  # for pickle
    with gzip.open(os.path.join(save_dirpath, 'f_output.pkl.gz'), 'wb') as f:
        cPickle.dump(f_output, f, protocol=cPickle.HIGHEST_PROTOCOL)

    with gzip.open(os.path.join(save_dirpath, 'norm_coef.pkl.gz'), 'wb') as f:
        cPickle.dump(norm_coef, f, protocol=cPickle.HIGHEST_PROTOCOL)

    with gzip.open(os.path.join(save_dirpath, 'train_args.pkl.gz'), 'wb') as f:
        cPickle.dump(args, f, protocol=cPickle.HIGHEST_PROTOCOL)

    with gzip.open(os.path.join(save_dirpath, 'after_tensor_coupling.pkl.gz'), 'wb') as f:
        cPickle.dump(model.after_tensor_coupling, f, protocol=cPickle.HIGHEST_PROTOCOL)

    save_list = ['args', 'n_epochs', 'n_train_samples', 'batch_size', 'training_error_array', 'test_error_array',
                 'compile_time', 'train_time']
    str_list = [n + '\t{' + n + '}\n' for n in save_list]
    write_list = [s.format(**locals()) for s in str_list]
    with open(os.path.join(save_dirpath, 'summary.txt'), 'w') as f:
        f.writelines(write_list)

    plt.figure()
    plt.plot(training_error_array, label='training')
    plt.plot(test_error_array, label='test')
    plt.legend()
    plt.savefig(os.path.join(save_dirpath, 'errors.png'))

    if not args.only_train:
        df = pd.read_csv(test_eval_filepath)
        fig = plt.figure(figsize=(4, 12))
        ax = fig.add_subplot(411)
        df.loc[:, ['success_rate_tol']].plot(ax=ax)
        ax = fig.add_subplot(412)
        df.loc[:, ['n_spurious_peaks']].plot(ax=ax)
        ax = fig.add_subplot(413)
        ax.set_ylim(0, df.loc[:, ['bias1', 'bias2']].max().max() + 0.5)
        df.loc[:, ['bias1', 'bias2']].plot(ax=ax)
        ax = fig.add_subplot(414)
        ax.set_ylim(0, df.loc[:, ['std1', 'std2']].max().max())
        df.loc[:, ['std1', 'std2']].plot(ax=ax)
        plt.savefig(os.path.join(save_dirpath, 'test_evaluation.png'))

    # prediction and evaluate over angles
        rcn_pred_evaluation.pred_evaluation(
            os.path.join('/home/matsui-k/projects/blind_deconv/simulation', args.simulation_dir),
            save_dirpath, force=True, only_rotated=True)

        #
        # blind_utils.summarize_over_angles2df(
        #     args.simulation_dir, model_description,
        #     ratio=1, rotated=False,
        #     save_filepath=os.path.join(save_dirpath, 'summary_not_rotated.png'))

        if 'multishell' not in args.simulation_dir:
            blind_utils.summarize_over_angles2df(
                args.simulation_dir, model_description, ratio=1, rotated=True,
                save_filepath=os.path.join(save_dirpath, 'summary_rotated.png'))
