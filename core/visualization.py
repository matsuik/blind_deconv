# -*- coding: utf-8 -*-

import numpy as np
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.io as sio
from mpl_toolkits.mplot3d import Axes3D
from scipy.spatial import ConvexHull


def load_bdirs_ch(n_dirs):
    """

    Parameters
    ----------
    n_dirs : int
        90, 256, 1272

    Returns
    -------

    """
    ms = sio.loadmat('/home/matsui-k/projects/blind_deconv/dwsignsim/bdirs_whole_'+str(n_dirs)+'.mat')
    bdirs = ms['bdirs']
    ch = ConvexHull(bdirs.T)
    return bdirs, ch


def equal_aspect(ax, bw):
    X, Y, Z = bw
    ax.set_aspect('equal')
    max_range = np.array([X.max()-X.min(), Y.max()-Y.min(), Z.max()-Z.min()]).max() / 2.0
    mid_x = (X.max()+X.min()) * 0.5
    mid_y = (Y.max()+Y.min()) * 0.5
    mid_z = (Z.max()+Z.min()) * 0.5
    ax.set_xlim(mid_x - max_range, mid_x + max_range)
    ax.set_ylim(mid_y - max_range, mid_y + max_range)
    ax.set_zlim(mid_z - max_range, mid_z + max_range)


def matlab_view(ax):
    ax.view_init(elev=30, azim=-127.5)


def visualize_pred(pred, bdirs, ch):
    """

    Parameters
    ----------
    pred : numpy.ndarray
        (n_dirs, )
    bdirs : numpy.ndarray
        (3, n_dirs)
    ch : scipy.spatial.ConvexHull
    Returns
    -------

    """
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    bw = pred * bdirs
    equal_aspect(ax, bw)
    matlab_view(ax)
    ax.plot_trisurf(bw[0], bw[1], bw[2], triangles=ch.simplices, cmap='viridis', linewidth=0, alpha=1)
    ax.set_xlabel("x",fontsize=18)
    ax.set_ylabel("y",fontsize=18)
    return ax


def visualize_localmax(pred, peak_value_list, peak_dir_list, bdirs, ch):
    fig = plt.figure(figsize=(10, 10))
    ax = fig.add_subplot(1, 1, 1, projection='3d')
    bw = pred * bdirs
    equal_aspect(ax, bw)
    matlab_view(ax)
    ax.plot_trisurf(bw[0], bw[1], bw[2], triangles=ch.simplices, cmap='viridis', linewidth=0, alpha=1)

    for peak_value, peak_dir in zip(peak_value_list, peak_dir_list):
        b = peak_value * peak_dir
        ax.scatter3D(b[0], b[1], b[2], c='r')
        ax.scatter3D(-b[0], -b[1], -b[2], c='r')


def visualize_gt(pred, chosen_peak_list, peak_value_list, peak_dir_list, n1, n2, bdirs, ch, ax=None, view='matlab'):
    if ax is None:
        fig = plt.figure(figsize=(10, 10))
        ax = fig.add_subplot(1, 1, 1, projection='3d')
    bw = pred * bdirs
    equal_aspect(ax, bw)
    if view == 'matlab':
        matlab_view(ax)
    else:
        ax.view_init(elev=90, azim=0)
    ax.plot_trisurf(bw[0], bw[1], bw[2], triangles=ch.simplices, cmap='viridis', linewidth=0, alpha=1)

    for peak_value, peak_dir in zip(peak_value_list, peak_dir_list):
        b = peak_value * peak_dir
        ax.scatter3D(b[0], b[1], b[2], c='y')
        ax.scatter3D(-b[0], -b[1], -b[2], c='y')

    b = peak_value_list[0] * n1
    ax.scatter3D(b[0], b[1], b[2], c='b')
    ax.scatter3D(-b[0], -b[1], -b[2], c='b')
    b = peak_value_list[0] * n2  # 大きい方に合わせてる
    ax.scatter3D(b[0], b[1], b[2], c='b')
    ax.scatter3D(-b[0], -b[1], -b[2], c='b')

    if chosen_peak_list is not None:
        b = chosen_peak_list[0]
        ax.scatter3D(b[0], b[1], b[2], c='r')
        ax.scatter3D(-b[0], -b[1], -b[2], c='r')

        b = chosen_peak_list[1]
        ax.scatter3D(b[0], b[1], b[2], c='c')
        ax.scatter3D(-b[0], -b[1], -b[2], c='c')


def tile_visualization(pred, success_mask, chosen_peak_list_list, peak_value_list_list, peak_dir_list_list,
                       n1, n2, bdirs, ch):
    """


    Parameters
    ----------
    pred : 全部
    success_mask : 全部 boolian 成功してるとこだけTrue
    chosen_peak_list_list : 成功のみ
    peak_value_list_list : 全部
    peak_dir_list_list : 全部
    n1
    n2
    bdirs : 全部
    ch

    Returns
    -------

    """
    fig = plt.figure(figsize=(15, 15))
    np.random.seed(1999)
    if bdirs.ndim == 2:  # not rotated
        for i_plot, i in enumerate(np.random.randint(pred.shape[0], size=(9,))):
            ax = fig.add_subplot(3, 3, i_plot+1, projection='3d')
            if success_mask[i]:
                visualize_gt(pred[i], chosen_peak_list_list[np.sum(success_mask[:i])],
                             peak_value_list_list[i], peak_dir_list_list[i],
                             n1, n2, bdirs, ch, ax)
            else:
                visualize_gt(pred[i], None, peak_value_list_list[i], peak_dir_list_list[i],
                             n1, n2, bdirs, ch, ax)
    if bdirs.ndim == 3:  # rotated
        for i_plot, i in enumerate(np.random.randint(pred.shape[0], size=(9,))):
            ax = fig.add_subplot(3, 3, i_plot+1, projection='3d')
            if success_mask[i]:
                visualize_gt(pred[i], chosen_peak_list_list[np.sum(success_mask[:i])],
                             peak_value_list_list[i], peak_dir_list_list[i],
                             n1, n2, bdirs[i], ch, ax)
            else:
                visualize_gt(pred[i], None, peak_value_list_list[i], peak_dir_list_list[i],
                             n1, n2, bdirs[i], ch, ax)


