load ../data/test/angle90ratio1/data_base.mat
load bdirs_whole_256.mat
signal = signal(:, 1, 1:800);
signal = reshape(signal, [size(signal, 1), 10, 10, 8]);
signal = permute(signal, [2, 3, 4, 1]);
bdirs = bdirs';
bdirs = [bdirs, 1000.*ones(size(bdirs, 1), 1)];
bdirs = [[0, 0, 0, 0]; bdirs];
image.data = signal;
image.vox = [2, 2, 2];
image.DW_scheme = bdirs;
write_mrtrix(image, '../data/test/angle90ratio1/test_dwi.mif')

load ../data/train/b1000/data_base.mat
load bdirs_whole_256.mat
signal = reshape(signal, [size(signal, 1), 100, 100, 85]);
signal = permute(signal, [2, 3, 4, 1]);
bdirs = bdirs';
bdirs = [bdirs, 1000.*ones(size(bdirs, 1), 1)];
bdirs = [[0, 0, 0, 0]; bdirs];
image.data = signal;
image.vox = [2, 2, 2];
image.DW_scheme = bdirs;
write_mrtrix(image, '../data/train/b1000/train_dwi.mif')



