angle_list = 20:10:90;
for angle=angle_list,
    create_test_data(angle, 1, 'b3000_gaussian_snr50_noweighting', 'gauss')
end

for angle=angle_list,
    rotated_create_test_data(angle, 1, 'b3000_gaussian_snr50_noweighting', 'gauss')
end
