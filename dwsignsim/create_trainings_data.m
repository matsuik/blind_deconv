%%

%% you have to match the ndir, nt and the reshaping for mrtrix

to_save = false;
simulation_dir = 'dw90HCP_ndirs10000_nt100_out256_snr20_b3000_gauss'
multiply_vi = false;
do_weighting = true;
is_single = false;
grad_scheme_name = 'bdirs_whole_90HCP.mat';
discretize_scheme_name = 'bdirs_whole_256.mat';
gen_param_dist_key = 'gauss';
snr = 20;
inv_snr = 1/20;
to_visualize = true;

save_dir = fullfile('~/projects/blind_deconv/simulation', ...
    simulation_dir, 'train');

grad_scheme_ms = load(grad_scheme_name);
bdirs = grad_scheme_ms.bdirs;
nrealdirs=size(bdirs,2)/2;
% we set the bvalue to 1000 and create the b-tensors
bvalue=3000;
bten=zeros([3,3,2*nrealdirs+1]);
for a=1:2*nrealdirs,
    bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';
end;

bten=bvalue*bten;

% plot the sphere discretization
CH=convhull(bdirs');
trisurf(CH,bdirs(1,:),bdirs(2,:),bdirs(3,:));
axis equal


%%
% generating the signal
%data = genDWIsignalCrossings(bten,'ndir',1000,'nt',1000);
data = genDWIsignalCrossings(bten, gen_param_dist_key, do_weighting, ...
    is_single, 'ndir',10000,'nt',100, 'nz', inv_snr);

% scale invariance: deviding the signal by the b0 image
signal=data.signal(2:end,:)./((repmat(data.signal(1,:),size(data.signal,1)-1,1)));

alldirs=bdirs;

discretize_scheme_ms = load(discretize_scheme_name);
output_dirs = discretize_scheme_ms.bdirs;


%% fodf directions

input_L = 8;
output_L = 8;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
w1=repmat(data.w(1, :, :), [size(fodf_n1, 1), 1, 1]);

if do_weighting,
    fodf_sh=fodf_n1.*w1+fodf_n2.*(1-w1);
elseif is_single,
    fodf_sh=fodf_n1;
else
    fodf_sh=fodf_n1.*0.5 + fodf_n2.*0.5;
end;

data_base.input=signal_sh;
data_base.output=fodf_sh(:,:);


Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
% reconstruction of signal (harmonic domain -> spatial domain)
signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);

Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
% reconstruction of fodf (harmonic domain -> spatial domain)

fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
fodf = fodf ./ repmat(sum(fodf, 1), size(fodf, 1), 1, 1);
iso = ones(size(fodf)) / size(fodf, 1);
if multiply_vi,
    for i_config=1:size(fodf, 3),
        fodf(:, :, i_config) = data.Vi(i_config) * fodf(:, :, i_config) + (1 - data.Vi(i_config)) * iso(:, :, i_config);    
    end
end
    %% 

fodf_3 = fodf;
fodf = fodf(:, :);

%% 

if to_save
    if not(exist(save_dir))
        mkdir(save_dir)
    end;
    save(fullfile(save_dir, 'data_base.mat'), '-struct', 'data', '-v7.3')
    save(fullfile(save_dir, 'hardi_sh8.mat'), 'signal_sh', '-v7.3')
    save(fullfile(save_dir, 'fodf_reconst8.mat'), 'fodf', '-v7.3')
    save(fullfile(save_dir, 'Mcontra_signal8.mat'), 'Mcontra_signal')
end;

%% 
% signal_r = reshape(signal, [size(signal, 1), 50, 50, 80]); % (num_dw, 50, 50, 80) <- (n_dw, n_samples)
% signal_r = permute(signal_r, [2, 3, 4, 1]); % (50, 50, 80, num_dw)
% 
% 
% bdirs = grad_scheme_ms.bdirs; % (3, n_dw)
% bdirs = bdirs';
% bdirs = [bdirs, bvalue.*ones(size(bdirs, 1), 1)];
% image.data = signal_r;
% image.vox = [2, 2, 2];
% image.DW_scheme = bdirs;
% 
% if to_save
%     write_mrtrix(image, fullfile(save_dir, 'dwi.mif'))
% end

%%
if to_visualize,
bdirs = grad_scheme_ms.bdirs;

figure(3);
subplot(1,2,1);
hist(rad2deg(acos(abs(sum(data.n1.*data.n2,1)))))
title('crossing degree');

subplot(1,2,2);
hist(data.w(1,:,1))
title('crossing ratio');

figure(2);
colormap default
clf
hold on
for indx=1:5

    ind=randi(size(data.signal,2));
    ind2=randi(size(data.signal,3));

    %% signal + ground truth
    subplot(3,5,indx);
    hold on
    title([num2str(ind),'  ',num2str(ind2)])
    s=data.signal(2:end,ind,ind2);
    snorm=data.signal(1,ind,ind2);
    s=s./repmat(snorm,size(s,1),1);

    bdirs_w=(bdirs.*repmat(s',3,1));
    CH=convhull(bdirs');

    trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none','FaceVertexCData',s(:));

    w1=data.W(ind);
    w2=1-w1;
    plot3(w1*[data.n1(1,ind),-data.n1(1,ind)],w1*[data.n1(2,ind),-data.n1(2,ind)],w1*[data.n1(3,ind),-data.n1(3,ind)],'linewidth',5)
    plot3(w2*[data.n2(1,ind),-data.n2(1,ind)],w2*[data.n2(2,ind),-data.n2(2,ind)],w2*[data.n2(3,ind),-data.n2(3,ind)],'linewidth',5)
    axis equal
    axis(1.5*[-1 1 -1 1 -1 1])
    view(2)


    %% reconstructed signal (raw -> sh -> raw')
    subplot(3,5,indx+5);
    hold on
    title('reconstruction (signal)')
    s=signal_rec(:,ind,ind2);
    bdirs_w=(bdirs.*repmat(s',3,1));
    CH=convhull(bdirs');

    trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
    axis equal
    axis(1.5*[-1 1 -1 1 -1 1])
    view(2)


    %% reconstructed gt (raw -> sh -> raw')
    subplot(3,5,indx+10);
    hold on
    title(['reconstruction (directions)  ',num2str(dot(data.n1(:,ind),data.n2(:,ind)))])
    s=50*fodf_3(:,ind,ind2);

    bdirs_w=(output_dirs.*repmat(s',3,1));
    CH=convhull(output_dirs');

    trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
    axis equal
    axis(1.5*[-1 1 -1 1 -1 1])     
    view(2)
end;
end;

if to_save
    angle_list = 20:10:90;
    for angle=angle_list,
        rotated_create_test_data(angle, 1, simulation_dir, gen_param_dist_key, ...
            grad_scheme_ms, discretize_scheme_ms, inv_snr, to_visualize);
    end

    for angle=angle_list,
        create_test_data(angle, 1, simulation_dir, gen_param_dist_key, ...
            grad_scheme_ms, discretize_scheme_ms, inv_snr, to_visualize);
    end
end

