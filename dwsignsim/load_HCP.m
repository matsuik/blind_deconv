to_save = true;
save_dir = '../HCP/b3000';

image_dwi3000 = read_mrtrix('../HCP/dwi3000.mif')
image_meanb0 = read_mrtrix('../HCP/meanb0.mif')

hcp_size = size(image_dwi3000.data); % (x, y, z, n_dw)
dw3000_flat = reshape(image_dwi3000.data, hcp_size(1)*hcp_size(2)*hcp_size(3), hcp_size(4));
% dw3000_normed = dw3000_flat ./ repmat(image_meanb0.data(:), 1, hcp_size(4));

% dw3000_normed(dw3000_normed > 2) = 2;

% dw3000_normed = dw3000_flat / 1000;

b3000_b0normed = zeros(size(dw3000_flat));
meanb0_st = image_meanb0.data / max(image_meanb0.data(:));
b3000_b0normed(meanb0_st(:) > 0.08, :) = dw3000_flat(meanb0_st(:) > 0.08, :) ./ repmat(image_meanb0.data(meanb0_st(:) > 0.08), 1, size(b3000_b0normed, 2));
h = histogram(b3000_b0normed(b3000_b0normed > 0))

signal = b3000_b0normed'; % (num_dw, num_samples)
alldirs = image_dwi3000.DW_scheme(:, 1:3)'; % (3, num_dw)
input_L = 8;
signal_sh = discrete2sh(signal,alldirs, input_L);

save_dir = fullfile(save_dir, 'thresholded_b0_normed_dw_scheme3', 'data');
if to_save
if not(exist(save_dir))
    mkdir(save_dir)
end;
save(fullfile(save_dir, 'hardi_sh8.mat'), 'signal_sh', '-v7.3')
save(fullfile(save_dir, 'hardi_q.mat'), 'signal', '-v7.3')
end