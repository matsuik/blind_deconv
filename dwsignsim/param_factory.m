function params = param_factory(nt, key), 
    N = nt*0.8;
    key
    if strcmp(key, 'uniform'),
        D1 = 0.2+rand(nt,1)*2.2;
        D2 = 0.2+rand(nt,1)*1.5;
        D3 = 0.2+rand(nt,1)*0.5;    
 
        V = rand(nt,1)*1; % water in axon
        Vw = (1-V).*rand(nt,1)*csf; % water out of axon
    end
    if strcmp(key, 'gauss'),
        D1 = 2.2 + randn(nt, 1)*0.5;
        D2 = 1.2 + randn(nt, 1)*0.3;
        D3 = 0.7 + randn(nt, 1)*0.1;
        V = 0.6 + randn(nt, 1)*0.2;
        Vw = 0.05 + randn(nt, 1)*0.05;
        
        D1 = zero_clip(D1);
        D2 = zero_clip(D2);
        D3 = zero_clip(D3);
        V = zero_clip(V);
        Vw = zero_clip(Vw);
        
        V = one_clip(V);
        Vw = one_clip(Vw);
        
        
    end
    idx = find(abs(D1-(D2+2*D3))<1.5);
    idx = idx(1:N, :);
    size(idx) 

    params.D1 = D1(idx)';
    params.D2 = D2(idx)';
    params.D3 = D3(idx)';
    params.Vi = V(idx)';
    params.Vf = Vw(idx)'; % free difusion
    params.Ve = 1-V(idx)'-Vw(idx)'; % extra fiber
end

function param = zero_clip(param)
    param(param < 0) = 0;
end

function param = one_clip(param)
    param(param > 1) = 1;
end
        