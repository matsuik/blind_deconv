function rotated_create_test_data(angle, ratio, simulation_dir, key, ...
    grad_scheme_ms, discretize_scheme_ms, inv_snr, to_visualize)
% ratio : how larger the second peak than the first
save_dir = fullfile('~/projects/blind_deconv/simulation', ...
    simulation_dir, 'test');

% loading gradient directions
bdirs = grad_scheme_ms.bdirs;
nrealdirs=size(bdirs, 2)/2;

% we set the bvalue to 1000 and create the b-tensors
bvalue=3000;
bten=zeros([3,3,2*nrealdirs+1]);
for a=1:2*nrealdirs,
    bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';
end;

bten=bvalue*bten;
setting_dir = ['angle', num2str(angle), 'ratio', num2str(ratio), 'rotated'];
save_dir = fullfile(save_dir, setting_dir, 'data');
%%
% generating the signal
data = rotated_genDWIsignalCrossings(bten, angle, ratio, key, inv_snr);

%% 
% scale invariance: deviding the signal by the b0 image
signal=data.signal(2:end,:)./((repmat(data.signal(1,:),size(data.signal,1)-1,1)));

alldirs=bdirs;

output_dirs = discretize_scheme_ms.bdirs;
%% fodf directions

input_L = 8;
output_L = 8;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1_rotated,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2_rotated,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
size(data.w)
size(fodf_n1)
% w1=(data.w(1:size(fodf_n1,1),:,:));
w1 = repmat(data.w(1, :, :), size(fodf_n1, 1), 1, 1);
size(w1)
fodf_sh=fodf_n1.*w1+fodf_n2.*(1-w1);

data_base.input=signal_sh;
data_base.output=fodf_sh(:,:);


Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
% reconstruction of signal (harmonic domain -> spatial domain)
signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);

Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
% reconstruction of fodf (harmonic domain -> spatial domain)
fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
multiply_vi = false;
if multiply_vi,
    for i_config=1:size(fodf, 3),
            fodf(:, :, i_config) = data.Vi(i_config) * fodf(:, :, i_config);    
    end
end
fodf = fodf(:, :);
%% 

if not(exist(save_dir))
    mkdir(save_dir)
end;


save(fullfile(save_dir, 'data_base.mat'), '-struct', 'data')
save(fullfile(save_dir, 'hardi_sh8.mat'), 'signal_sh')
save(fullfile(save_dir, 'fodf_reconst8.mat'), 'fodf')
save(fullfile(save_dir, 'fodf_sh8.mat'), 'fodf_sh', '-v7.3')


%% fodf directions

input_L = 4;
output_L = 4;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1_rotated,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2_rotated,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
w1=(data.w(1:size(fodf_n1,1),:,:));
fodf_sh=fodf_n1.*w1+fodf_n2.*(1-w1);

data_base.input=signal_sh;
data_base.output=fodf_sh(:,:);


Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
% reconstruction of signal (harmonic domain -> spatial domain)
signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);

Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
% reconstruction of fodf (harmonic domain -> spatial domain)
fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
multiply_vi = false;
if multiply_vi,
    for i_config=1:size(fodf, 3),
            fodf(:, :, i_config) = data.Vi(i_config) * fodf(:, :, i_config);    
    end
end
fodf = fodf(:, :);

save(fullfile(save_dir, 'hardi_sh4.mat'), 'signal_sh')
save(fullfile(save_dir, 'fodf_reconst4.mat'), 'fodf')
save(fullfile(save_dir, 'fodf_sh4.mat'), 'fodf_sh', '-v7.3')
%% 

signal_r = reshape(signal, [size(signal, 1), 10, 10, 80]);
signal_r = permute(signal_r, [2, 3, 4, 1]);

bdirs = grad_scheme_ms.bdirs;
bdirs = bdirs';
bdirs = [bdirs, bvalue.*ones(size(bdirs, 1), 1)];
image.data = signal_r;
image.vox = [2, 2, 2];
image.DW_scheme = bdirs;

write_mrtrix(image, fullfile(save_dir, 'dwi.mif'))
%%
if to_visualize,
bdirs = grad_scheme_ms.bdirs;
figure(100+angle+3);
subplot(1,2,1);
hist(rad2deg(acos(abs(sum(data.n1_rotated.*data.n2_rotated,1)))))
title('crossing degree');

subplot(1,2,2);
hist(data.w(1,:,1))
title('crossing ratio');

figure(100+angle+2);
colormap default
clf
hold on
for indx=1:5

    ind=randi(size(data.signal,2));
    ind2=1;

    %% signal + ground truth
    subplot(3,5,indx);
    hold on
    title([num2str(ind),'  ',num2str(ind2)])
    s=data.signal(2:end,ind,ind2);
    snorm=data.signal(1,ind,ind2);
    s=s./repmat(snorm,size(s,1),1);

    bdirs_w=(bdirs.*repmat(s',3,1));
    eul = data.euler_angles(ind, :);
    bdirs_w = rotz(-eul(3))*roty(-eul(2))*rotx(-eul(1))*bdirs_w;
    CH=convhull(bdirs');

    trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none','FaceVertexCData',s(:));


    axis equal
    axis(1.5*[-1 1 -1 1 -1 1])


    %% reconstructed signal (raw -> sh -> raw')
    subplot(3,5,indx+5);
    hold on
    title('rec s')
    s=signal_rec(:,ind,ind2);
    bdirs_w=(bdirs.*repmat(s',3,1));
    bdirs_w = rotz(-eul(3))*roty(-eul(2))*rotx(-eul(1))*bdirs_w;
    CH=convhull(bdirs');

    trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
    axis equal
    axis(1.5*[-1 1 -1 1 -1 1])


    %% reconstructed gt (raw -> sh -> raw')
    subplot(3,5,indx+10);
    hold on
    title('rec fodf')
    s=8*fodf(:, ind);

    bdirs_w=(output_dirs.*repmat(s',3,1));
    bdirs_w = rotz(-eul(3))*roty(-eul(2))*rotx(-eul(1))*bdirs_w;
    CH=convhull(output_dirs');

    trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
    axis equal
    axis(1*[-1 1 -1 1 -1 1])            
end;
end;
