%%
%bten=zeros([3,3,128]);for a=1:128,bten(:,:,a)=dirs.bDir{128}(:,a)*dirs.bDir{128}(:,a)';end;

% ten [3 x 3 x N]
% b = 1000 = "1"
% nt = 1000 verschiedene diff parameter gesa,plet
% D1 parallel in axon
% D2 parallel out of axon
% D3 parallel out of axon orthogonal
% 
% standart
% D1=2.2
% D2=1.5
% D3=0.5 

% V axon [0, 1]
% Vw intra and extra water V+Vw = 1

% ndir = anzahl richtung



function data = genDWIsignalCrossings(ten, angle, second_ratio, key, nz)

    ten = ten/1000;
    nt = 100; %n_sample of configuation
    N = nt * 0.8;
    n_rotations = 100;

    csf = 0.5; % water ratio

    rng(2);
    param = param_factory(nt, key);
    D1 = param.D1;
    D2 = param.D2;
    D3 = param.D3;
    Vi = param.Vi;
    Vf = param.Vf;
    Ve = param.Ve;
    
    
    %% sample fiber direction
    euler_angles = 180 * rand(n_rotations, 3);
    
    n = [1.0, 0.0, 0.0]';
    n = n ./ repmat(sqrt(sum(n.^2)),[3 1]); %normalization
    
    n1_rotated = zeros(3, n_rotations);
    for i_rot=1:n_rotations,
        n1_rotated(:, i_rot) = rotx(euler_angles(i_rot, 1))*roty(euler_angles(i_rot, 2))*rotz(euler_angles(i_rot, 3))*n;
    end;
    
    n2 = rotz(angle) * n;
    n2 = n2 ./ repmat(sqrt(sum(n2.^2)),[3 1]); %normalization
    n2_rotated = zeros(3, n_rotations);
    for i_rot=1:n_rotations,
        n2_rotated(:, i_rot) = rotx(euler_angles(i_rot, 1))*roty(euler_angles(i_rot, 2))*rotz(euler_angles(i_rot, 3))*n2;
    end;
    
    %% sample dispersion
    A = 0.1*(rand(1,size(D1,2)).^2) ;

    %% generate signal 
    S1 = genSigd(ten,n1_rotated,D1,D2,D3,Vi,Ve,Vf,A); 
    S2 = genSigd(ten,n2_rotated,D1,D2,D3,Vi,Ve,Vf,A); 
    
    data.shape_org=size(S1);
    
    shape=size(S1)
    shape=shape([2,1,3]);
    S1=(reshape(permute(S1,[2,1,3]),shape));
    S2=(reshape(permute(S2,[2,1,3]),shape));
    
    %w=1;
    W=[[1.0/(1.0 + second_ratio)]];
    w=(repmat(W,size(S1,1),size(S1, 2)));
    w=(reshape(repmat(w(:),1,size(S1,3)),shape));
    %w=reshape(repmat(rand([1,size(S1,1)]),1,size(S1,2)*size(S1,3)),shape);
    %w(:)=1;
    S=S1.*w+S2.*(1-w);
    S = abs(S+ nz*(randn(size(S))+1i*randn(size(S))));
    
    data.signal = S;
    data.n1= n;
    data.n2= n2;
    data.n1_rotated = n1_rotated;
    data.n2_rotated = n2_rotated;
    data.euler_angles = euler_angles;
    data.w=w;
    data.W=W;
    
    data.D1 = D1;
    data.D2 = D2;
    data.D3 = D3;
    data.Vi = Vi;
    data.Ve = Ve;
    data.Vf = Vf;




% generate shell signal
function S = genSig(qxx,b,D1,D2,D3,Vi,Ve,Vf,A)
   
S = SHDisp(qxx,b,D1,A).*repmat(permute(Vi,[1 3 2]),[size(qxx,1) size(qxx,2) 1]) + ...
    SHDisp(qxx,b,D2-D3,A).*repmat(permute(Ve,[1 3 2]),[size(qxx,1) size(qxx,2) 1]).* ...
     repmat(exp(-repmat(permute(D3,[1 3 2]),[1 size(qxx,2) 1]).*repmat(b',[1 1 length(D3)])),[size(qxx,1) 1 1]) + ...
     repmat(permute(Vf,[1 3 2]),[size(qxx,1) size(qxx,2) 1]).* ...
     repmat(exp(-repmat(permute(0*D3+3,[1 3 2]),[1 size(qxx,2) 1]).*repmat(b',[1 1 length(D3)])),[size(qxx,1) 1 1]);
 
function S = genSigd(ten,n,D1,D2,D3,Vi,Ve,Vf,A)
b = squeeze(ten(1,1,:) + ten(2,2,:) + ten(3,3,:));
qxx =    (n(1,:).^2)' * squeeze(ten(1,1,:))' + ...
         (n(2,:).^2)' * squeeze(ten(2,2,:))' + ...
         (n(3,:).^2)' * squeeze(ten(3,3,:))' + ...
       + 2*(n(1,:).*n(2,:))' * squeeze(ten(1,2,:))' + ... 
       + 2*(n(1,:).*n(3,:))' * squeeze(ten(1,3,:))' + ... 
       + 2*(n(3,:).*n(2,:))' * squeeze(ten(3,2,:))' ;   
S = genSig(qxx,b,D1,D2,D3,Vi,Ve,Vf,A);




% generate dispersed stick
function S = SHDisp(qxx,b,D,lam)

    lmax = 50;
    L = (0:1:lmax);
    f = exp(-lam'*(L.*(L+1)));
    f =f(:,1:2:end);
    buni = unique(round(b*10));
    
    t = 0:0.001:1; 
    
    p = 0.5*(myleg(lmax,t')+myleg(lmax,-t')) .* repmat(sqrt((2*L+1)),[size(t,2) 1]) /sqrt(length(t));
    p = p(:,1:2:end);
    S = ones(size(qxx,1),length(b),length(lam));
    for k = 2:length(buni),
        bD = D*buni(k)/10;
        idx = round(b*10)==buni(k);
        %P = ((exp(-bD'*t.^2)*p).*f)*p';
        P = ((exp(-bD'*t.^2)*p).*f)*pinv(p);
        
        it = sqrt(qxx(:,idx)/buni(k)*10); it(it>1) = 1; it(it<0) = 0;

        %r = interp1(t,real(P)',real(it));
        r = interp1(t,real(P)',real(it));

        % itx = floor(it*(length(t)-1))+1;
        % r = P(:,itx)';
        %r = reshape(r,[size(S,1),sum(idx),size(S,3)]);
        
        S(:,idx,:) = r;    
    end;

function p = myleg(n,x);


    if n == 0,
        p = x*0+1;
        return;
    end
    if n == 1
        p = [x*0+1 x];
        return;
    end;
    p = zeros(size(x,1),n+1);
    p(:,1:2) = [x*0+1 x];
    for k = 2:n,
        p(:,k+1) = ((2*k-1)*x.*p(:,k) - (k-1)*p(:,k-1))/k;    
    end;
           
