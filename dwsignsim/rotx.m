function R = rotx(angle)
    R = [[1, 0, 0]; [0, cos(pi*angle/180), -sin(pi*angle/180)]; [0, sin(pi*angle/180), cos(pi*angle/180)]];
end
