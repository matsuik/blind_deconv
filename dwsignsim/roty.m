function R = roty(angle)
    R = [[cos(pi*angle/180), 0, sin(pi*angle/180)]; [0, 1, 0]; [-sin(pi*angle/180), 0, cos(pi*angle/180)]];
end     