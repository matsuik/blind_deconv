function R = rotz(angle_degree)
    angle_radian = pi*angle_degree/180;
    R = [[cos(angle_radian), -sin(angle_radian), 0];[sin(angle_radian), cos(angle_radian), 0]; [0, 0, 1]];
end


