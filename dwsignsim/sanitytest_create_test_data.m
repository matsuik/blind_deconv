angle = 90;
ratio = 1;
simulation_dir = 'sanity_test_x_dw90HCP';
% ratio : how larger the second peak than the first
save_dir = fullfile('~/projects/blind_deconv/simulation', simulation_dir, 'test')
% loading gradient directions
load 'bdirs_whole_90HCP.mat'
nrealdirs=size(bdirs, 2)/2;

% we set the bvalue to 1000 and create the b-tensors
bvalue=3000;
bten=zeros([3,3,2*nrealdirs+1]);
for a=1:2*nrealdirs,
    bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';
end;

bten=bvalue*bten;
setting_dir = ['angle', num2str(angle), 'ratio', num2str(ratio)];
save_dir = fullfile(save_dir, setting_dir, 'data');
%%
% generating the signal
data = sanitytest_genDWIsignalCrossings(bten, angle, ratio, 0.4);

%% 


% scale invariance: deviding the signal by the b0 image
signal=data.signal(2:end,:)./((repmat(data.signal(1,:),size(data.signal,1)-1,1)));

    
alldirs=bdirs;


ms = load('bdirs_whole_1272.mat');
output_dirs = ms.bdirs;

%% fodf directions

input_L = 4;
output_L = 4;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
fodf_sh=fodf_n1*0.5+fodf_n2*0.5;

    Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of signal (harmonic domain -> spatial domain)
    signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);
       
    Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of fodf (harmonic domain -> spatial domain)
    fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
    fodf_for_vis = fodf;
    fodf = fodf(:, :);
%% 

if not(exist(save_dir))
    mkdir(save_dir)
end;


save(fullfile(save_dir, 'data_base.mat'), '-struct', 'data')
save(fullfile(save_dir, 'hardi_sh4.mat'), 'signal_sh')
save(fullfile(save_dir, 'fodf_reconst4.mat'), 'fodf')

train_save_dir = fullfile('~/projects/blind_deconv/simulation', simulation_dir, 'train');
if not(exist(train_save_dir))
    mkdir(train_save_dir)
end;
signal_sh = signal_sh(:, 1:2);
fodf = fodf(:, 1:2);

save(fullfile(train_save_dir, 'hardi_sh4.mat'), 'signal_sh', '-v7.3')
save(fullfile(train_save_dir, 'fodf_reconst4.mat'), 'fodf', '-v7.3')



%% fodf directions

input_L = 8;
output_L = 8;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
fodf_sh=fodf_n1*0.5+fodf_n2*0.5;

    Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of signal (harmonic domain -> spatial domain)
    signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);
       
    Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of fodf (harmonic domain -> spatial domain)
    fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
    fodf_for_vis = fodf;
    fodf = fodf(:, :);
%% 

if not(exist(save_dir))
    mkdir(save_dir)
end;


save(fullfile(save_dir, 'data_base.mat'), '-struct', 'data')
save(fullfile(save_dir, 'hardi_sh8.mat'), 'signal_sh')
save(fullfile(save_dir, 'fodf_reconst8.mat'), 'fodf')

train_save_dir = fullfile('~/projects/blind_deconv/simulation', simulation_dir, 'train');
if not(exist(train_save_dir))
    mkdir(train_save_dir)
end;
signal_sh = signal_sh(:, 1:2);
fodf = fodf(:, 1:2);

save(fullfile(train_save_dir, 'hardi_sh8.mat'), 'signal_sh', '-v7.3')
save(fullfile(train_save_dir, 'fodf_reconst8.mat'), 'fodf', '-v7.3')


%% fodf directions

input_L = 12;
output_L = 12;
%order for FODF
L=output_L;

signal_sh = discrete2sh(signal,alldirs, input_L);

% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1,L);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2,L);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
w1=(data.w(1:size(fodf_n1,1),:,:));
fodf_sh=fodf_n1.*w1+fodf_n2.*(1-w1);

data_base.input=signal_sh;
data_base.output=fodf_sh(:,:);


    Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of signal (harmonic domain -> spatial domain)
    signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);
       
    Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of fodf (harmonic domain -> spatial domain)
    fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);

fodf = fodf(:, :);
save(fullfile(save_dir, 'hardi_sh12.mat'), 'signal_sh')
save(fullfile(save_dir, 'fodf_reconst12.mat'), 'fodf')

%% 

signal_r = reshape(signal, [size(signal, 1), 2, 1, 1]);
signal_r = permute(signal_r, [2, 3, 4, 1]);

load bdirs_whole_90.mat
bdirs = bdirs';
bdirs = [bdirs, bvalue.*ones(size(bdirs, 1), 1)];
image.data = signal_r;
image.vox = [2, 2, 2];
image.DW_scheme = bdirs;

write_mrtrix(image, fullfile(save_dir, 'dwi.mif'))

if 0,
    signal_r = reshape(data.signal, [size(data.signal, 1), 10, 10, 8]);
    signal_r = permute(signal_r, [2, 3, 4, 1]);

    load bdirs_whole_90.mat
    bdirs = bdirs';
    bdirs = [bdirs, bvalue.*ones(size(bdirs, 1), 1)];
    bdirs = [[1, 0, 0, 5]; bdirs];
    image.data = signal_r;
    image.vox = [2, 2, 2];
    image.DW_scheme = bdirs;

    write_mrtrix(image, fullfile(save_dir, 'dwi.mif'))
end




%%

        figure(3);
        subplot(1,2,1);
        hist(rad2deg(acos(abs(sum(data.n1.*data.n2,1)))))
        title('crossing degree');
       

        figure(2);
        colormap default
        clf
        hold on
        for indx=1:3

            ind=randi(size(data.signal, 2));
            ind2=randi(size(data.signal,3));
            ind=indx;
            ind2=1;
      
            
            %% signal + ground truth
            subplot(3,5,indx);
            hold on
            title([num2str(ind),'  ',num2str(ind2)])
            s=data.signal(2:end,ind,ind2);
            snorm=data.signal(1,ind,ind2);
            s=s./repmat(snorm,size(s,1),1);
            
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');
            
            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none','FaceVertexCData',s(:));
            
            w1=0.5;
            w2=0.5;
            plot3(w1*[data.n1(1,ind),-data.n1(1,ind)],w1*[data.n1(2,ind),-data.n1(2,ind)],w1*[data.n1(3,ind),-data.n1(3,ind)],'linewidth',5)
            plot3(w2*[data.n2(1,ind),-data.n2(1,ind)],w2*[data.n2(2,ind),-data.n2(2,ind)],w2*[data.n2(3,ind),-data.n2(3,ind)],'linewidth',5)
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])
            view(2)
            
            
            %% reconstructed signal (raw -> sh -> raw')
            subplot(3,5,indx+5);
            hold on
            title('rec s')
            s=signal_rec(:,ind,ind2);
            bdirs_w=(bdirs.*repmat(s',3,1));
            CH=convhull(bdirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])
            view(2)

            
            %% reconstructed gt (raw -> sh -> raw')
            subplot(3,5,indx+10);
            hold on
            title('rec fodf')
            s=10*fodf_for_vis(:, ind, ind2);
            
            bdirs_w=(output_dirs.*repmat(s',3,1));
            CH=convhull(output_dirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])   
            view(2)
        end;

