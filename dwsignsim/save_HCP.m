function save_HCP(model_path, n_output_samples)
image_dwi3000 = read_mrtrix('../HCP/dwi3000.mif')
s_hcp = size(image_dwi3000.data) % (x, y, z, n_dw)

%model_path = '../HCP/b3000/db1000/rcn_results/MultiLayer_8_nlayer4_jin8_jout8_actrelu_bias0.005_wdl0.0_adam_0.001_1.0_'

pred_st = load(fullfile(model_path, 'pred.mat'))
% pred_st.pred (n_samples, n_dirs)

bw = load(['bdirs_whole_', num2str(n_output_samples), '.mat'])

signal_r = reshape(pred_st.pred, [s_hcp(1), s_hcp(2), s_hcp(3), size(pred_st.pred, 2)]); % (x, y, z, n_dirs) <- (n_samples, n_dirs)
bdirs = bw.bdirs; % (3, n_dirs)
bdirs = bdirs'; %(n_dirs, 3)
bvalue = 3000;
bdirs = [bdirs, bvalue.*ones(size(bdirs, 1), 1)];

image.data = signal_r; % (x, y, z, n_dirs)
image.vox = image_dwi3000.vox;
image.DW_scheme = bdirs;

write_mrtrix(image, fullfile(model_path, 'dixel_pred.mif'))
exit
end