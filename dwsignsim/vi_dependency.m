
d1_list = 1.5:0.1:3.0;
means = zeros(1, size(d1_list, 2));

for i_d1 = 1:size(d1_list, 2),
    i_d1
    
    angle = 90;
    ratio = 1;
    simulation_dir = 'sanity_test_x_dw90HCP';
    % ratio : how larger the second peak than the first
    save_dir = fullfile('~/projects/blind_deconv/simulation', simulation_dir, 'test')
    % loading gradient directions
    load 'bdirs_whole_90HCP.mat'
    nrealdirs=size(bdirs, 2)/2;

    % we set the bvalue to 1000 and create the b-tensors
    bvalue=3000;
    bten=zeros([3,3,2*nrealdirs+1]);
    for a=1:2*nrealdirs,
        bten(:,:,a+1)=bdirs(:,a)*bdirs(:,a)';
    end;

    bten=bvalue*bten;
    setting_dir = ['angle', num2str(angle), 'ratio', num2str(ratio)];
    save_dir = fullfile(save_dir, setting_dir, 'data');
    %%
    % generating the signal
    data = sanitytest_genDWIsignalCrossings(bten, angle, ratio, 0.8, d1_list(1, i_d1));

    %% 


    % scale invariance: deviding the signal by the b0 image
    signal=data.signal(2:end,:)./((repmat(data.signal(1,:),size(data.signal,1)-1,1)));


    alldirs=bdirs;


    ms = load('bdirs_whole_1272.mat');
    output_dirs = ms.bdirs;

    %% fodf directions

    input_L = 4;
    output_L = 4;
    %order for FODF
    L=output_L;

    signal_sh = discrete2sh(signal,alldirs, input_L);

    % shcoeffients of  delta impuls in n1 direction
    n1_sh=n2delta(data.n1,L);
    fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

    % shcoeffients of  delta impuls in n2 direction
    n2_sh=n2delta(data.n2,L);
    fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

    % weighted sum = fodf
    fodf_sh=fodf_n1*0.5+fodf_n2*0.5;

        Mcontra_signal = getSHMatrix(input_L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
        % reconstruction of signal (harmonic domain -> spatial domain)
        signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);

        Mcontra_fodf = getSHMatrix(L,output_dirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
        % reconstruction of fodf (harmonic domain -> spatial domain)
        fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
        fodf_for_vis = fodf;
    fodf = fodf(:, :);
    
    means(1, i_d1) = mean(signal_rec(:));
end
    