function visualize_fod(s, output_dirs)
bdirs_w=(output_dirs.*repmat(s',3,1));
            CH=convhull(output_dirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis(1*[-1 1 -1 1 -1 1])