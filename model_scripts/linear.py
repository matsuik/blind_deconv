# -*- coding:utf-8 -*-
import os
os.environ['THEANO_FLAGS'] = 'device=gpu'
os.system('nvidia-smi')

import sys
sys.path.append('/home/matsui-k/projects/blind_deconv')
from core import blind_utils, models_positive_const, training

os.environ['THEANO_FLAGS'] = 'device=gpu'
os.system('nvidia-smi')

args = training.handle_command()

train_dir = os.path.join('/home/matsui-k/projects/blind_deconv', 'simulation', args.simulation_dir, 'train')
assert os.path.exists(train_dir)

input_tensors = blind_utils.load_hardi_sh(args.input_L, train_dir)
if args.debug_data:
    input_tensors = [tens[:10000] for tens in input_tensors]

input_tensors, n_input_tensors_list = blind_utils.multi_order_mix(input_tensors,
                                                                  max_j_input=args.input_L, max_j_output=args.output_L)
fodf = blind_utils.load_fodf_reconst(args.output_L, train_dir)
if args.debug_data:
    fodf = fodf[:10000]

train_input, train_output, test_input, test_output, norm_coef = blind_utils.pre_blind_pc(input_tensors, fodf, args.normed,
                                                                                         train_ratio=args.train_ratio,
                                                                                         permu=args.not_permutate_train)

nn = models_positive_const.LinearCombination(
    j_input_list=range(0, args.output_L+1, 2), j_output_list=range(0, args.output_L+1, 2),
    n_input_tensors_list=n_input_tensors_list,
    reg_lambda=args.reg_lambda, reg_func_key=args.reg_func_key,
    error_func_key=args.error_func_key,
    pc_funk_key=args.pc_func_key,
    default_bias=args.default_bias,
    n_dirs=args.n_dirs)

training.train(train_input, train_output, test_input, test_output, norm_coef, nn, args)