function Mreal=comp2real(M)

full=false;
if full
    shape=size(M);
    Mreal=zeros([shape.*[2,2]]);


    Mreal(1:2:end,1:2:end)=real(M);
    Mreal(1:2:end,2:2:end)=-imag(M);
    Mreal(2:2:end,1:2:end)=imag(M);
    Mreal(2:2:end,2:2:end)=real(M);
else
    shape=size(M);
    Mreal=zeros([shape.*[2,1]]);

    Mreal(1:2:end,1:1:end)=real(M);
    Mreal(2:2:end,1:1:end)=imag(M);
    
end;