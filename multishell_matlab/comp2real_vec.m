function vreal=comp2real_vec(v)

full=false;

if full 
    if isreal(v)
        shape=size(v);
        shape(1)=shape(1)/2;
        vreal=zeros([shape]);

        vreal=v(1:2:end,:)+1i*v(2:2:end,:);    
    else
        shape=size(v);
        shape(1)=shape(1)*2;
        vreal=zeros([shape]);

        vreal(1:2:end,:)=real(v);
        vreal(2:2:end,:)=-imag(v);
    end;
else
     if isreal(v)
        vreal=v;
    else
        shape=size(v);
        shape(1)=shape(1)*2;
        vreal=zeros([shape]);

        vreal(1:2:end,:)=real(v);
        vreal(2:2:end,:)=-imag(v);
    end;
end;