function  [n1,n2]=createOrthsVec(v)
v=single(v);
ndirs=size(v,1);
vn1=repmat(single([0,0,1]),ndirs,1);
vn2=repmat(single([0,1,0]),ndirs,1);
vn3=repmat(single([1,0,0]),ndirs,1);

vn_1=cross(v,vn1);
vn_2=cross(v,vn2);
vn_3=cross(v,vn3);

cond_12=repmat(abs(v(:,1))>abs(v(:,2)),1,3);
cond_23=repmat(abs(v(:,2))>abs(v(:,3)),1,3);
cond_13=repmat(abs(v(:,1))>abs(v(:,3)),1,3);


n1= cond_12.*(cond_23.*vn1+~cond_23.*vn_2)...
   +(~cond_12).*(cond_13.*vn_1+~cond_13.*vn_3);



n1=n1./(repmat(sqrt(sum(n1.^2,2))+eps,1,3));
n2=cross(n1,v);
n1=cross(n2,v);
  

check0=sum(sum(n1.*n2,2));
check1=sum(sum(n1.*v,2));
check2=sum(sum(n2.*v,2));

if any(abs([check0,check1,check2])>0.00001)
   abs([check0,check1,check2])
   error('ahhh') 
end

return 
      if (abs(v(1))>abs(v(2)))
      
            if (abs(v(2))>abs(v(3)))
              vn1=[0,0,1];
            else

              vn1=cross(v,[0,1,0]);
            end
      else
        if (abs(v(1))>abs(v(3)))
          vn1=cross(v,[0,0,1]);
        else
          vn1=cross(v,[1,0,0]);
        end     
      end
      vn1=vn1./norm(vn1);
      vn2=cross(vn1,v);
  