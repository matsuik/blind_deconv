
%dfname='../data/dirs.mat';
dfname='./dirs.mat';

if exist(dfname,'file')
    load(dfname,'dirs128','dirs96','dirs64','dirs32')
    fprintf('exists')
else
    dirs128 = diffencfpair(64);dirs128=[dirs128,-dirs128 ];
    dirs96 = diffencfpair(48);dirs96 =[dirs96 ,-dirs96  ];
    dirs64 = diffencfpair(32);dirs64=[dirs64,-dirs64 ];
    dirs32 = diffencfpair(16);dirs32=[dirs32,-dirs32 ];
    save(dfname,'dirs128','dirs96','dirs64','dirs32')
end