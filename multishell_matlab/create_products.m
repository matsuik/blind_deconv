function fieldsr=create_products(field,Lstable,Lmax1,Lmax2,o)

fields={};
fields0={};

%Lmax1=4;
%Lmax2=Lmax;

p=[];
    for l1=0:2:min(field.L,Lstable)
        f1=stafieldStruct(field,l1);
        %for l2=0:2:min(4,field.L)
        for l2=0:2:field.L
            f2=stafieldStruct(field,l2);
            for l3=abs(l1-l2):(l1+l2)
                if (mod(l3,2)==0)  && (l3<=Lmax1)
                    p=[p;l1,l2,l3];
                    fields{numel(fields)+1}=sta_prod(f1,f2,l3);
                    fields{numel(fields)}.p=sprintf('%d %d -> %d\n',l1,l2,l3);
                    %fprintf('%d %d -> %d\n',l1,l2,l3);
                end
            end
        end 
     
            fields0{numel(fields0)+1}=f1;
            fields0{numel(fields0)}.p=sprintf('%d\n',l1);
            %fprintf('%d\n',l1);
       
    end

    
fieldso3={};
%%

p3=[];
for l1=0:2:field.L
    f1=stafieldStruct(field,l1);
    for f=1:numel(fields)
       f2=fields{f};
       l2=f2.L;
       for l3=abs(l1-l2):(l1+l2)
            if (mod(l3,2)==0)  && (l3<=Lmax2) %&& (l3>=l1)
                assert(l2==p(f,3))
                %fprintf('%d %d -> %d\n');
                p3=[p3;p(f,:),l1,l3,f];
                
                %fprintf('(%d %d -> %d) %d -> %d\n',p(f,:),l1,l3);
                %fieldso3{numel(fieldso3)+1}=sta_prod(f1,f2,l3);
                %fieldso3{numel(fieldso3)}.p=sprintf('(%d %d -> %d) %d -> %d\n',p(f,:),l1,l3);
            end
       end
    end
end
%


ordered=(p3(:,[1])<=p3(:,[2])) &  (p3(:,[2])<=p3(:,[4]));
p3=p3(ordered,:);
%p3_d=cat(2,sort(p3(:,[1,2,4]),2),p3(:,end-1:end));
%[v,idx]=unique(p3_d(:,1:4),'rows');
%p3=p3(idx,:);
%%



for p=1:size(p3,1)
    l1=p3(p,4);
    l3=p3(p,5);
    f=p3(p,6);
    f1=stafieldStruct(field,l1);
    f2=fields{f};
    l2=f2.L;
    %fprintf('(%d %d -> %d) %d -> %d\n',p3(p,1:5));
    fieldso3{numel(fieldso3)+1}=sta_prod(f1,f2,l3);
    fieldso3{numel(fieldso3)}.p=sprintf('(%d %d -> %d) %d -> %d\n',p3(p,1:5));
end

% 
% 
% p3=[];
% for l1=0:2:field.L
%     f1=stafieldStruct(field,l1);
%     for f=1:numel(fields)
%        f2=fields{f};
%        l2=f2.L;
%        for l3=abs(l1-l2):(l1+l2)
%             if (mod(l3,2)==0)  && (l3<=Lmax2) %&& (l3>=l1)
%                 %fprintf('%d %d -> %d\n');
%                 %p3=[p3,p,
%                 fprintf('(%d %d -> %d) %d -> %d\n',p(f,:),l1,l3);
%                 fieldso3{numel(fieldso3)+1}=sta_prod(f1,f2,l3);
%                 
%                 
%                 fieldso3{numel(fieldso3)}.p=sprintf('(%d %d -> %d) %d -> %d\n',p(f,:),l1,l3);
%             end
%        end
%     end
% end

fieldsr={};
if o(1)
    fieldsr=fields0;
end
if o(2)
    fieldsr={fieldsr{:},fields{:}};
end
if o(3)
    fieldsr={fieldsr{:},fieldso3{:}};
end








