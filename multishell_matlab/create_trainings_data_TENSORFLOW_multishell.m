function data_base=create_trainings_data_TENSORFLOW_multishell(varargin)
alpha=0:10:90;
ndir = 10;
nt = 10;
nz=0.05;

for k = 1:2:length(varargin),
    eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;
alpha %#ok<NOPRT>
nt %#ok<NOPRT>
nz %#ok<NOPRT>

nz=0.05;
%nz=0.0;
%raw_dirs32 = diffencfpair(16);raw_dirs32=[raw_dirs32,-raw_dirs32];

weight_dir=false;

bval_hex=[0 0 500 2000 1500 500 1500 500 1500 1500 2000 1000 1500 500 2000 1000 1500 500 500 1500 1500 1000 2000 1500 1500 1500 2000 1500 2000 ];
bvec_hex=[0 0 0 0 -0.166289553046227 -0.288187116384506 0.166194945573807 -0.288187116384506 0.166194945573807 -0.166289553046227 -0.289080113172531 0.408621430397034 -0.333756059408188 0.577255845069885 -0.289080113172531 0.408621430397034 -0.500138521194458 -0.865778863430023 0.866278886795044 0.499849766492844 -0.666467010974884 -0.816211104393006 0.577175915241242 -0.666467010974884 0.833393335342407 0.833393335342407 -0.866028547286987 0.999999761581421 0.866028547286987 
0 0 1 0.999999880790711 0.865866482257843 0.499390482902527 -0.288632363080978 -0.499390482902527 0.288632363080978 -0.865866482257843 0.499900430440903 -0.70681095123291 0 0 -0.499900430440903 0.70681095123291 0.865945160388947 0.500426709651947 0.499560654163361 0.866111874580383 0.577427268028259 0 0 -0.577427268028259 -0.288395881652832 0.288395881652832 0.499994307756424 0 0.499994307756424 
0 0 0 -0.000500000023748726 -0.471829354763031 -0.81704181432724 -0.942905426025391 -0.81704181432724 -0.942905426025391 -0.471829354763031 -0.8164142370224 -0.577448487281799 -0.942659497261048 -0.816563308238983 -0.8164142370224 -0.577448487281799 -0.000577139959204942 0 0 -0.000577579892706126 -0.471592515707016 -0.577753782272339 -0.816619873046875 -0.471592515707016 -0.471469283103943 -0.471469283103943 -0.000433020002674311 -0.000666669977363199 -0.000433020002674311 ];


%b0=find(bval_hex<eps);
b0=(bval_hex<eps);

bdirs=bvec_hex(:,~b0);


% 
% 
% raw_dirs64 = diffencfpair(32);raw_dirs64=[raw_dirs64,-raw_dirs64];
% bdirs=raw_dirs64 ;

nrealdirs=size(bvec_hex,2);



%bvalue=1000;
bten=zeros([3,3,nrealdirs]);
for a=1:nrealdirs,
    bten(:,:,a)=bvec_hex(:,a)*bvec_hex(:,a)';
end;

bv=permute((reshape(repmat(bval_hex.',1,9),[nrealdirs,3,3])),[3,2,1]);
bten=bten.*bv;

%%
% plot the sphere discretization
bdirsfull=[bdirs,-bdirs];
CH=convhull(bdirsfull');
figure(1);
trisurf(CH,bdirsfull(1,:),bdirsfull(2,:),bdirsfull(3,:));
axis equal

%%
% generating the signal

alpha_noise=0;
data = genDWIsignalCrossings2(bten,'ndir',ndir,'nt',nt,'nz',nz,'alpha',alpha,'alpha_noise',alpha_noise,'dir_weights_free',weight_dir);

data_shape=size(data.signal);

% scale invariance: dividing the signal by the b0 image
signal=data.signal(~b0,:)./((repmat((mean(data.signal(b0,:),1)),size(data.signal,1)-sum(b0),1)));
signal=cat(1,signal,signal);
%data.signal=cat(1,data.signal(end,:,:),cat(1,data.signal(2:end,:,:),data.signal(2:end,:,:)));
data.signal=cat(1,data.signal(b0,:,:),cat(1,data.signal(~b0,:,:),data.signal(~b0,:,:)));

bdirs=bdirsfull;
    
alldirs=bdirs;
alldirs_signal=bdirs;

%% mapping the signal to spherical harmonics



L=4;
Lgt=10;

signal_sh = discrete2sh_b(signal,alldirs,'L',L,'b',[bval_hex(~b0),bval_hex(~b0)]);


%% from here, my dirs

clear bdirs raw_dirs128
load ../dwsignsim/bdirs_whole_1272.mat
alldirs=bdirs;
clear bdirs

%% fodf directions

%order for FODF


% shcoeffients of  delta impuls in n1 direction
n1_sh=n2delta(data.n1,Lgt);
fodf_n1=(reshape(repmat(n1_sh(:),1,size(data.signal,3)),[size(n1_sh,1),size(data.signal,2),size(data.signal,3)]));

% shcoeffients of  delta impuls in n2 direction
n2_sh=n2delta(data.n2,Lgt);
fodf_n2=(reshape(repmat(n2_sh(:),1,size(data.signal,3)),[size(n2_sh,1),size(data.signal,2),size(data.signal,3)]));

% weighted sum = fodf
%w1=(data.w(1:size(fodf_n1,1),:,:));
w1=(reshape(repmat(data.w(1,:),size(fodf_n1,1),1),size(fodf_n1))); %#ok<NASGU>

if weight_dir
   fodf_sh=fodf_n1.*w1+fodf_n2.*(1-w1); %#ok<UNRCH>
else
   fodf_sh=fodf_n1+fodf_n2; 
end

%w1=0.5;


%%
data_base.input=single(signal_sh);
data_base.output=single(fodf_sh(:,:));
data_base.L=L;
data_base.ngradientdirs=single(nrealdirs);
data_base.signal_raw=single(signal);
data_base.grad_dirs=single(alldirs_signal);
data_base.angle=data.alpha;
data_base.dirs=data_shape(2);
data_base.dsamples=data_shape(3);
data_base.n1 = data.n1;
data_base.n2 = data.n2;
%%

   
    Mcontra_signal = getSHMatrix(L,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    % reconstruction of signal (harmonic domain -> spatial domain)
    signal_rec=reshape(real((Mcontra_signal.'*signal_sh(:,:))),[size(Mcontra_signal,2),size(fodf_sh,2),size(fodf_sh,3)]);
   
%%    
    Mcontra_fodf = getSHMatrix(Lgt,alldirs,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',1);
    
    % reconstruction of fodf (harmonic domain -> spatial domain)
    fodf=reshape(real((Mcontra_fodf .'*fodf_sh(:,:))),[size(Mcontra_fodf,2),size(fodf_sh,2),size(fodf_sh,3)]);
    
%%

fodf_2=real((Mcontra_fodf .'*fodf_sh(:,:)));
data_base.output_raw=single(max(fodf_2,0));

%%

        figure();
        subplot(1,2,1);
        hist(rad2deg(acos(abs(sum(data.n1.*data.n2,1)))))
        title('crossing degree');
        
        subplot(1,2,2);
        hist(data.w(1,:,1))
        title('crossing ratio');

        figure();
        colormap default
        clf
        hold on
        for indx=1:5

            ind=randi(size(data.signal,2));
            ind2=randi(size(data.signal,3));
            
            %% signal + ground truth
            subplot(3,5,indx);
            hold on
            title([num2str(ind),'  ',num2str(ind2)])
            s=data.signal(~b0,ind,ind2);
            snorm=data.signal(1,ind,ind2);
            s=s./repmat(snorm,size(s,1),1);
            s=[s',s'];
            
            bdirs_w=(alldirs_signal.*repmat(s,3,1));
            CH=convhull(alldirs_signal');
            
            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','none','FaceVertexCData',s(:));
            
            w1=data.W(ind);
            w2=1-w1;
            plot3(w1*[data.n1(1,ind),-data.n1(1,ind)],w1*[data.n1(2,ind),-data.n1(2,ind)],w1*[data.n1(3,ind),-data.n1(3,ind)],'linewidth',5)
            plot3(w2*[data.n2(1,ind),-data.n2(1,ind)],w2*[data.n2(2,ind),-data.n2(2,ind)],w2*[data.n2(3,ind),-data.n2(3,ind)],'linewidth',5)
            axis equal
            axis(1.5*[-1 1 -1 1 -1 1])
            
            
            %% reconstructed signal (raw -> sh -> raw')
            subplot(3,5,indx+5);
            hold on
            title('reconstruction (signal)')
            s=signal_rec(:,ind,ind2);
            bdirs_w=(alldirs.*repmat(s',3,1));
            CH=convhull(alldirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','black');
            axis equal
            axis(max(abs(s))*[-1 1 -1 1 -1 1])

            
            %% reconstructed gt (raw -> sh -> raw')
            subplot(3,5,indx+10);
            hold on
            title(['reconstruction (directions)  ',num2str(dot(data.n1(:,ind),data.n2(:,ind)))])
            s=4*fodf(:,ind,ind2);
            
            bdirs_w=(alldirs.*repmat(s',3,1));
            CH=convhull(alldirs');

            trimesh(CH,bdirs_w(1,:),bdirs_w(2,:),bdirs_w(3,:),'FaceColor','interp','FaceVertexCData',s(:),'EdgeColor','none');
            axis equal
            axis([-1 1 -1 1 -1 1])            
        end;