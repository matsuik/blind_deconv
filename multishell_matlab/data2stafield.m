function field=data2stafield(data,L)

shape_in=size(data);
shape=[1,1,prod(shape_in(2:end))];

field=stafieldStruct(shape,L,'STA_OFIELD_EVEN','STA_FIELD_STORAGE_C',class(data));
field.data(1,:)=real(data(:));
field.data(2,:)=imag(data(:));