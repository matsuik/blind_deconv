function ofield = discrete2sh_b(ndata,dir,varargin)

L = 4;
type = 'STA_OFIELD_EVEN';
storage = 'STA_FIELD_STORAGE_C';
pseudoinverse = true;
pseudoinverse = false;
for k = 1:2:numel(varargin),
        eval(sprintf('%s=varargin{k+1};',varargin{k}));
end;


if length(size(dir)) == 3,
    for k = 1:size(dir,3),
        [V D] = eigs(dir(:,:,k),1);
        bDir(:,k) = V(:,1);
    end;
else
    bDir = dir;
end;



bvals=unique(b);

for t=1:numel(bvals)
    b_shell=(b==bvals(t));
    bDir_shell=bDir(:,b_shell);
    %M= getSHMatrix(L,bDir_shell,type,storage,0);
    M= getSHMatrix(L,bDir_shell,type,storage,0)/sum(b_shell);
    q=sqrt(bvals(t)/1000);
    
    dfac=@(x)gamma(x+1/2).*2.^x/sqrt(pi);
    
    QW=[];
    for l=0:2:L
        %QW=[QW;repmat(q^l*exp(-q*q)/dfac(l),2*l+1,1)];
        QW=[QW;repmat(q^l*exp(-q*q),2*l+1,1)];
    end
    QW=repmat(QW,1,size(M,2));
    M=M.*QW;
    ndata_shell=ndata(b_shell,:);
    
    %if pseudoinverse,
    %    tmp = reshape(pinv(M')*ndata(:,:),[size(M,1) size(ndata,2) size(ndata,3) size(ndata,4)]) ;
    %else
        tmp = reshape(M*ndata_shell(:,:),[size(M,1) size(ndata_shell,2) size(ndata_shell,3) size(ndata_shell,4)]) ;
    %end;
    
    if t==1
        ofield=tmp;
    else
        ofield=ofield+tmp;
    end
    
    
end

