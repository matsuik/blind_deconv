%%
%bten=zeros([3,3,128]);for a=1:128,bten(:,:,a)=dirs.bDir{128}(:,a)*dirs.bDir{128}(:,a)';end;

% ten [3 x 3 x N]
% b = 1000 = "1"
% nt = 1000 verschiedene diff parameter gesa,plet
% D1 parallel intra
% D2 parallel extra
% D3 parallel extra orthogonal zur faser
% 
% standart
% D1=2.2
% D2=1.5
% D3=0.5 

% V intra axional volumenfactro
% Vw freie wasser

% ndir = anzahl richtung



function data = genDWIsignalCrossings2(ten,varargin)


    ten = ten/1000;
  
    alpha=0:10:90;
    alpha_noise=0;
    
    nt = 1000;
    ndir = 100;
    nz = 0.02;

    csf = 0.5;
    
    dir_weights_free=false;
    
    
    for k = 1:2:length(varargin),
            eval(sprintf('%s=varargin{k+1};',varargin{k}));
    end;
    
    D1 = 0.2+rand(nt,1)*2.2;
    D2 = 0.2+rand(nt,1)*1.5;
    D3 = 0.2+rand(nt,1)*0.5;    
%     D1 = 0.2+rand(nt,1)*2.8;
%     D2 = 0.2+rand(nt,1)*2.8;
%     D3 = 0.2+rand(nt,1)*1.3;

    V = rand(nt,1)*1;
    Vw = (1-V).*rand(nt,1)*csf;
    
    idx = find(abs(D1-(D2+2*D3))<1.5);

    D1 = D1(idx)';
    D2 = D2(idx)';
    D3 = D3(idx)';
    Vi = V(idx)';
    Vf = Vw(idx)';
    Ve = 1-V(idx)'-Vw(idx)';
    
    %% sample fiber direction
    nalpha=numel(alpha);
    ndir=nalpha*floor(ndir/nalpha);
    fprintf('dirs: %d\n',ndir)
    
    if ndir==1 % for test data
        n=[1.;0.;0.];
        n2=rotz(alpha)*n;
        fprintf('test n: %d, %d\n',size(n))
        fprintf('test n2: %d, %d\n',size(n2))
        alphas=reshape(repmat(alpha,ndir/nalpha,1),1,ndir);
        alphas=repmat(alphas+alpha_noise*randn(size(alphas)),3,1);
    else
        n = randn(3,ndir);
        n = n ./ repmat(sqrt(sum(n.^2)),[3 1]);
        fprintf('train n: %d, %d\n',size(n))
        [n1,~]=createOrthsVec(n.');
        fprintf('train n1: %d, %d\n',size(n1))
        alpha=alpha*2*pi/360;
        alpha_noise=alpha_noise*2*pi/360;
        alphas=reshape(repmat(alpha,ndir/nalpha,1),1,ndir);
        alphas=repmat(alphas+alpha_noise*randn(size(alphas)),3,1);
        n2=real(sin(alphas).*n1.'+cos(alphas).*n);
        fprintf('train n2: %d, %d\n',size(n2))
    end
    
    %n2 = randn(3,ndir);
    %n2 = n2 ./ repmat(sqrt(sum(n2.^2)),[3 1]);
    
    
    %% sample dispersion
    A = 0.1*(rand(1,size(D1,2)).^2) ;


    %% generate signal 
    S1 = genSigd(ten,n,D1,D2,D3,Vi,Ve,Vf,A); 
    S2 = genSigd(ten,n2,D1,D2,D3,Vi,Ve,Vf,A); 
    
    data.shape_org=size(S1);
    
    shape=size(S1);
    shape=shape([2,1,3]);
    S1=(reshape(permute(S1,[2,1,3]),shape));
    S2=(reshape(permute(S2,[2,1,3]),shape));
    
    
    %w=1;
    if dir_weights_free
        W=rand([1,size(S1,2)]); %#ok<UNRCH>
    else
        W=max(0.5+0.1*rand([1,size(S1,2)]),0);
    end
    
    
    w=(repmat(W,size(S1,1),1));
    w=(reshape(repmat(w(:),1,size(S1,3)),shape));
    %w=reshape(repmat(rand([1,size(S1,1)]),1,size(S1,2)*size(S1,3)),shape);
    %w(:)=1;
    S=S1.*w+S2.*(1-w);
    S = abs(S+ nz*(randn(size(S))+1i*randn(size(S))));
    
    
    
    
    
    data.alpha=single(alphas(1,:));
    data.signal = S;
    data.n1= n;
    data.n2= n2;
    data.w=w(1,:,:);
    data.W=W;
    
    data.D1 = D1;
    data.D2 = D2;
    data.D3 = D3;
    data.Vi = Vi;
    data.Ve = Ve;
    data.Vf = Vf;




% generate shell signal
function S = genSig(qxx,b,D1,D2,D3,Vi,Ve,Vf,A)
   
S = SHDisp(qxx,b,D1,A).*repmat(permute(Vi,[1 3 2]),[size(qxx,1) size(qxx,2) 1]) + ...
    SHDisp(qxx,b,D2-D3,A).*repmat(permute(Ve,[1 3 2]),[size(qxx,1) size(qxx,2) 1]).* ...
     repmat(exp(-repmat(permute(D3,[1 3 2]),[1 size(qxx,2) 1]).*repmat(b',[1 1 length(D3)])),[size(qxx,1) 1 1]) + ...
     repmat(permute(Vf,[1 3 2]),[size(qxx,1) size(qxx,2) 1]).* ...
     repmat(exp(-repmat(permute(0*D3+3,[1 3 2]),[1 size(qxx,2) 1]).*repmat(b',[1 1 length(D3)])),[size(qxx,1) 1 1]);
 
function S = genSigd(ten,n,D1,D2,D3,Vi,Ve,Vf,A)
b = squeeze(ten(1,1,:) + ten(2,2,:) + ten(3,3,:));
qxx =    (n(1,:).^2)' * squeeze(ten(1,1,:))' + ...
         (n(2,:).^2)' * squeeze(ten(2,2,:))' + ...
         (n(3,:).^2)' * squeeze(ten(3,3,:))' + ...
       + 2*(n(1,:).*n(2,:))' * squeeze(ten(1,2,:))' + ... 
       + 2*(n(1,:).*n(3,:))' * squeeze(ten(1,3,:))' + ... 
       + 2*(n(3,:).*n(2,:))' * squeeze(ten(3,2,:))' ;   
S = genSig(qxx,b,D1,D2,D3,Vi,Ve,Vf,A);




% generate dispersed stick
function S = SHDisp(qxx,b,D,lam)

    lmax = 50;
    L = (0:1:lmax);
    f = exp(-lam'*(L.*(L+1)));
    f =f(:,1:2:end);
    buni = unique(round(b*10));
    
    t = 0:0.001:1; 
    
    p = 0.5*(myleg(lmax,t')+myleg(lmax,-t')) .* repmat(sqrt((2*L+1)),[size(t,2) 1]) /sqrt(length(t));
    p = p(:,1:2:end);
    S = ones(size(qxx,1),length(b),length(lam));
    for k = 2:length(buni),
        bD = D*buni(k)/10;
        idx = round(b*10)==buni(k);
        %P = ((exp(-bD'*t.^2)*p).*f)*p';
        P = ((exp(-bD'*t.^2)*p).*f)*pinv(p);
        
        it = sqrt(qxx(:,idx)/buni(k)*10); it(it>1) = 1; it(it<0) = 0;

        %r = interp1(t,real(P)',real(it));
        r = interp1(t,real(P)',real(it));

        % itx = floor(it*(length(t)-1))+1;
        % r = P(:,itx)';
        %r = reshape(r,[size(S,1),sum(idx),size(S,3)]);
        
        S(:,idx,:) = r;    
    end;

function p = myleg(n,x)


    if n == 0,
        p = x*0+1;
        return;
    end
    if n == 1
        p = [x*0+1 x];
        return;
    end;
    p = zeros(size(x,1),n+1);
    p(:,1:2) = [x*0+1 x];
    for k = 2:n,
        p(:,k+1) = ((2*k-1)*x.*p(:,k) - (k-1)*p(:,k-1))/k;    
    end;
