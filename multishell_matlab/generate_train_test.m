simulation_dirname = 'multishell_test_along_axes';

train_dir = fullfile('~/projects/blind_deconv/simulation', ...
    simulation_dirname, 'train');

[data_base, fields] = create_tranings_data_TENSORFLOW_2_FEATS(create_trainings_data_TENSORFLOW_multishell('ndir', 1000, 'nt', 100));

if not(exist(train_dir, 'dir'))
    [status, msg, msgID] = mkdir(train_dir);
end;
save(fullfile(train_dir, 'data_base.mat'), '-struct', 'data_base', '-v7.3');
save(fullfile(train_dir, 'fields.mat'), 'fields');

test_dir = fullfile('~/projects/blind_deconv/simulation', ...
    simulation_dirname, 'test');
if not(exist(test_dir, 'dir'))
    [status, msg, msgID] = mkdir(test_dir);
end;

for angle=20:10:90
    [data_base, fields] = create_tranings_data_TENSORFLOW_2_FEATS(create_trainings_data_TENSORFLOW_multishell('alpha', angle, 'ndir', 1, 'nt', 100));
    
    angle_dir = fullfile(test_dir, ['angle', num2str(angle), 'ratio1'], 'data') %#ok<NOPTS>
    if not(exist(angle_dir, 'dir'))
        [status, msg, msgID] = mkdir(angle_dir);
    end;
    save(fullfile(angle_dir, 'data_base.mat'), '-struct', 'data_base', '-v7.3');
    save(fullfile(angle_dir, 'fields.mat'), 'fields');
end;
