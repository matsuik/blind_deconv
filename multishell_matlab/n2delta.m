function sh=n2delta(n,L)


 ncomponents=(L+1)*(L+2)/2;
 
 
% sh=zeros([ncomponents,size(n,2)]);
% for a=1:size(sh,2)
%     count=1;
%     for l=0:2:L
%         for m=-l:1:l
%             sh(count,a)=sta_sphericalHarmonic(l,m,n(1,a),n(2,a),n(3,a));
%             count=count+1;
%         end;
%     end;
% end;

offset=1;
sh=zeros([ncomponents,size(n,2)]);
for l=0:2:L
  sh(offset:offset+2*l,:)=mysh(n,l);
    offset=offset+2*l+1;
end;

