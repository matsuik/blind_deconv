# -*- coding:utf-8 -*-
import os
import sys
opj = os.path.join

os.environ['THEANO_FLAGS'] = 'device=cuda'
os.system('nvidia-smi')

sys.path.append('/home/matsui-k/projects/blind_deconv')
from core import blind_utils, models_positive_const, training, multishell_utils

args = training.handle_command()
train_dir = os.path.join('/home/matsui-k/projects/blind_deconv', 'simulation', args.simulation_dir, 'train')
assert os.path.exists(train_dir)
input_tensors, n_input_tensors_list = multishell_utils.load_hardi_coupled_sh(train_dir)

fodf = multishell_utils.load_fodf_reconst(train_dir)

train_input, train_output, test_input, test_output, norm_coef = multishell_utils.preprocess(input_tensors, fodf)
# tensor couplingでnnへの入力のlmaxはoutput_Lになってる

nn = models_positive_const.ThreeLayer(
    j_input_list=range(0, args.output_L+1, 2),
    j_output_list=range(0, args.output_L+1, 2),
    n_input_tensors_list=n_input_tensors_list,
    n_hidden=args.n_hidden,
    func_key_list=[args.act_func_key for _ in range(3)],
    reg_lambda=args.reg_lambda,
    reg_func_key=args.reg_func_key,
    error_func_key=args.error_func_key,
    pc_funk_key=args.pc_func_key,
    n_dirs=args.n_dirs,
    default_bias=args.default_bias,
    weight_decay_lambda=args.weight_decay_lambda,
    weight_decay_func_key=args.weight_decay_func_key)

f_train = training.train(train_input, train_output, test_input, test_output, norm_coef, nn, args)