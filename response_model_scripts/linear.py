# -*- coding:utf-8 -*-
import os
import sys
sys.path.append('/home/matsui-k/projects/blind_deconv')
os.environ['THEANO_FLAGS'] = 'device=cuda,optimizer=fast_compile'
#os.environ['THEANO_FLAGS'] = 'device=cuda'
os.system('nvidia-smi')
import theano
print theano.config.optimizer
sys.stdout.flush()
from core import blind_utils, response_models, response_training


args = response_training.handle_command()

train_dir = os.path.join('/home/matsui-k/projects/blind_deconv', 'simulation', args.simulation_dir, 'train')
assert os.path.exists(train_dir)

input_tensors = blind_utils.load_fodf_sh(args.input_L, train_dir)
if args.debug_data:
    input_tensors = [tens[:10000] for tens in input_tensors]

hardi_q = blind_utils.load_hardi_qspace(train_dir)
if args.debug_data:
    hardi_q = hardi_q[:10000]

train_input, train_output, test_input, test_output, norm_coef = blind_utils.pre_blind_pc(input_tensors, hardi_q, args.normed,
                                                                                         train_ratio=args.train_ratio,
                                                                                         permu=args.not_permutate_train)
nn = response_models.LinearCombination(
    j_input_list=range(0, args.output_L+1, 2), j_output_list=range(0, args.output_L+1, 2),
    m_reconstruct=blind_utils.load_Mcontra_signal(max_L=args.output_L, dirpath=train_dir),
    reg_func_key=args.reg_func_key,
    error_func_key=args.error_func_key,
    pc_funk_key=args.pc_func_key)

response_training.train(train_input, train_output, test_input, test_output, norm_coef, nn, args)