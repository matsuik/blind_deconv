import subprocess

simulation_dir_list = ['b3000_gauss_15_1272_snr20', 'b3000_gaussian_snr20_dw30', 'b3000_gaussian_snr20_dw90']
n_layer_list = [4]
bias_list = [0.0, 0.1]

for simulation_dir in simulation_dir_list:
    for bias in bias_list:
        for i_seed in range(5):
            for n_layer in n_layer_list:
                with open('tmp/srun_{}_{}.log'.format(simulation_dir, n_layer), 'w') as f:
                    subprocess.Popen(
                        "bash shell_utils/run_rcn_kng.sh 40 'python model_scripts/multiLayer.py {s} {l} 8 8 --default_bias {b} --note Seed{seed}'".format(
                        s=simulation_dir, l=n_layer, b=bias, seed=i_seed
                    ), shell=True, stdout=f)
