import subprocess

simulation_dir_list = ['dw30_ndirs10000_nt100_out256_snr20_b3000_gauss',
                       'dw90HCP_ndirs10000_nt100_out256_snr20_b3000_gauss']
n_layer_list = [4]

for simulation_dir in simulation_dir_list:
    with open('tmp/srun_{}_scalar_{}.log'.format(simulation_dir, 0), 'w') as f:
        subprocess.Popen("bash shell_utils/run_rcn_kng.sh 100 'python model_scripts/scalar.py {s} {l} 8 8 --n_dirs 256'".format(
            s=simulation_dir, l=0
        ), shell=True, stdout=f)

    for n_layer in n_layer_list:
        for i_seed in range(5):
            with open('tmp/srun_{}_{}.log'.format(simulation_dir, n_layer), 'w') as f:
                subprocess.Popen(
                    "bash shell_utils/run_rcn_kng.sh 100 'python model_scripts/multiLayer.py {s} {l} 8 8 --n_dirs 256 --note Seed{seed}'".format(
                    s=simulation_dir, l=n_layer, seed=i_seed
                ), shell=True, stdout=f)


