import subprocess

simulation_dir_list = ['b3000_gauss_15_1272_snr20', 'b3000_gaussian_snr20_dw30', 'b3000_gaussian_snr20_dw90']
bias_list = [0.01]
list_alpha = [1.0, 2.0, 5.0, 10.0]

for simulation_dir in simulation_dir_list:
    for bias in bias_list:
        for alpha in list_alpha:
            # for n_layer in n_layer_list:
            #     with open('tmp/srun_{}_{}.log'.format(simulation_dir, n_layer), 'w') as f:
            #         subprocess.Popen(
            #             "bash shell_utils/run_rcn_kng.sh 40 'python model_scripts/multiLayer.py {s} {l} 8 8 --default_bias {b} --error_func_key l2 --alpha {a} --opt_algo sgd'".format(
            #             s=simulation_dir, l=n_layer, b=bias, a=alpha
            #         ), shell=True, stdout=f)

            with open('tmp/srun_{}_linear_{}.log'.format(simulation_dir, 0), 'w') as f:
                subprocess.Popen("bash shell_utils/run_rcn_kng.sh 40 'python model_scripts/linear.py {s} {l} 8 8 --default_bias {b} --alpha {a} --opt_algo sgd --n_epochs 301 --lr_decay 0.1 --decay_epoch_list 150 225 262'".format(
                    s=simulation_dir, l=0, b=bias, a=alpha
                ), shell=True, stdout=f)

            with open('tmp/srun_{}_scalar_{}.log'.format(simulation_dir, 0), 'w') as f:
                subprocess.Popen("bash shell_utils/run_rcn_kng.sh 40 'python model_scripts/scalar.py {s} {l} 8 8 --default_bias {b}  --alpha {a} --opt_algo sgd --n_epochs 301 --lr_decay 0.1 --decay_epoch_list 150 225 262'".format(
                    s=simulation_dir, l=0, b=bias, a=alpha
                ), shell=True, stdout=f)

