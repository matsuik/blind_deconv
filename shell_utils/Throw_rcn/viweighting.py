import subprocess

simulation_dir_list = ['viweighting_ndirs10000_nt1000_dwHCP90_out256_snr20_b3000_gauss']
n_layer_list = [2, 4]
bias_list = [0.0]
pc_key_list = ['l-relu', 'sigmoid']

for simulation_dir in simulation_dir_list:
    for pc_key in pc_key_list:
        for bias in bias_list:
            for i_seed in range(3):
                for n_layer in n_layer_list:
                    with open('tmp/srun_{}_{}.log'.format(simulation_dir, n_layer), 'w') as f:
                        subprocess.Popen(
                            "bash shell_utils/run_rcn_kng.sh 100 'python model_scripts/multiLayer.py {s} {l} 8 8 --default_bias {b} --note Seed{seed} --pc_func_key {pc} --n_dirs 256'".format(
                            s=simulation_dir, l=n_layer, b=bias, seed=i_seed, pc=pc_key
                        ), shell=True, stdout=f)

                with open('tmp/srun_{}_scalar_{}.log'.format(simulation_dir, 0), 'w') as f:
                    subprocess.Popen(
                        "bash shell_utils/run_rcn_kng.sh 100 'python model_scripts/scalar.py {s} {l} 8 8 --default_bias {b} --note Seed{seed} --pc_func_key {pc} --n_dirs 256'".format(
                            s=simulation_dir, l=0, b=bias, seed=i_seed, pc=pc_key
                        ), shell=True, stdout=f)
