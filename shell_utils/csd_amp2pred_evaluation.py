# -*- coding: utf-8 -*-
"""
mauiで動かす
b3000_uniformとかをsimulation_dirに与えると, まだpred.matがないangle60ratio1とかで
amp.mifをpred.matに変えて、evaluationもしてくれる
"""
import time
import os, sys
import argparse
import subprocess
sys.path.append('/home/matsui-k/projects/blind_deconv')

from core import blind_utils

parser = argparse.ArgumentParser()
parser.add_argument('simulation_dir', type=str)
parser.add_argument('lmax', type=str)
parser.add_argument('reg', type=str)
parser.add_argument('--force', action='store_true', default=False)
parser.add_argument('--only_evaluation', action='store_true', default=False)
args = parser.parse_args()

project = os.path.expanduser('~/projects/blind_deconv/')
head = os.path.join(project, 'simulation')
simulation_dir = os.path.join(head, args.simulation_dir)

test_dir = os.path.join(simulation_dir, 'test')
angle_ratio_dir_list = [os.path.join(test_dir, d) for d in os.listdir(test_dir) if 'DS_Store' not in d]
print angle_ratio_dir_list
for angle_ratio_dir in angle_ratio_dir_list:
    if args.force or (not os.path.exists(os.path.join(angle_ratio_dir, 'csd_results', 'lmax{}reg{}'.format(args.lmax, args.reg), 'pred.mat'))):
        if not args.only_evaluation:
            cmd = ' && '.join(['cd {}'.format(os.path.join(angle_ratio_dir, 'csd_results', 'lmax{}reg{}'.format(args.lmax, args.reg))),
                               'matlab -nojvm -nodisplay -nosplash -r "addpath {}; amp2pred; quit"'.format(os.path.join(project, 'dwsignsim'))
                               ])
            p = subprocess.Popen(cmd, shell=True)
            p.wait()

            os.remove(os.path.join(angle_ratio_dir, 'csd_results', 'lmax{}reg{}'.format(args.lmax, args.reg), 'fod.mif'))

        cmd = 'python {script} {ar_dir} {result_dir}'.format(
            script=os.path.join(project, 'core/evaluation.py'),
            ar_dir=angle_ratio_dir,
            result_dir=os.path.join(angle_ratio_dir, 'csd_results', 'lmax{}reg{}'.format(args.lmax, args.reg)))
        p = subprocess.Popen(cmd, shell=True)
        p.wait()


time.sleep(10)
blind_utils.summarize_over_angles2df(args.simulation_dir, 'lmax{}reg{}'.format(args.lmax, args.reg), 1, 0,
                                     os.path.join('/home/matsui-k/projects/blind_deconv/simulation', args.simulation_dir,
                                                  'trained_models/lmax{}reg{}/not_rotated.png'.format(args.lmax, args.reg)),
                                     method='csd')

blind_utils.summarize_over_angles2df(args.simulation_dir, 'lmax{}reg{}'.format(args.lmax, args.reg), 1, 1,
                                     os.path.join('/home/matsui-k/projects/blind_deconv/simulation', args.simulation_dir,
                                                  'trained_models/lmax{}reg{}/rotated.png'.format(args.lmax, args.reg)),
                                     method='csd')

