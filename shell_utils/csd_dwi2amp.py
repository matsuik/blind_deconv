# -*- coding: utf-8 -*-
"""
kngで動かす
b3000_uniformとかをsimulation_dirに与えると, まだcsd_resultsがないangle60ratio1とかで
csdをやって、amp.mifを返してくれる,
"""
import os
import argparse
import subprocess

parser = argparse.ArgumentParser()
parser.add_argument('simulation_dir', type=str)
parser.add_argument('lmax', type=str)
parser.add_argument('reg', type=str)
parser.add_argument('--force', action='store_true', default=False)
args = parser.parse_args()

project = os.path.expanduser('~/projects/blind_deconv/')
head = os.path.join(project, 'simulation')
simulation_dir = os.path.join(head, os.path.basename(os.path.normpath(args.simulation_dir)))


print simulation_dir

if not os.path.exists(os.path.join(simulation_dir, 'train/res.txt')):
    subprocess.call('cd '+os.path.join(simulation_dir, 'train')+' && '+
                    'bash {}'.format(os.path.join(project, 'shell_utils/train2response.sh')),
                    shell=True)

test_dir = os.path.join(simulation_dir, 'test')
print test_dir

angle_ratio_dir_list = [os.path.join(test_dir, d) for d in os.listdir(test_dir) if 'DS_Store' not in d]
result_dir_list = [os.path.join(test_dir, d,  'csd_results', 'lmax{}reg{}'.format(args.lmax, args.reg))
                        for d in os.listdir(test_dir) if 'DS_Store' not in d]
for result_dir, angle_ratio_dir in zip(result_dir_list, angle_ratio_dir_list):
    if args.force or (not os.path.exists(os.path.join(result_dir, 'fod.mif'))):
        if not os.path.exists(result_dir):
            os.makedirs(result_dir)
        print result_dir
        subprocess.call('dwi2fod csd {angle_ratio_dir} {res} {fod} -lmax {lmax} -neg_lambda {reg}'.format(
            angle_ratio_dir=os.path.join(angle_ratio_dir, 'data/dwi.mif'),
            res=os.path.join(simulation_dir, 'train/res.txt'),
            fod=os.path.join(result_dir, 'fod.mif'),
            lmax=args.lmax, reg=args.reg),
        shell=True)

        subprocess.call('sh2amp {fod} -gradient {gra} {amp}'.format(
            fod=os.path.join(result_dir, 'fod.mif'),
            amp=os.path.join(result_dir, 'amp.mif'),
            gra=os.path.join(project, 'dwsignsim/bdirs_whole_1272.txt')),
        shell=True
        )

        os.remove(os.path.join(result_dir, 'fod.mif')