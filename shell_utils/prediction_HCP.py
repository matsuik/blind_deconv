import os
import argparse
import subprocess

opj = os.path.join
parser = argparse.ArgumentParser()
parser.add_argument('pred_dir_fp', type=str)
parser.add_argument('n_output_samples', type=int)
args = parser.parse_args()

project_dir_fp = os.path.expanduser('~/projects/blind_deconv/')
pred_dir_fp = os.path.expanduser(args.pred_dir_fp)

cmd = ' && '.join(['cd {}'.format(opj(project_dir_fp, 'dwsignsim')),
                   'matlab -nojvm -r \"save_HCP(\'{}\', {})\"'.format(pred_dir_fp, args.n_output_samples),
                   'cd {}'.format(pred_dir_fp),
                   'amp2sh dixel_pred.mif sh_pred.mif',
                   'mrconvert sh_pred.mif -stride 2,3,-4,1 sh_pred_zflipped.mif',
                   'mrtransform -linear {} sh_pred_zflipped.mif sh_pred_zflipped_translated.mif'.format(opj(project_dir_fp, 'HCP/translate.txt')),
                   'rm pred.mat',
                   'rm dixel_pred.mif',
                   'rm sh_pred.mif',
                   'rm sh_pred_zflipped.mif',
                   'cd {}'.format(project_dir_fp),
                   'rsync -rl0tv HCP/b3000/ $BD_MAUI/HCP/b3000/'
                   ])

subprocess.Popen(cmd, shell=True).wait()
