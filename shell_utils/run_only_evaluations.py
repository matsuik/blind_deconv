import glob
import os
import sys
import subprocess
sys.path.append('/home/matsui-k/projects/blind_deconv')
import blind_utils
simulation_dir_list = ['b3000_gauss_15_1272_snr20', 'b3000_gaussian_snr20_dw30', 'b3000_gaussian_snr20_dw90']

for simulation_dir in simulation_dir_list:
    for model_name in glob.glob(os.path.join('simulation', simulation_dir, 'trained_models/*')):
    #     subprocess.Popen('pwd && python ../core/rcn_pred_evaluation.py {} {} --only_evaluation'.format(
    #         simulation_dir, model_name
    #     ), shell=True)

        for rotated in [True, False]:
            try:
                blind_utils.summarize_over_angles2df(simulation_dir, model_name, 1, rotated)
            except Exception as e:
                print e
