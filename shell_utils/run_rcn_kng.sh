#!/usr/bin/env bash
# run model_scripts/*.py on post K gpu
# ex source shell_utils/run_rcn_kng.sh 40 'python model_scripts/linear.py b3000_gaussian_novi_snr50 0 8 8
# ex source shell_utils/run_rcn_kng.sh 40 'python model_scripts/multiLayer.py b3000_gaussian_novi_snr50 3 8 8
# ex debug) source shell_utils/run_rcn_kng.sh 40 'python model_scripts/multiLayer.py b3000_gaussian_snr20_dw30 3 8 8 --n_epochs=2 --interval=1 --debug_data'
# lmax8なら40でいける
# lmax12なら60くらい
gb=$1
cmd=$2

srun -p urgent --gres=gpu:1 --mem ${gb}000 -c 4 -t 1440 ${cmd}
