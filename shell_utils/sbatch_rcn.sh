#!/usr/bin/env bash
# run model_scripts/*.py on post K gpu.
gb=$1
cmd=$2
printf '#!/bin/bash \n#SBATCH --gres=gpu:teslaM40:1 \n#SBATCH --mem '${gb}'000\n#SBATCH -c 8\n#SBATCH -t 2000\n'${cmd}
