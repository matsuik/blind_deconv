num_cpu=$1
num_gpu=$2
mem_gb=$3

srun --pty -c ${num_cpu} --gres=gpu:${num_gpu} --mem ${mem_gb}000 -c 2 /bin/bash