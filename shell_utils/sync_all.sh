#!/usr/bin/env bash
rsync -av $MAUI_HOME/projects/blind_deconv/core/ core/
rsync -av $MAUI_HOME/projects/blind_deconv/shell_utils/ shell_utils/
rsync -av $MAUI_HOME/projects/blind_deconv/model_scripts/ model_scripts/
