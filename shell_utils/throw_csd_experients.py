import subprocess

simulation_dir_list = ['b3000_gaussian_snr20_dw30', 'b3000_gaussian_snr20_dw90']
lmax_list = [6]
reg_list = [0.1, 1, 10]

for simulation_dir in simulation_dir_list:
    for lmax in lmax_list:
        for reg in reg_list:
            with open('tmp/srun_csd_{}_{}_{}.log'.format(simulation_dir, lmax, reg), 'w') as f:
                subprocess.Popen(
                    "python shell_utils/csd_dwi2amp.py {} {} {} --force".format(simulation_dir, lmax, reg),
                    shell=True, stdout=f)

                # subprocess.Popen(
                #     "python shell_utils/csd_amp2pred_evaluation.py {} {} {}".format(simulation_dir, lmax, reg),
                #     shell=True, stdout=f, stderr=subprocess.STDOUT)
